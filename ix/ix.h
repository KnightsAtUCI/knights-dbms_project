#include <vector>
#include <string>
#include <string.h>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <algorithm>
#include <set>
#include "../rbf/rbfm.h"

# define IX_EOF (-1)
#define PAGE_SIZE 4096

#define FAIL -1
#define PASS 0

#ifndef BPLUSTREEDEFINED
#define BPLUSTREEDEFINED
#define BCONST 250
class IXFileHandle {
public:

	unsigned ixReadPageCounter;
	unsigned ixWritePageCounter;
	unsigned ixAppendPageCounter;

	FileHandle dataFile;
	FileHandle indexFile;
	IXFileHandle();
	~IXFileHandle();
	// Put the current counter values of associated PF FileHandles into variables
	RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);
	RC readPage(PageNum pageNum, void *data, FileHandle &fileHandle);
	RC writePage(PageNum pageNum, void *data, FileHandle &fileHandle);
	RC appendPage(const void *data, FileHandle &fileHandle);
	RC getNumberOfPages(FileHandle &fileHandle);
	bool indexFileExists(FileHandle &fileHandle);

};

struct leafNodeStruct;
struct leafValue{
	unsigned int pageNumber;
	unsigned int slotNumber;
};

class IX_ScanIterator {
public:
	leafNodeStruct * leafNode;
	int leafNodePosition;
	leafValue leafData;
	IXFileHandle *fileHandlePointer;
	IXFileHandle ixfileHandle;
	Attribute attribute;
	const void *lowKey;
	const void *highKey;
	bool lowKeyInclusive;
	bool highKeyInclusive;
	bool iterate;
	IX_ScanIterator();
	~IX_ScanIterator();
	RC getNextEntry(RID &rid, void *key);
	RC close();


};
enum KeyType {INVALID = -1, INTNUMBER = 0, REALNUMBER = 1, STRING = 2};



struct indexKey {
	KeyType keyType;
	int charLength;
	//union{
	int integerValue;
	float realValue;
	char keyString[1000];
	//}
	bool readFromFile(string f, int offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r");
		fseek(fp, offset, SEEK_SET);
		fread(&this->keyType, sizeof(this->keyType), 1, fp);
		offset += sizeof(this->keyType);
		fread(&this->charLength, sizeof(this->charLength), 1, fp);
		offset += sizeof(this->charLength);
		fread(&this->integerValue, sizeof(this->integerValue), 1, fp);
		offset += sizeof(this->integerValue);
		fread(&this->realValue, sizeof(this->realValue), 1, fp);
		offset += sizeof(this->realValue);
		fread(this->keyString, this->charLength, 1, fp);
		fclose(fp);
		return true;
	}

	bool writeToFile(string f, int offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r+");
		fseek(fp, offset, SEEK_SET);
		int pos = sizeof(*this) - sizeof(this->keyString);
		void *buffer = malloc(pos + this->charLength);
		memcpy(buffer, (void*)this, pos);
		memcpy((char*)buffer + pos, this->keyString, this->charLength);
		fwrite(buffer, pos+this->charLength, 1, fp);
		fclose(fp);
		return true;
	}

	indexKey(){
		//		keyString = NULL;
		integerValue = 0;
		realValue = 0;
		keyType = INTNUMBER;
		charLength = 0;
	}

	indexKey(int keyType, int* intValue, float* realValue, const char *str = "", int charLength = 0){
		if(keyType == 0){
			this->keyType = INTNUMBER;
			this->integerValue = *intValue;
			//			this->keyString = NULL;
		}else if(keyType == 1){
			this->keyType = REALNUMBER;
			this->realValue = *realValue;
			//			this->keyString = NULL;
		}else{
			this->keyType = STRING;
			this->charLength = charLength;
			this->charLength = this->charLength + 1;
			//			keyString = new char[this->charLength];
			bzero(keyString, this->charLength);
			memcpy(keyString, str, this->charLength);
			keyString[this->charLength - 1] = '\0';
			//			cout<<"\n Assigning : " << keyString;
		}
	}
};

inline int checkConditionForSearchRange(indexKey &a, indexKey &b) {
	int treeKeyType = a.keyType;
	float t = 0;
	int givenKeyType = b.keyType;
	if(treeKeyType == givenKeyType){
		switch(treeKeyType){
		case 0:
			return a.integerValue - b.integerValue;
			break;
		case 1:
			t = a.realValue - b.realValue;
			if(t > 0 ){
				return 1;
			}else if(t == 0){
				return 0;
			}else{
				return -1;
			}
			break;
		case 2:
			return  strcmp(a.keyString, b.keyString);
			break;
		}
	}
	return FAIL;

}

inline int checkCondition(const indexKey &a, const indexKey &b) {
	int treeKeyType = a.keyType;
	int givenKeyType = b.keyType;
	float t = 0;
	if(treeKeyType == givenKeyType){
		switch(treeKeyType){
		case 0:
			return a.integerValue - b.integerValue;
			break;
		case 1:
			t = a.realValue - b.realValue;
			if(t > 0 ){
				return 1;
			}else if(t == 0){
				return 0;
			}else{
				return -1;
			}
			break;
			break;
		case 2:
			return strcmp(a.keyString, b.keyString);
			break;
		}
	}
	return FAIL;

}

#define META_INFORMATION_START 0
#define META_INFO_END META_INFORMATION_START + sizeof(IndexCatalogInfo)
#define LeafNodeStructMinusChildren sizeof(leafNodeStruct) - BCONST * sizeof(leafNodeRecord)

typedef struct {
	size_t order;
	size_t value_size;
	size_t key_size;
	size_t internal_node_num;
	size_t leaf_node_num;
	size_t height;
	off_t freeSpacePointer;
	off_t root_offset;
	off_t leaf_offset;
} IndexCatalogInfo;

struct internalNodeKeyStruct {
	off_t child;
	indexKey key;

	bool readFromFile(string f, int& offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r");
		fseek(fp, offset, SEEK_SET);
		fread(&this->child, sizeof(this->child), 1, fp);
		offset += sizeof(this->child);
		this->key.readFromFile(f, offset);
		fclose(fp);
		return true;
	}

	bool writeToFile(string f, int &offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r+");
		fseek(fp, offset, SEEK_SET);
		int pos = sizeof(this->child);
		void *buffer = malloc(pos);
		memcpy(buffer, &this->child, pos);
		offset += sizeof(this->child);
		fwrite(buffer, sizeof(this->child), 1, fp);
		this->key.writeToFile(f, offset);
		fclose(fp);
		return true;
	}

};

struct internalNodeStruct {
	typedef internalNodeKeyStruct * child_t;
	off_t parent;
	off_t next;
	off_t prev;
	size_t n;
	internalNodeKeyStruct children[BCONST];

	bool readFromFile(string f, int& offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r");
		fseek(fp, offset, SEEK_SET);
		fread(&this->parent, sizeof(this->parent), 1, fp);
		fread(&this->next, sizeof(this->next), 1, fp);
		fread(&this->prev, sizeof(this->prev), 1, fp);
		fread(&this->n, sizeof(this->n), 1, fp);
		offset +=  sizeof(this->parent) + sizeof(this->next) + sizeof(this->prev) + sizeof(this->n);
		for(int t = 0; t < BCONST; t++){
			this->children[t].readFromFile(f, offset);
		}
		fclose(fp);
		return true;
	}

	bool writeToFile(string f, int offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r+");
		fseek(fp, offset, SEEK_SET);
		int size = sizeof(off_t) * 3 + sizeof(size_t);
		void *buffer = malloc(size);
		memcpy(buffer, (void*)this, size);
		offset+=size;
		for(int t = 0; t < BCONST; t++){
			this->children[t].writeToFile(f, offset);
		}
		fclose(fp);
		return true;
	}
};

struct leafNodeRecord {
	leafValue valueLeaf;
	indexKey key;

	bool readFromFile(string f, int& offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r");
		fseek(fp, offset, SEEK_SET);
		fread(&this->valueLeaf, sizeof(this->valueLeaf), 1, fp);
		offset += sizeof(this->valueLeaf);
		this->key.readFromFile(f, offset);
		fclose(fp);
		return true;
	}

	bool writeToFile(string f, int &offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r+");
		fseek(fp, offset, SEEK_SET);
		int pos = sizeof(this->valueLeaf);
		void *buffer = malloc(pos);
		memcpy(buffer, &this->valueLeaf, pos);
		offset += sizeof(this->valueLeaf);
		fwrite(buffer, sizeof(this->valueLeaf), 1, fp);
		this->key.writeToFile(f, offset);
		fclose(fp);
		return true;
	}


};

struct leafNodeStruct {
	leafNodeStruct() : isNull(false){}
	bool isNull;
	off_t parent;
	off_t next;
	off_t prev;
	size_t n;
	leafNodeRecord children[BCONST];

	bool readFromFile(string f, int& offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r");
		fseek(fp, offset, SEEK_SET);
		fread(&this->isNull, sizeof(this->isNull), 1, fp);
		fread(&this->parent, sizeof(this->parent), 1, fp);
		fread(&this->next, sizeof(this->next), 1, fp);
		fread(&this->prev, sizeof(this->prev), 1, fp);
		fread(&this->n, sizeof(this->n), 1, fp);
		offset += sizeof(this->isNull) + sizeof(this->parent) + sizeof(this->next) + sizeof(this->prev) + sizeof(this->n);
		for(int t = 0; t < BCONST; t++){
			this->children[t].readFromFile(f, offset);
		}
		fclose(fp);
		return true;
	}

	bool writeToFile(string f, int offset){
		FILE *fp;
		fp = fopen (f.c_str() , "r+");
		fseek(fp, offset, SEEK_SET);
		int size = sizeof(bool) + sizeof(off_t) * 3 + sizeof(size_t);
		void *buffer = malloc(size);
		memcpy(buffer, (void*)this, size);
		offset+=size;
		for(int t = 0; t < BCONST; t++){
			this->children[t].writeToFile(f, offset);
		}
		fclose(fp);
		return true;
	}


};
typedef leafNodeRecord *child_t;

class BPlusTree {
public:

	PagedFileManager * _pf_manager_bplustree;
	indexKey& getFirstLeafKey();
	indexKey& getLastLeafKey();
	char *buffer;
	BPlusTree(){
		bFileHandle = NULL;
	}
	~BPlusTree();
	BPlusTree(IXFileHandle& bPlusreeFileHandle, bool createIndexFile = false);
	int search_range(indexKey *left, const indexKey &right, leafValue &rid, bool l, bool r, leafNodeStruct *& leafNode, int &n, bool *next = NULL) const;
	int remove(const indexKey& key, RID rid);
	int insert(const indexKey& key, leafValue value);
	IndexCatalogInfo get_indexCatalogInfo() const {
		return indexCatalogInfo;
	};

	int getBPlusTreeOrder(Attribute &attribute, void *key);

	void printLeaf();
	IXFileHandle *bFileHandle;
	string indexFilePath;
	IndexCatalogInfo indexCatalogInfo;
	void printInternalNode(off_t offset, const Attribute &attribute);
	void printLeafNode(off_t leafNode, const Attribute &attribute);
	void printBplusTree(const Attribute &attribute);
	void BPlusTtreeInit(IXFileHandle& bPlusreeFileHandle);
	off_t getParentOfLeaf(const indexKey &key) const;
	off_t searchLeafNode(off_t index, const indexKey &key) const;
	off_t searchLeafNodeLowerBound(off_t index, const indexKey &key) const;
	off_t searchLeafNode(const indexKey &key) const
	{
		return searchLeafNode(getParentOfLeaf(key), key);
	}

	off_t searchLeafNodeLowerBound(const indexKey &key) const
	{
		return searchLeafNodeLowerBound(getParentOfLeaf(key), key);
	}

	void removeFromIndexFile(off_t offset, internalNodeStruct &node,
			const indexKey &key);
	bool borrowKey(bool from_right, internalNodeStruct &borrower,
			off_t offset);

	bool borrowKey(bool from_right, leafNodeStruct &borrower);
	void change_parent_child(off_t parent, const indexKey &o, const indexKey &n);
	void mergeLeafNodeStruct(leafNodeStruct *left, leafNodeStruct *right);

	void mergeKeys(internalNodeKeyStruct *where, internalNodeStruct &left,
			internalNodeStruct &right);

	void insertKeyValueDirectly(leafNodeStruct *leaf,
			const indexKey &key, const leafValue &value);

	void insertIndexKey(off_t offset, const indexKey &key,
			off_t value, off_t after);
	void insertIndexWithoutSplit(internalNodeStruct &node, const indexKey &key,
			off_t value);
	void updateParentForChild(internalNodeKeyStruct *begin, internalNodeKeyStruct *end,
			off_t parent);

	template<class T>
	void newNodeCreate(off_t offset, T *currentNode, T *nextNode);

	template<class T>
	void removeNodeFromTree(T *previousNode, T *currentNode);

	off_t getNewFreeSpacePointer(size_t size)
	{
		off_t slot = indexCatalogInfo.freeSpacePointer;
		indexCatalogInfo.freeSpacePointer += size;
		return slot;
	}

	off_t alloc(leafNodeStruct *leaf)
	{
		leaf->n = 0;
		indexCatalogInfo.leaf_node_num++;
		return getNewFreeSpacePointer(sizeof(leafNodeStruct));
	}

	off_t alloc(internalNodeStruct *node)
	{
		node->n = 1;
		indexCatalogInfo.internal_node_num++;
		return getNewFreeSpacePointer(sizeof(internalNodeStruct));
	}

	void unalloc(leafNodeStruct *leaf, off_t offset)
	{
		--indexCatalogInfo.leaf_node_num;
	}

	void unalloc(internalNodeStruct *node, off_t offset)
	{
		--indexCatalogInfo.internal_node_num;
	}

	int readData(void *block, off_t offset, size_t size) const
	{
		int temp = 0;
		int boffset = 0;
		int pageNumber = offset/PAGE_SIZE;
		int positionPage = offset - (pageNumber * PAGE_SIZE);
		int rc = this->bFileHandle->indexFile.readPage(pageNumber, buffer);
		int r = 0;
		temp = size;
		if(positionPage + size <= PAGE_SIZE){
			memcpy(block, buffer + positionPage, size);
		}else{
			while(temp > (PAGE_SIZE - positionPage)){
				memcpy(block + boffset, buffer + positionPage, PAGE_SIZE - positionPage);
				temp = temp - (PAGE_SIZE - positionPage);
				boffset += (PAGE_SIZE - positionPage);
				pageNumber = pageNumber + 1;
				positionPage = 0;
				rc = this->bFileHandle->indexFile.readPage(pageNumber, buffer);
				r = r + 1;
			}
			memcpy(block + boffset, buffer + positionPage, temp);
		}
		this->bFileHandle->indexFile.readPageCounter -= (r-1);
		return 1;
	}

	int writeData(void *data, off_t offset, size_t size) const
	{
		int pageNumber = offset/PAGE_SIZE;
		int positionPage = offset - (pageNumber * PAGE_SIZE);
		int rc;
		int r = 0;
		int w = 0;
		int a = 0;
		int boffset = 0;
		int temp = size;
		int time = 0;
		bzero(buffer, PAGE_SIZE);
		rc = this->bFileHandle->indexFile.readPage(pageNumber, buffer);
		if(positionPage + size <= PAGE_SIZE){
			memcpy(buffer + positionPage, data, size);
			rc = this->bFileHandle->indexFile.writePage(pageNumber, buffer);
		}else{
			while(temp > (PAGE_SIZE - positionPage)){
				memcpy(buffer + positionPage, (char*)data + boffset, PAGE_SIZE - positionPage);
				if(pageNumber == this->bFileHandle->getNumberOfPages(this->bFileHandle->indexFile)){
					rc = this->bFileHandle->indexFile.appendPage(buffer);
					a = a + 1;
				}else{
					rc = this->bFileHandle->indexFile.writePage(pageNumber, buffer);
					w = w + 1;
				}
				temp = temp - (PAGE_SIZE - positionPage);
				boffset += (PAGE_SIZE - positionPage);
				pageNumber = pageNumber + 1;
				positionPage = 0;
				time = time + 1;
				rc = this->bFileHandle->indexFile.readPage(pageNumber, buffer);
				r = r + 1;
			}
			if(pageNumber == this->bFileHandle->getNumberOfPages(this->bFileHandle->indexFile)){
				memcpy(buffer + positionPage, (char*)data + boffset, temp);
				rc = this->bFileHandle->indexFile.appendPage(buffer);
			}else{
				memcpy(buffer + positionPage, (char*)data + boffset, temp);
				rc = this->bFileHandle->indexFile.writePage(pageNumber, buffer);
			}

		}
		this->bFileHandle->indexFile.readPageCounter -= (r-1);
		this->bFileHandle->indexFile.writePageCounter -= (w-1);
		this->bFileHandle->indexFile.appendPageCounter -= (a-1);
		return 1;
	}

	template<class T>
	int readData(T *block, off_t offset) const
	{
		return readData(block, offset, sizeof(T));
	}

	template<class T>
	int writeData(T *block, off_t offset) const
	{
		return writeData(block, offset, sizeof(T));
	}


};


#define OPERATOR_KEYCMP(type) \
		bool operator< (const indexKey &l, const type &r) {\
	return checkCondition(l, r.key) < 0;\
}\
bool operator< (const type &l, const indexKey &r) {\
	return checkCondition(l.key, r) < 0;\
}\
bool operator== (const indexKey &l, const type &r) {\
	return checkCondition(l, r.key) == 0;\
}\
bool operator== (const type &l, const indexKey &r) {\
	return checkCondition(l.key, r) == 0;\
}


class IndexManager {

public:
	short lengthOfAttribute(Attribute Attr);		 // to find length of a given attribute
	PagedFileManager * _pf_manager;
	RecordBasedFileManager * rbfm;

	static IndexManager* instance();

	// Create an index file.
	RC createFile(const string &fileName);

	// Delete an index file.
	RC destroyFile(const string &fileName);

	// Open an index and return an ixfileHandle.
	RC openFile(const string &fileName, IXFileHandle &ixfileHandle);

	// Close an ixfileHandle for an index.
	RC closeFile(IXFileHandle &ixfileHandle);

	// Insert an entry into the given index that is indicated by the given ixfileHandle.
	RC insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

	// Delete an entry from the given index that is indicated by the given ixfileHandle.
	RC deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

	// Initialize and IX_ScanIterator to support a range search
	RC scan(IXFileHandle &ixfileHandle,
			const Attribute &attribute,
			const void *lowKey,
			const void *highKey,
			bool lowKeyInclusive,
			bool highKeyInclusive,
			IX_ScanIterator &ix_ScanIterator);

	// Print the B+ tree in pre-order (in a JSON record format)
	void printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const;

	RC getDataFileName(const string &fileName, string &newFileName);

	RC getIndexFileName(const string &fileName, string &newFileName);

	RC  getNextEntry(IX_ScanIterator &iX_ScanIterator, void* key, bool iterate);

protected:
	IndexManager();
	~IndexManager();

private:
	static IndexManager *_index_manager;
};




#endif
