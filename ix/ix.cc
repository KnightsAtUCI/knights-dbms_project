#include "ix.h"
#include <fstream>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <queue>
#include <list>
#include <set>
#include <algorithm>
#include <vector>
#include <climits>
#include <set>
#define PASS 0
#define FAIL -1

using namespace std;
using std::swap;
using std::binary_search;
using std::lower_bound;
using std::upper_bound;

bool isInLeaf = false;

bool flag = false;
int lcount = 0;
IndexManager* IndexManager::_index_manager = 0;

leafNodeStruct PreviousleafNode;
int nInLeaf;

IndexManager* IndexManager::instance() {
	if (!_index_manager)
		_index_manager = new IndexManager();

	return _index_manager;
}

IndexManager::IndexManager() {
	_pf_manager = PagedFileManager::instance();
	rbfm = RecordBasedFileManager::instance();
}

IndexManager::~IndexManager() {

}

RC IndexManager::createFile(const string &fileName) {
	string dataFileName;
	string indexFileName;
	//	this->getDataFileName(fileName, dataFileName);
	this->getIndexFileName(fileName, indexFileName);
	//	int dataFileCreatedStatus = _pf_manager->createFile(dataFileName);
	int indexFileCreatedStatus = _pf_manager->createFile(indexFileName);
	if (indexFileCreatedStatus == PASS) {
		return PASS;
	}
	return FAIL;

}

RC IndexManager::destroyFile(const string &fileName) {
	string dataFileName;
	string indexFileName;
	//	this->getDataFileName(fileName, dataFileName);
	this->getIndexFileName(fileName, indexFileName);
	//	int dataFileCreatedStatus = _pf_manager->destroyFile(dataFileName);
	int indexFileCreatedStatus = _pf_manager->destroyFile(indexFileName);
	if (indexFileCreatedStatus == PASS) {
		return PASS;
	}
	return FAIL;
}

//RC IndexManager::getDataFileName(const string &fileName, string &newFileName) {
//
//	string suffix = ".DATA";
//	newFileName = fileName;// + suffix;
//	return PASS;
//}

RC IndexManager::getIndexFileName(const string &fileName, string &newFileName) {

	string suffix = ".INDEX";
	newFileName = fileName;// + suffix;
	return PASS;
}

RC IndexManager::openFile(const string &fileName, IXFileHandle &ixfileHandle) {
	string dataFileName;
	string indexFileName;
	//	this->getDataFileName(fileName, dataFileName);
	this->getIndexFileName(fileName, indexFileName);
	//	int dataFileOpenStatus = _pf_manager->openFile(dataFileName.c_str(),
	//			ixfileHandle.dataFile);
	int indexFileOpenStatus = _pf_manager->openFile(indexFileName.c_str(),
			ixfileHandle.indexFile);
	if (indexFileOpenStatus == PASS) {
		return PASS;
	}
	return FAIL;

}

RC IndexManager::closeFile(IXFileHandle &ixfileHandle) {
	//	int dataFileCloseStatus = _pf_manager->closeFile(ixfileHandle.dataFile);
	int indexFileCloseStatus = _pf_manager->closeFile(ixfileHandle.indexFile);
	if (indexFileCloseStatus == PASS) {
		return PASS;
	}
	return FAIL;

}

short IndexManager::lengthOfAttribute(Attribute attr) {

	if (attr.type == TypeInt)
		return sizeof(int);
	else if (attr.type == TypeReal)
		return sizeof(float);
	else
		return (sizeof(int) + attr.length);
}

bool IXFileHandle::indexFileExists(FileHandle &fileHandle){
	return fileHandle.checkFileEmpty();
}

RC IndexManager::insertEntry(IXFileHandle &ixfileHandle,
		const Attribute &attribute, const void *key, const RID &rid) {
	AttrType attributeType;
	attributeType = attribute.type;
	int keyType;
	int* intValue;
	float* realValue;
	int ch;
	char *t;
	switch(attributeType){
	case TypeInt:
		keyType = 0;
		intValue = (int*)key;
		break;
	case TypeReal:
		keyType = 1;
		realValue = (float*)key;
		break;
	case TypeVarChar:
		keyType = 2;
		ch = *((int*)((char*)key));
		t = new char[ch];
		memcpy(t, key + sizeof(int), ch);
		break;
	case TypeNone:
		return FAIL;
	}

	indexKey indexKey(keyType, intValue, realValue, (const char*)t, ch);
	leafValue indexValue;
	indexValue.pageNumber = rid.pageNum;
	indexValue.slotNumber = rid.slotNum;

	if(ixfileHandle.indexFile.checkFileEmpty()){
		BPlusTree bplustree(ixfileHandle, true);
		if(keyType == 2){ if(ch > 500){ bplustree.indexCatalogInfo.order = 4;}}
		bplustree.insert(indexKey, indexValue);
	}else{
		BPlusTree bplustree(ixfileHandle, false);
		if(keyType == 2){ if(ch > 500){ bplustree.indexCatalogInfo.order = 4; }}
		bplustree.insert(indexKey, indexValue);
	}
	return PASS;
}

RC IndexManager::deleteEntry(IXFileHandle &ixfileHandle,
		const Attribute &attribute, const void *key, const RID &rid) {
	AttrType attributeType;
	attributeType = attribute.type;
	int keyType;
	int* intValue;
	float* realValue;
	int ch;
	char *t;
	int charlength = attribute.length;
	switch(attributeType){
	case TypeInt:
		keyType = 0;
		intValue = (int*)key;
		break;
	case TypeReal:
		keyType = 1;
		realValue = (float*)key;
		break;
	case TypeVarChar:
		keyType = 2;
		ch = *((int*)((char*)key));
		t = new char[ch];
		memcpy(t, key + sizeof(int), ch);
		break;
	case TypeNone:
		return FAIL;
	}
	bool isEmpty = false;
	if(ixfileHandle.indexFile.checkFileEmpty()){
		isEmpty = true;
	}
	BPlusTree bplustree(ixfileHandle, isEmpty);
	indexKey indexKey(keyType, intValue, realValue, (const char*)t, ch);
	RC status = bplustree.remove(indexKey, rid);
	return status;
}

RC IndexManager::scan(IXFileHandle &ixfileHandle, const Attribute &attribute,
		const void *lowKey, const void *highKey, bool lowKeyInclusive,
		bool highKeyInclusive, IX_ScanIterator &ix_ScanIterator) {

	if(ixfileHandle.indexFile.checkFileEmpty()){
		//cout<<"\n Cannot scan when file is empty.";
		return FAIL;
	}
	ix_ScanIterator.ixfileHandle = ixfileHandle;
	ix_ScanIterator.attribute = attribute;
	ix_ScanIterator.lowKey = lowKey;
	ix_ScanIterator.highKey = highKey;
	ix_ScanIterator.lowKeyInclusive = lowKeyInclusive;
	ix_ScanIterator.highKeyInclusive = highKeyInclusive;
	ix_ScanIterator.fileHandlePointer = &ixfileHandle;
	ix_ScanIterator.leafNode = new leafNodeStruct();
	ix_ScanIterator.leafNode->isNull = true;
	ix_ScanIterator.leafNodePosition = 0;
	ix_ScanIterator.leafData.pageNumber = 0;
	ix_ScanIterator.leafData.slotNumber = 0;

	_pf_manager->openFile(ixfileHandle.indexFile.currentFileName.c_str(), ixfileHandle.indexFile);
	return PASS;
}

RC IX_ScanIterator::close(){
	PagedFileManager *_pf_manager = PagedFileManager::instance();
	delete this->leafNode;
	return _pf_manager->closeFile(this->ixfileHandle.indexFile);
}

void IndexManager::printBtree(IXFileHandle &ixfileHandle,
		const Attribute &attribute) const {
	bool isEmpty = false;
	if(ixfileHandle.indexFile.checkFileEmpty()){
		isEmpty = true;
	}

	BPlusTree bplustree(ixfileHandle, isEmpty);
	bplustree.printBplusTree(attribute);
}

void BPlusTree::printInternalNode(off_t offset, const Attribute &attribute){
	internalNodeStruct internalNode;
	readData(&internalNode, offset);
	AttrType attrType;
	set<int>::iterator it1;
	set<float>::iterator it2;
	set<string>::iterator it3;
	attrType = attribute.type;
	cout<<"{" << endl;
	cout<<"\"keys\":[";
	if(attrType == TypeInt){
		set <int> values;
		for(size_t i = 0; i < internalNode.n; i++){
			values.insert(internalNode.children[i].key.integerValue);
		}
		for (it1=values.begin() ; it1 != values.end(); it1++ ){
			cout << "\"" << *it1 << "\"";
			if(it1 == --values.end()){
				break;
			}
			cout << ",";
		}

	}else if(attrType == TypeReal){
		set <float> values;
		for(size_t i = 0; i < internalNode.n; i++){
			values.insert(internalNode.children[i].key.realValue);
		}
		for ( it2=values.begin() ; it2 != values.end(); it2++ ){
			cout << "\"" << *it2 << "\"";
			if(it2 == --values.end()){
				break;
			}
			cout << ",";
		}
	}else if(attrType == TypeVarChar){
		set <string> values;
		for(size_t i = 0; i < internalNode.n; i++){
			if(string(internalNode.children[i].key.keyString) != ""){
				values.insert(string(internalNode.children[i].key.keyString));
			}

		}
		for ( it3=values.begin() ; it3 != values.end(); it3++ ){
			cout << "\"" << *it3 << "\"";
			if(it3 == --values.end()){
				break;
			}
			cout << ",";

		}
	}
	cout<<"],";
	cout<<endl;
	cout<<"\"children\": [\n";
	for(size_t i = 0; i < internalNode.n; i++){
		printLeafNode(internalNode.children[i].child, attribute);
		if(i < internalNode.n - 1){
			cout<<",";
		}
		cout<<endl;
	}
	cout<<"]"<<endl<<"}";
}

void BPlusTree::printLeafNode(off_t leafNodeOffset, const Attribute &attribute){
	leafNodeStruct leaf;
	readData(&leaf, leafNodeOffset);
	off_t p = 0;
	p = leaf.parent;
	size_t t = 0;
	map <int, vector<RID> > intValues;
	map <float, vector<RID> > realValues;
	map <string, vector<RID> > stringValues;
	vector<RID> temp;
	std::map<int, vector<RID> >::iterator it1;
	std::map<float, vector<RID> >::iterator it2;
	std::map<string, vector<RID> >::iterator it3;
	RID rid;
	cout<<"{\"keys\":[";
	int k = 0;
	switch(attribute.type){
	case TypeInt:
		k = 0;
		break;
	case TypeReal:
		k = 1;
		break;
	case TypeVarChar:
		k = 2;
		break;
	}
	for(t = 0; t < leaf.n; t++){
		if(k == 0){
			it1 = intValues.find(leaf.children[t].key.integerValue);
			if(it1 != intValues.end()){
				temp = intValues.find(leaf.children[t].key.integerValue)->second;
				rid.pageNum = leaf.children[t].valueLeaf.pageNumber;
				rid.slotNum = leaf.children[t].valueLeaf.slotNumber;
				temp.push_back(rid);
				it1->second = temp;
			}else{
				rid.pageNum = leaf.children[t].valueLeaf.pageNumber;
				rid.slotNum = leaf.children[t].valueLeaf.slotNumber;
				temp.clear();
				temp.push_back(rid);

			}
		}else if(k == 1){
			it2 = realValues.find(leaf.children[t].key.realValue);
			if(it2 != realValues.end()){
				temp = realValues.find(leaf.children[t].key.realValue)->second;
				rid.pageNum = leaf.children[t].valueLeaf.pageNumber;
				rid.slotNum = leaf.children[t].valueLeaf.slotNumber;
				temp.push_back(rid);
				it2->second = temp;
			}else{
				rid.pageNum = leaf.children[t].valueLeaf.pageNumber;
				rid.slotNum = leaf.children[t].valueLeaf.slotNumber;
				temp.clear();
				temp.push_back(rid);
				realValues.insert(pair<float, vector<RID> > (leaf.children[t].key.realValue, temp) );
			}
		}else{
			it3 = stringValues.find(leaf.children[t].key.keyString);
			if(it3 != stringValues.end()){
				temp = stringValues.find(leaf.children[t].key.keyString)->second;
				rid.pageNum = leaf.children[t].valueLeaf.pageNumber;
				rid.slotNum = leaf.children[t].valueLeaf.slotNumber;
				temp.push_back(rid);
				it3->second = temp;
			}else{
				rid.pageNum = leaf.children[t].valueLeaf.pageNumber;
				rid.slotNum = leaf.children[t].valueLeaf.slotNumber;
				temp.clear();
				temp.push_back(rid);
				stringValues.insert(pair<string, vector<RID> > (leaf.children[t].key.keyString, temp) );
			}
		}
		rid.pageNum = leaf.children[t].valueLeaf.pageNumber;
		rid.slotNum = leaf.children[t].valueLeaf.slotNumber;
		temp.push_back(rid);
	}
	if(k == 0){
		for (it1=intValues.begin() ; it1 != intValues.end(); it1++ ){
			cout << "\"" << it1->first << ":[";
			temp.clear();
			temp = it1->second;
			for(int s = 0; s < temp.size(); s++){
				cout<<"(" <<temp[s].pageNum << "," << temp[s].slotNum<<")";
				if(it1 == intValues.end()){
					cout<<",";
				}
			}
			cout<<"]\"";
			if(it1 == --intValues.end()){
				break;
			}
			cout << ",";

		}
		cout<<"]}";
	}else if(k == 1){
		for (it2=realValues.begin() ; it2 != realValues.end(); it2++ ){
			cout << "\"" << it2->first << ":[";
			temp.clear();
			temp = it2->second;
			for(int s = 0; s < temp.size(); s++){
				cout<<"(" <<temp[s].pageNum << "," << temp[s].slotNum<<")";
				if(it2 == realValues.end()){
					cout<<",";
				}
			}
			cout<<"]\"";
			if(it2 == --realValues.end()){
				break;
			}
			cout << ",";

		}
		cout<<"]}";
	}else{
		for (it3=stringValues.begin() ; it3 != stringValues.end(); it3++ ){
			cout << "\"" << it3->first << ":[";
			temp.clear();
			temp = it3->second;
			for(int s = 0; s < temp.size(); s++){
				cout<<"(" <<temp[s].pageNum << "," << temp[s].slotNum<<")";
				if(it3 == stringValues.end()){
					cout<<",";
				}
			}
			cout<<"]\"";
			if(it3 == --stringValues.end()){
				break;
			}
			cout << ",";

		}
		cout<<"]}";
	}
	intValues.clear();
	realValues.clear();
	stringValues.clear();
	temp.clear();
	readData(&leaf, leaf.next);
}


void BPlusTree::printBplusTree(const Attribute &attribute){
	off_t rootOffset = indexCatalogInfo.root_offset;
	int height = indexCatalogInfo.height;
	off_t leafFirst = indexCatalogInfo.leaf_offset;
	if(indexCatalogInfo.internal_node_num == 1 && indexCatalogInfo.leaf_node_num == 1){
		cout<<endl;
		this->printLeafNode(leafFirst, attribute);
		cout<<"\n";
	}else if(indexCatalogInfo.internal_node_num == 1){
		cout<<"\n";
		this->printInternalNode(rootOffset, attribute);
		cout<<"\n";
	}else{
		internalNodeStruct internalNode;
		readData(&internalNode, rootOffset);
		AttrType attrType;
		set<int>::iterator it1;
		set<float>::iterator it2;
		set<string>::iterator it3;
		attrType = attribute.type;
		cout<<"{" << endl;
		cout<<"\"keys\":[";
		if(attrType == TypeInt){
			set <int> values;
			for(size_t i = 0; i < internalNode.n; i++){
				values.insert(internalNode.children[i].key.integerValue);
			}
			for (it1=values.begin() ; it1 != values.end(); it1++ ){
				cout << "\"" << *it1 << "\"";
				if(it1 == --values.end()){
					break;
				}
				cout << ",";
			}

		}else if(attrType == TypeReal){
			set <float> values;
			for(size_t i = 0; i < internalNode.n; i++){
				values.insert(internalNode.children[i].key.realValue);
			}
			for ( it2=values.begin() ; it2 != values.end(); it2++ ){
				cout << "\"" << *it2 << "\"";
				if(it2 == --values.end()){
					break;
				}
				cout << ",";
			}
		}else if(attrType == TypeVarChar){
			set <string> values;
			for(size_t i = 0; i < internalNode.n; i++){
				if(string(internalNode.children[i].key.keyString) != ""){
					values.insert(string(internalNode.children[i].key.keyString));
				}

			}
			for ( it3=values.begin() ; it3 != values.end(); it3++ ){
				cout << "\"" << *it3 << "\"";
				if(it3 == --values.end()){
					break;
				}
				cout << ",";

			}
		}
		cout<<"],";
		cout<<endl;
		cout<<"\"children\": [\n";
		for(size_t i = 0; i < internalNode.n; i++){
			printInternalNode(internalNode.children[i].child, attribute);
			if(i < internalNode.n - 1){
				cout<<",";
			}
			cout<<endl;
		}
		cout<<"]"<<endl<<"}";
	}
}

IX_ScanIterator::IX_ScanIterator() {
	this->leafNodePosition = 0;
	this->iterate = true;
	this->leafData.pageNumber = 0;
	this->leafData.slotNumber = 0;
}

IX_ScanIterator::~IX_ScanIterator() {
}

RC IndexManager::getNextEntry(IX_ScanIterator &iX_ScanIterator, void* key, bool iterate){
	AttrType attributeType;
	attributeType = iX_ScanIterator.attribute.type;
	int keyType;
	int* lowKeyIntValue;
	int *highKeyIntValue;
	int chLow;
	int chHigh;
	char *lowString;
	char *highString;
	float* lowKeyRealValue;
	float* highKeyRealValue;
	switch(attributeType){
	case TypeInt:
		keyType = 0;
		lowKeyIntValue = (int*)iX_ScanIterator.lowKey;
		highKeyIntValue = (int*)iX_ScanIterator.highKey;
		break;
	case TypeReal:
		keyType = 1;
		lowKeyRealValue = (float*)iX_ScanIterator.lowKey;
		highKeyRealValue = (float*)iX_ScanIterator.highKey;
		break;
	case TypeVarChar:
		keyType = 2;
		if(iX_ScanIterator.lowKey != NULL){
			chLow = *((int*)((char*)iX_ScanIterator.lowKey));
			lowString = new char[chLow];
			memcpy(lowString, ((char*)iX_ScanIterator.lowKey + sizeof(int)), chLow);
		}
		if(iX_ScanIterator.highKey != NULL){
			chHigh = *((int*)((char*)iX_ScanIterator.highKey));
			highString = new char[chHigh];
			memcpy(highString, ((char*)iX_ScanIterator.highKey + sizeof(int)), chHigh);
		}
		break;
	case TypeNone:
		return FAIL;
	}
	int ret = 0;
	BPlusTree bplustree(iX_ScanIterator.ixfileHandle, false);

	indexKey lastKey;

	if(iX_ScanIterator.lowKey != NULL && iX_ScanIterator.highKey != NULL){
		indexKey lkey(keyType, lowKeyIntValue, lowKeyRealValue, (const char*)lowString, chLow);
		indexKey rkey(keyType, highKeyIntValue, highKeyRealValue, (const char*)highString, chHigh);
		indexKey leftKey = lkey;
		indexKey rightKey = rkey;
		lastKey = bplustree.getLastLeafKey();
		if(checkConditionForSearchRange(leftKey, lastKey) > 0){
			return -1;
		}
		ret = bplustree.search_range(&leftKey, rightKey, iX_ScanIterator.leafData, iX_ScanIterator.lowKeyInclusive, iX_ScanIterator.highKeyInclusive, iX_ScanIterator.leafNode, iX_ScanIterator.leafNodePosition, &iterate);
		iX_ScanIterator.iterate = iterate;
	}else{
		if(iX_ScanIterator.lowKey == NULL && iX_ScanIterator.highKey != NULL){
			indexKey leftKey = bplustree.getFirstLeafKey();
			indexKey rkey(keyType, highKeyIntValue, highKeyRealValue, (const char*)highString, chHigh);
			indexKey rightKey = rkey;
			lastKey = bplustree.getLastLeafKey();
			if(checkConditionForSearchRange(leftKey, lastKey) > 0){
				return -1;
			}
			ret = bplustree.search_range(&leftKey, rightKey, iX_ScanIterator.leafData, iX_ScanIterator.lowKeyInclusive, iX_ScanIterator.highKeyInclusive, iX_ScanIterator.leafNode, iX_ScanIterator.leafNodePosition, &iterate);
		}else if(iX_ScanIterator.lowKey != NULL && iX_ScanIterator.highKey == NULL){
			indexKey lkey(keyType, lowKeyIntValue, lowKeyRealValue, (const char*)lowString, chLow);
			indexKey leftKey = lkey;
			indexKey rightKey = bplustree.getLastLeafKey();
			lastKey = bplustree.getLastLeafKey();
			if(checkConditionForSearchRange(leftKey, lastKey) > 0){
				return -1;
			}
			ret = bplustree.search_range(&leftKey, rightKey, iX_ScanIterator.leafData, iX_ScanIterator.lowKeyInclusive, iX_ScanIterator.highKeyInclusive, iX_ScanIterator.leafNode, iX_ScanIterator.leafNodePosition, &iterate);
		}else{
			indexKey leftKey = bplustree.getFirstLeafKey();
			indexKey rightKey = bplustree.getLastLeafKey();
			lastKey = bplustree.getLastLeafKey();
			if(checkConditionForSearchRange(leftKey, lastKey) > 0){
				return -1;
			}
			ret = bplustree.search_range(&leftKey, rightKey, iX_ScanIterator.leafData, iX_ScanIterator.lowKeyInclusive, iX_ScanIterator.highKeyInclusive, iX_ScanIterator.leafNode, iX_ScanIterator.leafNodePosition, &iterate);
		}
		iX_ScanIterator.iterate = iterate;
	}
	unsigned readPageCount = 0;
	unsigned writePageCount = 0;
	unsigned appendPageCount = 0;

	iX_ScanIterator.ixfileHandle.indexFile.collectCounterValues(readPageCount, writePageCount, appendPageCount);
	iX_ScanIterator.fileHandlePointer->indexFile.readPageCounter = readPageCount;
	iX_ScanIterator.fileHandlePointer->indexFile.writePageCounter = writePageCount;
	iX_ScanIterator.fileHandlePointer->indexFile.appendPageCounter = appendPageCount;
	return ret;
}

RC IX_ScanIterator::getNextEntry(RID &rid, void *key) {
	IndexManager *_index_manager = IndexManager::instance();
	if(this->leafNodePosition == -10){
		return RBFM_EOF;
	}
	int returnValue = _index_manager->getNextEntry(*this, key, this->iterate);

	bool flag = false;
	if(this->leafNodePosition == -5){
		this->leafNodePosition = 1;
		flag = true;

	}

	if(this->leafNodePosition > BCONST + 5){
		this->leafNodePosition -= (2*BCONST);
		this->leafNodePosition += 1;
		flag = true;
	}
	if(returnValue < 0){
		return RBFM_EOF;
	}
	if(returnValue == FAIL){
		return RBFM_EOF;
	}
	int ivalue;
	int len = 0;
	int nlen = 0;
	float rvalue;
	rid.pageNum = this->leafData.pageNumber;
	rid.slotNum = this->leafData.slotNumber;
	AttrType attributeType;
	attributeType = this->attribute.type;
	switch(attributeType){
	case TypeInt:
		if(this->leafNodePosition != 0){
			ivalue = (this->leafNode->children[this->leafNodePosition-1].key.integerValue);
			memcpy(key, &ivalue, sizeof(int));
			break;
		}else{
			leafNodeStruct l;
			off_t t = this->leafNode->prev;
			BPlusTree bplustree(this->ixfileHandle, false);
			bplustree.readData(&l, t);
			ivalue = (l.children[l.n - 1].key.integerValue);
			memcpy(key, &ivalue, sizeof(int));
			break;

		}

	case TypeReal:
		if(this->leafNodePosition != 0){
			rvalue = (this->leafNode->children[this->leafNodePosition-1].key.realValue);
			memcpy(key, &rvalue, sizeof(float));
			break;
		}else{
			leafNodeStruct l;
			off_t t = this->leafNode->prev;
			BPlusTree bplustree(this->ixfileHandle, false);
			bplustree.readData(&l, t);
			rvalue = (l.children[l.n - 1].key.realValue);
			memcpy(key, &rvalue, sizeof(float));
			break;
		}
	case TypeVarChar:
		if(this->leafNodePosition != 0){
			len = this->leafNode->children[this->leafNodePosition-1].key.charLength;
			nlen = len - 1;
			//			cout<<"\n Length (1): " << nlen;
			//			cout<<"\n string : " << _index_manager->leafNode->children[_index_manager->leafNodePosition - 1].key.keyString;
			memcpy((char*)key, &(nlen), sizeof(int));
			memcpy((char*)key + sizeof(int), this->leafNode->children[this->leafNodePosition-1].key.keyString, nlen);
			break;
		}else{
			leafNodeStruct l;
			off_t t = this->leafNode->prev;
			BPlusTree bplustree(this->ixfileHandle, false);
			bplustree.readData(&l, t);
			len = l.children[l.n - 1].key.charLength;
			nlen = len - 1;
			//			cout<<"\n Length (2): " << nlen;
			memcpy((char*)key, &nlen, sizeof(int));
			memcpy((char*)key + sizeof(int), l.children[l.n - 1].key.keyString, nlen);
			break;
		}

		break;
	case TypeNone:
		return FAIL;
	}
	if(flag == true){
		this->leafNodePosition = -10;
		return PASS;
	}


	if(this->leafNodePosition == -1){
		return RBFM_EOF;
	}


	return PASS;
}

IXFileHandle::IXFileHandle() {
	ixReadPageCounter = 0;
	ixWritePageCounter = 0;
	ixAppendPageCounter = 0;
}

IXFileHandle::~IXFileHandle() {
}

RC IXFileHandle::collectCounterValues(unsigned &readPageCount,
		unsigned &writePageCount, unsigned &appendPageCount) {
	readPageCount = this->indexFile.readPageCounter;
	writePageCount = this->indexFile.writePageCounter;
	appendPageCount = this->indexFile.appendPageCounter;
	return PASS;
}

RC IXFileHandle::getNumberOfPages(FileHandle &fileHandle) {
	return fileHandle.getNumberOfPages();
}

RC IXFileHandle::readPage(PageNum pageNum, void *data, FileHandle &fileHandle) {
	ixReadPageCounter = ixReadPageCounter + 1;
	return fileHandle.readPage(pageNum, data);
}

RC IXFileHandle::writePage(PageNum pageNum, void *data,
		FileHandle &fileHandle) {
	ixWritePageCounter = ixWritePageCounter + 1;
	return fileHandle.readPage(pageNum, data);
}

RC IXFileHandle::appendPage(const void *data, FileHandle &fileHandle) {
	ixAppendPageCounter = ixAppendPageCounter + 1;
	return fileHandle.appendPage(data);
}


OPERATOR_KEYCMP(internalNodeKeyStruct)
OPERATOR_KEYCMP(leafNodeRecord)

inline internalNodeKeyStruct *find(internalNodeStruct &node, const indexKey &key) {
	return upper_bound(node.children, node.children + node.n - 1, key);
}
inline leafNodeRecord *find(leafNodeStruct &node, const indexKey &key) {
	//	return lower_bound(begin(node), end(node), key);
	return lower_bound(node.children, node.children + node.n, key);
}

BPlusTree::BPlusTree(IXFileHandle& bPlusreeFileHandle, bool createIndexFile)//: fp(NULL), fp_level(0)
{
	this->_pf_manager_bplustree = PagedFileManager::instance();
	this->indexFilePath = bPlusreeFileHandle.indexFile.currentFileName;
	this->_pf_manager_bplustree->openFile(indexFilePath, bPlusreeFileHandle.indexFile);
	this->bFileHandle = &bPlusreeFileHandle;
	buffer = (char*)malloc(PAGE_SIZE);
	if (!createIndexFile)
		if (readData(&indexCatalogInfo, META_INFORMATION_START) != 0){
			//			createIndexFile = true;
		}
	if (createIndexFile) {
		BPlusTtreeInit(bPlusreeFileHandle);
	}
}

BPlusTree::~BPlusTree(){
	free(buffer);
}

int BPlusTree::getBPlusTreeOrder(Attribute &attribute, void *key){
	AttrType type;
	type = attribute.type;
	int value;
	switch(type){
	case TypeInt:
		break;
	case TypeReal:
		break;
	case TypeVarChar:
		break;
	}

}

int BPlusTree::search_range(indexKey *left, const indexKey &right,
		leafValue &recordIdentifiers, bool leftKeyInclusive,
		bool rightKeyInclusive, leafNodeStruct *& leafNode, int &n,
		bool *next) const {
	IndexCatalogInfo metaInfo;
	readData(&metaInfo, META_INFORMATION_START);
	off_t off_left;
	indexKey a;
	indexKey b;
	int check;

//	if((unsigned)lcount >= metaInfo.leaf_node_num){
//		return -1;
//	}

	if (leafNode->isNull == true) {
		leafNode->isNull = false;

		if (checkCondition(*left, right) > 0) {
			return -1;
		}
		if (left == NULL) {
			off_left = metaInfo.leaf_offset;

		}
		off_left = searchLeafNodeLowerBound(*left);
		readData(leafNode, off_left);
		leafNode->isNull = false;

		while (1) {

			a = leafNode->children[n].key;
			b = *left;
			check = checkConditionForSearchRange(a, b);
			if (check >= 0)
				break;
			else
				n++;
		}
	}

	if((unsigned)n >= leafNode->n){
		return -1;
	}

	//	if (n == 0)
	//		return 1;

	if (leftKeyInclusive) {

		a = leafNode->children[n].key;
		b = right;
		//		check = checkConditionForSearchRange(a, b);
		check = checkCondition(leafNode->children[n].key, right);
		if (!rightKeyInclusive) {

			if (check >= 0) {
				//n = -1;
				return -1;
			}
		}

		if (rightKeyInclusive) {
			if (check > 0) {
				//	n = -1;
				return -1;
			}
		}

		recordIdentifiers.pageNumber =
				leafNode->children[n].valueLeaf.pageNumber;
		recordIdentifiers.slotNumber =
				leafNode->children[n].valueLeaf.slotNumber;
		if (n < leafNode->n - 1) {
			n = n + 1;

		} else {
			if (leafNode->next == 0 && n == 0) {
				n = -5;
				return 1;
			}
			if (leafNode->next == 0) {
				//n = -1;
				n += 2* BCONST;
				return 1;
			}
			n = 0;
			if (leafNode->next != 0){
				readData(leafNode, leafNode->next);
				lcount++;
				leafNode->isNull = false;
			}

		}
		return 1;

	}

	else {


		while (1) {

			if (n < leafNode->n - 1) {
				isInLeaf = true;
			}

			while (isInLeaf) {

				if (n >= leafNode->n) {
					isInLeaf = false;
					break;
				}

				else if ((checkConditionForSearchRange(
						leafNode->children[n].key, *left)) > 0) {
					flag = true;
					break;
				}

				else
					n++;

			}

			if (flag == true)
				break;
			else {
				if(n ==0 && leafNode->next == 0){
					break;
				}
				n = 0;
				if (leafNode->next == 0) {
					//n = -1;

					return -1;
				} else{
					readData(leafNode, leafNode->next);
					lcount++;
					leafNode->isNull = false;
				}
			}
		}
		if (leafNode != NULL) {
			if (n == -1)
				return -1;
			a = leafNode->children[n].key;
			b = right;
			check = checkConditionForSearchRange(a, b);

			if (!rightKeyInclusive) {

				if (check >= 0) {
					//n = -1;
					return -1;
				}
			}

			if (rightKeyInclusive) {
				if (check > 0) {
					//n = -1;
					return -1;
				}
			}
			recordIdentifiers.pageNumber =
					leafNode->children[n].valueLeaf.pageNumber;
			recordIdentifiers.slotNumber =
					leafNode->children[n].valueLeaf.slotNumber;
			if (n < leafNode->n - 1) {
				n = n + 1;

			} else {
				if (leafNode->next == 0 && n == 0) {
					n = -5;
					return 1;
				}

				if (leafNode->next == 0) {
					n += 2* BCONST;
					return 1;
				}
				n = 0;
				if (leafNode->next != 0){
					readData(leafNode, leafNode->next);
					lcount++;
					leafNode->isNull = false;
				}

			}
			return 1;
		}

	}
}


int BPlusTree::remove(const indexKey& key, RID rid)
{
	internalNodeStruct parent;
	leafNodeStruct leaf;
	off_t parent_off = getParentOfLeaf(key);
	readData(&parent, parent_off);
	internalNodeKeyStruct *where = find(parent, key);
	off_t offset = where->child;
	readData(&leaf, offset);
	if (!binary_search(leaf.children, leaf.children + leaf.n, key)){
		return -1;
	}
	size_t min_n = indexCatalogInfo.leaf_node_num == 1 ? 0 : indexCatalogInfo.order / 2;
	leafNodeRecord *to_delete = find(leaf, key);
	int b = 0;
	bool flagFound = false;
	readData(&leaf, indexCatalogInfo.leaf_offset);
	if(indexCatalogInfo.leaf_node_num == 1){
		for(unsigned int i = 0; i < leaf.n; i++){
			if(checkCondition(key, leaf.children[i].key) < 0){
				break;
			}
			if(leaf.children[i].valueLeaf.pageNumber == rid.pageNum && leaf.children[i].valueLeaf.slotNumber == rid.slotNum){
				to_delete = &leaf.children[i];
				flagFound = true;
				break;
			}
		}
	}else{
		do{
			for(unsigned int i = 0; i < leaf.n; i++){
				if(checkCondition(key, leaf.children[i].key) < 0){
					b = 1;
					break;
				}
				if(leaf.children[i].valueLeaf.pageNumber == rid.pageNum && leaf.children[i].valueLeaf.slotNumber == rid.slotNum){
					to_delete = &leaf.children[i];
					b = 1;
					flagFound = true;
					break;
				}
			}
			if(b == 1){
				break;
			}
			readData(&leaf, leaf.next);
		}while(leaf.next !=0);
		for(unsigned int i = 0; i < leaf.n; i++){
			if(checkCondition(key, leaf.children[i].key) < 0){
				break;
			}
			if(leaf.children[i].valueLeaf.pageNumber == rid.pageNum && leaf.children[i].valueLeaf.slotNumber == rid.slotNum){
				to_delete = &leaf.children[i];
				flagFound = true;
				break;
			}
		}
	}

	if(flagFound == false){
		return -1;
	}

	std::copy(to_delete + 1, leaf.children + leaf.n, to_delete);
	leaf.n--;

	//merge       , borrow
	if (leaf.n < min_n) {
		// left
		bool borrowed = false;
		if (leaf.prev != 0)
			borrowed = borrowKey(false, leaf);

		// right
		if (!borrowed && leaf.next != 0)
			borrowed = borrowKey(true, leaf);

		// finally we merge
		if (!borrowed) {

			indexKey index_key;

			if (where == parent.children + parent.n - 1) {
				leafNodeStruct prev;
				readData(&prev, leaf.prev);
				index_key = prev.children->key;

				mergeLeafNodeStruct(&prev, &leaf);
				removeNodeFromTree(&prev, &leaf);
				writeData(&prev, leaf.prev);
			} else {
				leafNodeStruct next;
				readData(&next, leaf.next);
				index_key = leaf.children->key;

				mergeLeafNodeStruct(&leaf, &next);
				removeNodeFromTree(&leaf, &next);
				writeData(&leaf, offset);
			}

			// remove parent's key
			removeFromIndexFile(parent_off, parent, index_key);
		} else {
			writeData(&leaf, offset);
		}
	} else {
		writeData(&leaf, offset);
	}

	return 0;
}

int BPlusTree::insert(const indexKey& key, leafValue value)
{
	off_t parent = getParentOfLeaf(key);
	off_t offset = searchLeafNode(parent, key);
	leafNodeStruct leaf;
	readData(&leaf, offset);
	//		if (binary_search(begin(leaf), end(leaf), key)){
	//			cout<<"\n DUPLICATE KEY";
	//			return 1;
	//		}
	if (leaf.n == indexCatalogInfo.order) {
		leafNodeStruct new_leaf;
		newNodeCreate(offset, &leaf, &new_leaf);
		size_t point = leaf.n / 2;
		bool place_right = checkCondition(key, leaf.children[point].key) > 0;
		if (place_right)
			++point;
		std::copy(leaf.children + point, leaf.children + leaf.n,
				new_leaf.children);
		new_leaf.n = leaf.n - point;
		leaf.n = point;
		if (place_right)
			insertKeyValueDirectly(&new_leaf, key, value);
		else
			insertKeyValueDirectly(&leaf, key, value);
		writeData(&leaf, offset);
		writeData(&new_leaf, leaf.next);
		insertIndexKey(parent, new_leaf.children[0].key,
				offset, leaf.next);
	} else {
		insertKeyValueDirectly(&leaf, key, value);
		writeData(&leaf, offset);
	}
	//	this->printLeaf();
	return 0;
}

void BPlusTree::removeFromIndexFile(off_t offset, internalNodeStruct &node,
		const indexKey &key)
{
	size_t min_n = indexCatalogInfo.root_offset == offset ? 1 : indexCatalogInfo.order / 2;

	// remove key
	indexKey index_key = node.children->key;
	internalNodeKeyStruct *to_delete = find(node, key);
	if (to_delete != node.children + node.n) {
		(to_delete + 1)->child = to_delete->child;
		std::copy(to_delete + 1, node.children + node.n, to_delete);
	}
	node.n--;

	// remove to only one key
	if (node.n == 1 && indexCatalogInfo.root_offset == offset &&
			indexCatalogInfo.internal_node_num != 1)
	{
		unalloc(&node, indexCatalogInfo.root_offset);
		indexCatalogInfo.height--;
		indexCatalogInfo.root_offset = node.children[0].child;
		writeData(&indexCatalogInfo, META_INFORMATION_START);
		return;
	}

	// merge or borrow
	if (node.n < min_n) {
		internalNodeStruct parent;
		readData(&parent, node.parent);

		// first borrow from left
		bool borrowed = false;
		if (offset != parent.children->child)
			borrowed = borrowKey(false, node, offset);

		// then borrow from right
		if (!borrowed && offset != (parent.children + parent.n - 1)->child)
			borrowed = borrowKey(true, node, offset);

		// finally we merge
		if (!borrowed) {

			if (offset == (parent.children + parent.n - 1)->child) {
				// if leaf is last element then merge | prev | leaf |
				internalNodeStruct prev;
				readData(&prev, node.prev);

				// merge
				internalNodeKeyStruct *where = find(parent, prev.children->key);
				updateParentForChild(node.children, node.children + node.n, node.prev);
				mergeKeys(where, prev, node);
				writeData(&prev, node.prev);
			} else {
				// else merge | leaf | next |
				internalNodeStruct next;
				readData(&next, node.next);

				// merge
				internalNodeKeyStruct *where = find(parent, index_key);
				updateParentForChild(next.children, next.children + next.n, offset);
				mergeKeys(where, node, next);
				writeData(&node, offset);
			}

			// remove parent's key
			removeFromIndexFile(node.parent, parent, index_key);
		} else {
			writeData(&node, offset);
		}
	} else {
		writeData(&node, offset);
	}
}

bool BPlusTree::borrowKey(bool from_right, internalNodeStruct &borrower,
		off_t offset)
{
	typedef typename internalNodeStruct::child_t child_t;

	off_t lender_off = from_right ? borrower.next : borrower.prev;
	internalNodeStruct lender;
	readData(&lender, lender_off);
	if (lender.n != indexCatalogInfo.order / 2) {
		child_t where_to_lend, where_to_put;

		internalNodeStruct parent;
		if (from_right) {
			where_to_lend = lender.children;
			where_to_put = borrower.children + borrower.n;

			readData(&parent, borrower.parent);
			child_t where = lower_bound(parent.children, parent.children + parent.n - 1,
					(borrower.children + borrower.n -1)->key);
			where->key = where_to_lend->key;
			writeData(&parent, borrower.parent);
		} else {
			where_to_lend = lender.children + lender.n - 1;
			where_to_put = borrower.children;

			readData(&parent, lender.parent);
			child_t where = find(parent, lender.children->key);
			where_to_put->key = where->key;
			where->key = (where_to_lend - 1)->key;
			writeData(&parent, lender.parent);
		}
		std::copy_backward(where_to_put, borrower.children + borrower.n, borrower.children + borrower.n + 1);
		*where_to_put = *where_to_lend;
		borrower.n++;

		// erase
		updateParentForChild(where_to_lend, where_to_lend + 1, offset);
		std::copy(where_to_lend + 1, lender.children + lender.n, where_to_lend);
		lender.n--;
		writeData(&lender, lender_off);
		return true;
	}

	return false;
}

bool BPlusTree::borrowKey(bool from_right, leafNodeStruct &borrower)
{
	off_t lender_off = from_right ? borrower.next : borrower.prev;
	leafNodeStruct lender;
	readData(&lender, lender_off);
	if (lender.n != indexCatalogInfo.order / 2) {
		child_t where_to_lend, where_to_put;
		if (from_right) {
			where_to_lend = lender.children;
			where_to_put = borrower.children + borrower.n;
			change_parent_child(borrower.parent, (borrower.children)->key,
					lender.children[1].key);
		} else {
			where_to_lend = lender.children + lender.n - 1;
			where_to_put = borrower.children;
			change_parent_child(lender.parent, (lender.children)->key,
					where_to_lend->key);
		}
		std::copy_backward(where_to_put, borrower.children + borrower.n, borrower.children + borrower.n + 1);
		*where_to_put = *where_to_lend;
		borrower.n++;
		std::copy(where_to_lend + 1, lender.children + lender.n, where_to_lend);
		lender.n--;
		writeData(&lender, lender_off);
		return true;
	}

	return false;
}

void BPlusTree::change_parent_child(off_t parent, const indexKey &o,
		const indexKey &n)
{
	internalNodeStruct node;
	readData(&node, parent);

	internalNodeKeyStruct *w = find(node, o);

	w->key = n;
	writeData(&node, parent);
	if (w == node.children + node.n - 1) {
		change_parent_child(node.parent, o, n);
	}
}

void BPlusTree::mergeLeafNodeStruct(leafNodeStruct *left, leafNodeStruct *right)
{
	std::copy(right->children, right->children + right->n, left->children + left->n);
	left->n += right->n;
}

void BPlusTree::mergeKeys(internalNodeKeyStruct *where, internalNodeStruct &node, internalNodeStruct &next)
{
	std::copy(next.children, next.children + next.n, node.children + node.n);
	node.n += next.n;
	removeNodeFromTree(&node, &next);
}

void BPlusTree::insertKeyValueDirectly(leafNodeStruct *leaf, const indexKey &key, const leafValue &value)
{
	leafNodeRecord *where = upper_bound((leaf)->children, (leaf)->children + (leaf)->n, key);
	std::copy_backward(where, (leaf)->children + (leaf)->n, (leaf)->children + (leaf)->n + 1);

	where->key = key;
	where->valueLeaf.pageNumber = value.pageNumber;
	where->valueLeaf.slotNumber = value.slotNumber;
	leaf->n++;
}

void BPlusTree::insertIndexKey(off_t offset, const indexKey &key,
		off_t old, off_t after)
{
	if (offset == 0) {
		internalNodeStruct root;
		root.next = root.prev = root.parent = 0;
		indexCatalogInfo.root_offset = alloc(&root);
		indexCatalogInfo.height++;
		root.n = 2;
		root.children[0].key = key;
		root.children[0].child = old;
		root.children[1].child = after;

		writeData(&indexCatalogInfo, META_INFORMATION_START);
		writeData(&root, indexCatalogInfo.root_offset);
		updateParentForChild(root.children, root.children + root.n,
				indexCatalogInfo.root_offset);
		return;
	}

	internalNodeStruct node;
	readData(&node, offset);

	if (node.n == indexCatalogInfo.order) {
		internalNodeStruct new_node;
		newNodeCreate(offset, &node, &new_node);
		size_t point = (node.n - 1) / 2;
		bool place_right = checkCondition(key, node.children[point].key) > 0;
		if (place_right)
			++point;
		// prevent key to be right middle_key
		if (place_right && checkCondition(key, node.children[point].key) < 0)
			point--;
		indexKey middle_key = node.children[point].key;
		std::copy(node.children + point + 1, node.children + node.n, new_node.children);
		new_node.n = node.n - point - 1;
		node.n = point + 1;
		if (place_right)
			insertIndexWithoutSplit(new_node, key, after);
		else
			insertIndexWithoutSplit(node, key, after);
		writeData(&node, offset);
		writeData(&new_node, node.next);
		updateParentForChild(new_node.children, new_node.children + new_node.n, node.next);
		insertIndexKey(node.parent, middle_key, offset, node.next);
	} else {
		insertIndexWithoutSplit(node, key, after);
		writeData(&node, offset);
	}
}

void BPlusTree::insertIndexWithoutSplit(internalNodeStruct &node,
		const indexKey &key, off_t value)
{
	internalNodeKeyStruct *where = upper_bound(node.children, node.children + node.n - 1, key);
	std::copy_backward(where, node.children + node.n, node.children + node.n + 1);
	where->key = key;
	where->child = (where + 1)->child;
	(where + 1)->child = value;
	node.n++;
}

void BPlusTree::updateParentForChild(internalNodeKeyStruct *begin, internalNodeKeyStruct *end,
		off_t parent)
{
	internalNodeStruct node;
	while (begin != end) {
		readData(&node, begin->child);
		node.parent = parent;
		writeData(&node, begin->child, LeafNodeStructMinusChildren);
		++begin;
	}
}

off_t BPlusTree::getParentOfLeaf(const indexKey &key) const
{
	off_t org = indexCatalogInfo.root_offset;
	int height = indexCatalogInfo.height;
	while (height > 1) {
		internalNodeStruct node;
		readData(&node, org);
		internalNodeKeyStruct *i = upper_bound(node.children, node.children + node.n - 1, key);
		org = i->child;
		--height;
	}

	return org;
}

off_t BPlusTree::searchLeafNode(off_t index, const indexKey &key) const
{
	internalNodeStruct node;
	readData(&node, index);

	internalNodeKeyStruct *i = upper_bound(node.children, node.children + node.n - 1, key);
	return i->child;
}

off_t BPlusTree::searchLeafNodeLowerBound(off_t index, const indexKey &key) const
{
	internalNodeStruct node;
	readData(&node, index);

	internalNodeKeyStruct *i = lower_bound(node.children, node.children + node.n - 1, key);
	return i->child;
}

template<class T>
void BPlusTree::newNodeCreate(off_t offset, T *node, T *next)
{
	next->parent = node->parent;
	next->next = node->next;
	next->prev = offset;
	node->next = alloc(next);
	if (next->next != 0) {
		T old_next;
		readData(&old_next, next->next, LeafNodeStructMinusChildren);
		old_next.prev = node->next;
		writeData(&old_next, next->next, LeafNodeStructMinusChildren);
	}

	writeData(&indexCatalogInfo, META_INFORMATION_START);
}

template<class T>
void BPlusTree::removeNodeFromTree(T *prev, T *node)
{
	unalloc(node, prev->next);
	prev->next = node->next;
	if (node->next != 0) {
		T next;
		readData(&next, node->next, LeafNodeStructMinusChildren);
		next.prev = node->prev;
		writeData(&next, node->next, LeafNodeStructMinusChildren);
	}
	writeData(&indexCatalogInfo, META_INFORMATION_START);
}

void BPlusTree::BPlusTtreeInit(IXFileHandle& bPlusreeFileHandle)
{
	bzero(&indexCatalogInfo, sizeof(IndexCatalogInfo));
	indexCatalogInfo.order = BCONST;
	indexCatalogInfo.value_size = sizeof(leafValue);
	indexCatalogInfo.key_size = sizeof(indexKey);
	indexCatalogInfo.height = 1;
	indexCatalogInfo.freeSpacePointer = META_INFO_END;

	internalNodeStruct root;
	root.next = root.prev = root.parent = 0;
	indexCatalogInfo.root_offset = alloc(&root);

	leafNodeStruct leaf;
	leaf.next = leaf.prev = 0;
	leaf.parent = indexCatalogInfo.root_offset;
	indexCatalogInfo.leaf_offset = root.children[0].child = alloc(&leaf);

	writeData(&indexCatalogInfo, META_INFORMATION_START);
	writeData(&root, indexCatalogInfo.root_offset);
	writeData(&leaf, root.children[0].child);
}

indexKey& BPlusTree::getFirstLeafKey(){
	leafNodeStruct leaf;
	readData(&leaf, indexCatalogInfo.leaf_offset);
	return leaf.children[0].key;

}

indexKey& BPlusTree::getLastLeafKey(){
	leafNodeStruct leaf;
	readData(&leaf, indexCatalogInfo.leaf_offset);
	while(leaf.next != 0){
		readData(&leaf, leaf.next);
	}
	return leaf.children[leaf.n-1].key;
}


void BPlusTree::printLeaf(){
	cout<<"\n ******************";
	leafNodeStruct leaf;
	readData(&leaf, indexCatalogInfo.leaf_offset);
	int t = 0;
	int count = 1;
	while(leaf.next != 0){
		cout<<"\n Leaf number : " << count;
		for(t = 0; t < leaf.n; t++){
			//			cout<< " \n VALUE : "  << leaf.children[t].key.integerValue;
			cout<< " \n Key " << leaf.children[t].key.integerValue << "VALUE : "  << leaf.children[t].valueLeaf.pageNumber << " and " << leaf.children[t].valueLeaf.slotNumber;
		}
		readData(&leaf, leaf.next);
		count += 1;
	}
	cout<<"\n lef number : " << count;
	for(t = 0; t < leaf.n; t++){
		cout<< " \n Key " << leaf.children[t].key.integerValue << "VALUE : "  << leaf.children[t].valueLeaf.pageNumber << " and " << leaf.children[t].valueLeaf.slotNumber;
	}
}
