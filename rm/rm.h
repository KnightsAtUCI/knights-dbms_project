

#ifndef _rm_h_
#define _rm_h_

#include <string>
#include <vector>
#include <string.h>
#include <stdlib.h>
#include <iostream>


#include "../rbf/rbfm.h"
#include "../ix/ix.h"

using namespace std;

# define RM_EOF (-1)  // end of a scan operator


class RM_IndexScanIterator;

// RM_ScanIterator is an iteratr to go through tuples
class RM_ScanIterator {
public:
	RM_ScanIterator() :
		value(NULL),fileName(""), tableName(""), conditionAttribute(""), compOp(NO_OP), attributeNames(NULL) {
	}

	~RM_ScanIterator() {
		rbfmsi.close();
	}

	// "data" follows the same format as RelationManager::insertTuple()
	RC getNextTuple(RID &rid, void *data) ;
	RC close() ;


public:
	RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
	RBFM_ScanIterator rbfmsi;
	string fileName;
	string tableName;
	FileHandle fileHandle;
	vector<Attribute> recordDescriptor;
	string conditionAttribute;
	CompOp compOp;                  // Comparison type such as "<" and "="
	const void *value;                    // used in the comparison
	vector<string> attributeNames;
	RID _rid;
};


// Relation Manager
class RelationManager
{
public:

	static RelationManager* instance();

	RC createCatalog();

	RC deleteCatalog();

	RC createTable(const string &tableName, const vector<Attribute> &attrs);

	RC deleteTable(const string &tableName);

	RC getAttributes(const string &tableName, vector<Attribute> &attrs);

	RC insertTuple(const string &tableName, const void *data, RID &rid);

	RC deleteTuple(const string &tableName, const RID &rid);

	RC updateTuple(const string &tableName, const void *data, const RID &rid);

	RC readTuple(const string &tableName, const RID &rid, void *data);

	// Print a tuple that is passed to this utility method.
	// The format is the same as printRecord().
	RC printTuple(const vector<Attribute> &attrs, const void *data);

	RC readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data);

	// Scan returns an iterator to allow the caller to go through the results one by one.
	// Do not store entire results in the scan iterator.
	RC scan(const string &tableName,
			const string &conditionAttribute,
			const CompOp compOp,                  // comparison type such as "<" and "="
			const void *value,                    // used in the comparison
			const vector<string> &attributeNames, // a list of projected attributes
			RM_ScanIterator &rm_ScanIterator);

	RC indexScan(const string &tableName,
			const string &attributeName,
			const void *lowKey,
			const void *highKey,
			bool lowKeyInclusive,
			bool highKeyInclusive,
			RM_IndexScanIterator &rm_IndexScanIterator);

	// Extra credit work (10 points)

public:
	RC addAttribute(const string &tableName, const Attribute &attr);

	RC dropAttribute(const string &tableName, const string &attributeName);

	RC createIndex(const string &tableName, const string &attributeName);

	RC destroyIndex(const string &tableName, const string &attributeName);

protected:
	RelationManager();
	~RelationManager();

public:

	string catalogName;
	string columnName;
	static RelationManager *_rm;
	RecordBasedFileManager *rbfm;
	//IndexManager *index;
	IndexManager *idx = IndexManager::instance();
	FileHandle catalogHandle;
	FileHandle columnHandle;
	vector<Attribute> Tables;
	vector<Attribute> Columns;
	vector<string>    tableAttr;					// stores all the table attribute.names
	vector<string>    columnAttr;					// stores all the column attribute.names {used for scan operators}
	RC tablesOpenHandler();
	RC tablesCloseHandler();
	void indexEntries(vector<RID> index, const string &tableName);
	std::string attrEnumToString(AttrType attr);
	int tableNameToTableId(const string &tableName);
	//  string tableNameToFileName(const string &tableName);
	RC columnsToRecordDescriptor(const void *data, Attribute &recordDescriptor);

};

class RM_IndexScanIterator {
public:
	IX_ScanIterator ixs;
	RM_IndexScanIterator() {};
	~RM_IndexScanIterator() {
		close();
	};

	RC getNextEntry(RID &rid, void *key);
	RC close();

};

#endif
