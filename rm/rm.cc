#include "rm.h"
#define PASS 0
#define FAIL -1
#define NOT_NULL_BYTE1 0x2

RelationManager* RelationManager::_rm = 0;

RelationManager* RelationManager::instance() {
	if (!_rm)
		_rm = new RelationManager();

	return _rm;
}

RelationManager::RelationManager() {
	rbfm = RecordBasedFileManager::instance();
	catalogName = "Tables";
	columnName = "Columns";

	Attribute attr;

	attr.name = "table-id";
	attr.type = TypeInt;
	attr.length = 4;
	Tables.push_back(attr);
	tableAttr.push_back(attr.name);

	attr.name = "table-name";
	attr.type = TypeVarChar;
	attr.length = 50;
	Tables.push_back(attr);
	tableAttr.push_back(attr.name);

	attr.name = "file-name";
	attr.type = TypeVarChar;
	attr.length = 50;
	Tables.push_back(attr);
	tableAttr.push_back(attr.name);

	attr.name = "table-id";
	attr.type = TypeInt;
	attr.length = 4;
	Columns.push_back(attr);
	columnAttr.push_back(attr.name);

	attr.name = "column-name";
	attr.type = TypeVarChar;
	attr.length = 50;
	Columns.push_back(attr);
	columnAttr.push_back(attr.name);

	attr.name = "column-type";
	attr.type = TypeInt;
	attr.length = 4;
	Columns.push_back(attr);
	columnAttr.push_back(attr.name);

	attr.name = "column-length";
	attr.type = TypeInt;
	attr.length = 4;
	Columns.push_back(attr);
	columnAttr.push_back(attr.name);

	attr.name = "column-position";
	attr.type = TypeInt;
	attr.length = 4;
	Columns.push_back(attr);
	columnAttr.push_back(attr.name);
}

RelationManager::~RelationManager() {
}

RC RelationManager::tablesOpenHandler() {

	int status1 = rbfm->openFile(catalogName + ".tbl", catalogHandle);

	int status2 = rbfm->openFile(columnName + ".tbl", columnHandle);

	if (status1 == 0 && status2 == 0)
		return 0;

	else
		return -1;
}

RC RelationManager::tablesCloseHandler() {

	int status1 = rbfm->closeFile(catalogHandle);

	int status2 = rbfm->closeFile(columnHandle);

	if (status1 == 0 && status2 == 0)

		return 0;

	else
		return -1;
}

RC RelationManager::createCatalog() {

	int tableId = 1;
	int noOfNullbytes = 1;
	rbfm->createFile(catalogName + ".tbl");
	rbfm->createFile(columnName + ".tbl");

	tablesOpenHandler();					// opening tables and columns file
	RID rid;
	int tableNameLength;
	void *data;
	const char *tableNameChar;
	RC status;

	// ------------- inserting tables details into tables table ----------------
	data = (char *) malloc(PAGE_SIZE);

	memset((char *) data, 0, noOfNullbytes);

	memcpy((char *) data + noOfNullbytes, &tableId, sizeof(int));	//table id

	tableNameLength = catalogName.length();
	tableNameChar = catalogName.c_str();

	memcpy((char *) data + noOfNullbytes + sizeof(int), &tableNameLength,
			sizeof(int));
	memcpy((char *) data + noOfNullbytes + 2 * sizeof(int),
			((char*) ((char *) tableNameChar)), tableNameLength);   //table name

	memcpy((char *) data + noOfNullbytes + 2 * sizeof(int) + tableNameLength,
			&tableNameLength, sizeof(int));
	memcpy((char *) data + noOfNullbytes + 3 * sizeof(int) + tableNameLength,
			((char*) ((char *) tableNameChar)), tableNameLength);	//file name

	status = rbfm->insertRecord(catalogHandle, Tables, data, rid);

	if (status != 0)
		return -1;

	// ------------- inserting columns details into tables table ----------------

	free(data);

	data = (char *) malloc(PAGE_SIZE);
	tableId = 2;

	memset((char *) data, 0, noOfNullbytes);

	memcpy((char *) data + noOfNullbytes, &tableId, sizeof(int));	//table id

	tableNameLength = columnName.length();
	tableNameChar = columnName.c_str();

	memcpy((char *) data + noOfNullbytes + sizeof(int), &tableNameLength,
			sizeof(int));
	memcpy((char *) data + noOfNullbytes + 2 * sizeof(int),
			((char*) ((char *) tableNameChar)), tableNameLength);   //table name

	memcpy((char *) data + noOfNullbytes + 2 * sizeof(int) + tableNameLength,
			&tableNameLength, sizeof(int));
	memcpy((char *) data + noOfNullbytes + 3 * sizeof(int) + tableNameLength,
			((char*) ((char *) tableNameChar)), tableNameLength);	//file name

	status = rbfm->insertRecord(catalogHandle, Tables, data, rid);

	if (status != 0)
		return -1;

	//------------  inserting table's details into columns table--------------
	for (int it = 0; it < Tables.size(); it++) {

		noOfNullbytes = 1;

		data = (char *) malloc(PAGE_SIZE);
		int position;
		tableId = 1;

		memset(data, 0, noOfNullbytes);				//Null bytes

		Attribute atr;
		atr = Tables[it];
		int nameLength = atr.name.length();
		const char *name = atr.name.c_str();

		//const char *type = attrEnumToString(atr.type).c_str();
		int type = atr.type;
		//table-id
		memcpy((char *) data + noOfNullbytes, &tableId, sizeof(int));

		//column-name
		memcpy((char *) data + noOfNullbytes + sizeof(int), &nameLength,
				sizeof(int));

		memcpy((char *) data + (sizeof(int) * 2) + noOfNullbytes,
				((char*) ((char *) name)), nameLength);

		// column-type
		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 2) + nameLength,
				&type, sizeof(int));

		// column-length

		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 3) + nameLength,
				&atr.length, sizeof(int));

		//column-position
		position = it + 1;
		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 4) + nameLength,
				&position, sizeof(int));

		status = rbfm->insertRecord(columnHandle, Columns, data, rid);
		if (status != 0) {

			tablesCloseHandler();
			return -1;
		}
		free(data);

	}

	//------------  inserting column's details into columns table--------------

	for (int it = 0; it < Columns.size(); it++) {

		noOfNullbytes = 1;

		data = (char *) malloc(PAGE_SIZE);
		int position;
		tableId = 2;

		memset(data, 0, noOfNullbytes);				//Null bytes

		Attribute atr;
		atr = Columns[it];
		int nameLength = atr.name.length();
		const char *name = atr.name.c_str();

		//const char *type = attrEnumToString(atr.type).c_str();
		int type = atr.type;

		//table-id
		memcpy((char *) data + noOfNullbytes, &tableId, sizeof(int));

		//column-name
		memcpy((char *) data + noOfNullbytes + sizeof(int), &nameLength,
				sizeof(int));

		memcpy((char *) data + (sizeof(int) * 2) + noOfNullbytes,
				((char*) ((char *) name)), nameLength);

		// column-type
		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 2) + nameLength,
				&type, sizeof(int));

		// column-length

		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 3) + nameLength,
				&atr.length, sizeof(int));

		//column-position
		position = it + 1;
		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 4) + nameLength,
				&position, sizeof(int));

		status = rbfm->insertRecord(columnHandle, Columns, data, rid);
		if (status != 0) {

			tablesCloseHandler();
			return -1;
		}
		free(data);

	}

	tablesCloseHandler();    // closing tables and columns files

	return 0;
}

RC RelationManager::deleteCatalog() {

	RC status1, status2;

	status1 = rbfm->destroyFile(catalogName + ".tbl");
	status2 = rbfm->destroyFile(columnName + ".tbl");

	if (status1 != 0 || status2 != 0)
		return -1;

	else
		return 0;

}

std::string RelationManager::attrEnumToString(AttrType attr) {

	switch (attr) {
	case TypeVarChar:
		return "TypeVarChar";
	case TypeInt:
		return "TypeInt";
	case TypeReal:
		return "TypeReal";
	default:
		return "";
	}

}

RC RelationManager::createTable(const string &tableName,
		const vector<Attribute> &attrs) {

	RBFM_ScanIterator rmsi;
	RID rid;

	void *data = (char *) malloc(PAGE_SIZE);
	short lengthOfTableRecord = 0;
	int noOfNullbytes = 1;
	if(tablesOpenHandler() == -1){
		return FAIL;
	}

	rbfm->scan(catalogHandle, Tables, "table-name", EQ_OP, tableName.c_str(),
			tableAttr, rmsi);

	for (; rmsi.getNextRecord(rid, data) != RBFM_EOF;) {

		tablesCloseHandler();
		free(data);
		return -1;
	}
	rmsi.close();
	rbfm->createFile(tableName + ".tbl");

	RBFM_ScanIterator rmsi2;
	int tableId = 0;
	rbfm->scan(catalogHandle, Tables, "table-name", NO_OP, tableName.c_str(),
			tableAttr, rmsi2);
	for (; rmsi2.getNextRecord(rid, data) != RBFM_EOF;) {

		tableId = *((int *) ((char *) data + noOfNullbytes));
	}
	tableId++;
	rmsi2.close();

	free(data);
	data = (char *) malloc(PAGE_SIZE);

	memset(data, 0, noOfNullbytes);				//Null bytes

	memcpy((char *) data + noOfNullbytes, &tableId, sizeof(int));	//table -id

	int tableLength = tableName.length();
	const char *tableNameChar = tableName.c_str();

	memcpy((char *) data + noOfNullbytes + sizeof(int), &tableLength,
			sizeof(int)); //table-name

	memcpy((char *) data + (sizeof(int) * 2) + noOfNullbytes,
			((char*) ((char *) tableNameChar)), tableLength);

	memcpy((char *) data + (sizeof(int) * 2) + tableLength + noOfNullbytes,
			&tableLength, sizeof(int));   							//file-name

	memcpy((char *) data + (sizeof(int) * 3) + tableLength + noOfNullbytes,
			((char*) ((char *) tableNameChar)), tableLength);

	RC status = rbfm->insertRecord(catalogHandle, Tables, data, rid);
	if (status != 0)
		return -1;

	free(data);

	for (int it = 0; it < attrs.size(); it++) {

		noOfNullbytes = 1;

		lengthOfTableRecord = 0;
		data = (char *) malloc(PAGE_SIZE);
		int position;

		memset(data, 0, noOfNullbytes);				//Null bytes

		Attribute atr;
		atr = attrs[it];
		int nameLength = atr.name.length();
		const char *name = atr.name.c_str();

		//const char *type = attrEnumToString(atr.type).c_str();
		int type = atr.type;
		//table-id
		memcpy((char *) data + noOfNullbytes, &tableId, sizeof(int));

		//column-name
		memcpy((char *) data + noOfNullbytes + sizeof(int), &nameLength,
				sizeof(int));

		memcpy((char *) data + (sizeof(int) * 2) + noOfNullbytes,
				((char*) ((char *) name)), nameLength);

		// column-type
		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 2) + nameLength,
				&type, sizeof(int));

		// column-length

		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 3) + nameLength,
				&atr.length, sizeof(int));

		//column-position
		position = it + 1;
		memcpy((char *) data + noOfNullbytes + (sizeof(int) * 4) + nameLength,
				&position, sizeof(int));

		status = rbfm->insertRecord(columnHandle, Columns, data, rid);
		if (status != 0) {

			tablesCloseHandler();
			return -1;
		}
		free(data);

	}

	tablesCloseHandler();

	return 0;
}

int RelationManager::tableNameToTableId(const string &tableName) {

	tablesOpenHandler();
	RBFM_ScanIterator rmsi;
	RID rid;
	int tableId = 0;
	int noOfNullbytes = 1;
	void *data = (char *) malloc(PAGE_SIZE);

	rbfm->scan(catalogHandle, Tables, "table-name", EQ_OP, tableName.c_str(),
			tableAttr, rmsi);
	for (; rmsi.getNextRecord(rid, data) != RBFM_EOF;) {
		tableId = *((int *) ((char *) data + noOfNullbytes));
	}
	rmsi.close();

	tablesCloseHandler();
	free(data);
	return tableId;
}

RC RelationManager::deleteTable(const string &tableName) {


	vector<Attribute> recordDescriptor;
	getAttributes(tableName, recordDescriptor);

	void *key5;
	key5 = (char *)malloc(PAGE_SIZE);
	int offset,size;
	tablesOpenHandler();
	for (int i = 0; i < recordDescriptor.size(); i++) {
		RID rid1;
		RBFM_ScanIterator rmsi2;

		int check = 0;

		rbfm->scan(catalogHandle, Tables, "table-name", NO_OP,
				tableName.c_str(), tableAttr, rmsi2);
		for (; rmsi2.getNextRecord(rid1, key5) != RBFM_EOF;) {

			offset = 5;

			size = *((int *) ((char *) key5 + offset));
			offset += 4;
			string name((char *) ((char *) key5 + offset), size);
			if (name.find(tableName) != std::string::npos
					&& name.find(recordDescriptor[i].name)
					!= std::string::npos) {
				check = 1;
				break;
			}

		}

		if(check == 1)
		{
			if((rbfm->deleteRecord(catalogHandle, Tables, rid1)) != 0)
				return FAIL;

			string s;
			s = tableName + "_" + recordDescriptor[i].name ;
			idx->destroyFile(s);


		}
	}

	free(key5);
	tablesCloseHandler();
	vector<RID> index;
	//indexEntries(&index);
	if (tableName.compare("Tables") == 0 || tableName.compare("Columns") == 0) {
		return FAIL;
	}
	int tableId = tableNameToTableId(tableName);

	tablesOpenHandler();
	// storing all RIDs of records to be deleted
	vector<RID> table;
	vector<RID> column;
	RID rid;
	RBFM_ScanIterator rmsi;

	void *data = (char *) malloc(PAGE_SIZE);

	rbfm->scan(catalogHandle, Tables, "table-id", EQ_OP, &tableId, tableAttr,
			rmsi);

	for (; rmsi.getNextRecord(rid, data) != RBFM_EOF;) {

		table.push_back(rid);
	}

	index.push_back(rid);

	rmsi.close();

	rbfm->scan(columnHandle, Columns, "table-id", EQ_OP, &tableId, columnAttr,
			rmsi);
	for (; rmsi.getNextRecord(rid, data) != RBFM_EOF;)
		column.push_back(rid);

	rmsi.close();

	//deleting all rids from catalog
	for (std::vector<RID>::const_iterator it = table.begin(); it != table.end();
			it++) {

		if (rbfm->deleteRecord(catalogHandle, Tables, *it) != 0) {

			return -1;
		}
	}

	//deleting all rids from columns table
	for (std::vector<RID>::const_iterator it = column.begin();
			it != column.end(); it++) {

		if (rbfm->deleteRecord(columnHandle, Columns, *it) != 0) {

			return -1;
		}
	}

	tablesCloseHandler();

	free(data);
	return rbfm->destroyFile(tableName + ".tbl");
}

RC RelationManager::columnsToRecordDescriptor(const void *data,
		Attribute &recordDescriptor) {

	int noOfNullbytes = 1;
	int offset = 0;
	int size = 0;

	// column-name
	offset = sizeof(int) + noOfNullbytes;

	size = *((int *) ((char *) data + offset));

	offset += sizeof(int);
	string name((char *) ((char *) data + offset), size);
	//string name((char *)(data + offset + sizeof(int)));

	//		memcpy(&name,(char *)(data + offset + sizeof(int)),size);

	recordDescriptor.name = name;

	// column-type
	offset += size;

	int type;
	memcpy(&type, (char *) data + offset, sizeof(int));

	if (type == TypeVarChar) {
		recordDescriptor.type = TypeVarChar;
	} else if (type == TypeReal) {
		recordDescriptor.type = TypeReal;
	} else if (type == TypeInt) {
		recordDescriptor.type = TypeInt;
	}

	// column-length
	offset += sizeof(int);
	recordDescriptor.length = *((int *) ((char *) data + offset));

	return 0;
}

RC RelationManager::getAttributes(const string &tableName,
		vector<Attribute> &attrs) {

	int tableId = tableNameToTableId(tableName);
	tablesOpenHandler();
	RBFM_ScanIterator rmsi;
	RID rid;
	void *data = (char *) malloc(PAGE_SIZE);
	vector<Attribute>::iterator it;
	Attribute columnAttribute;
	rbfm->scan(columnHandle, Columns, "table-id", EQ_OP, &tableId, columnAttr,
			rmsi);

	int indexOfAttrs = 0;
	for (; rmsi.getNextRecord(rid, data) != RBFM_EOF;) {
		this->columnsToRecordDescriptor(data, columnAttribute);
		attrs.insert(attrs.begin() + indexOfAttrs, columnAttribute);
		//			cout<<"\n\n Inserting at Index : " << indexOfAttrs <<" valu: " << attrs[indexOfAttrs].name;
		indexOfAttrs++;
	}
	rmsi.close();
	//attrs.insert(attrs.begin() + indexOfAttrs - 1, columnAttribute);

	tablesCloseHandler();
	free(data);

	return 0;
}

RC RelationManager::insertTuple(const string &tableName, const void *data,
		RID &rid) {

	int offset, size;
	int noOfNullbytes = 1;
	if (tableName.compare("Tables") == 0 || tableName.compare("Columns") == 0) {
		return FAIL;
	}

	FileHandle pFile;
	int statusOpen = rbfm->openFile(tableName + ".tbl", pFile);
	if (statusOpen != 0) {
		return FAIL;
	}

	char *tkey = (char*) malloc(PAGE_SIZE);
	vector<Attribute> recordDescriptor;
	getAttributes(tableName, recordDescriptor);
	char *key5, *key1;
	RC status = rbfm->insertRecord(pFile, recordDescriptor, data, rid);


	key5 = (char*) malloc(PAGE_SIZE);
	key1 = (char*) malloc(PAGE_SIZE);
	tablesOpenHandler();

	for (int i = 0; i < recordDescriptor.size(); i++) {
		RID rid1;
		RBFM_ScanIterator rmsi2;

		int check = 0;

		rbfm->scan(catalogHandle, Tables, "table-name", NO_OP,
				tableName.c_str(), tableAttr, rmsi2);
		for (; rmsi2.getNextRecord(rid1, key5) != RBFM_EOF;) {

			offset = 5;

			size = *((int *) ((char *) key5 + offset));
			offset += 4;
			string name((char *) ((char *) key5 + offset), size);
			if (name.find(tableName) != std::string::npos
					&& name.find(recordDescriptor[i].name)
					!= std::string::npos) {
				check = 1;
				break;
			}

		}

		if (check == 1) {
			rbfm->readAttribute(pFile, recordDescriptor, rid,
					recordDescriptor[i].name, key1);
			char c;
			int u = *(int*)(key1+1);

			memcpy(&c, key1, noOfNullbytes);
			if (recordDescriptor[i].type == 0
					|| recordDescriptor[i].type == 1) {
				memcpy(tkey, (char *) key1 + noOfNullbytes, sizeof(int));
			} else {
				offset = 1;
				size = *((int *) ((char *) key1 + offset));
				memcpy(tkey, &size, sizeof(int));
				offset += sizeof(int);

				memcpy(tkey + sizeof(int), (char *) key1 + offset, size);
			}
			IXFileHandle ixFileHandle;
			string snew = tableName + "_" + recordDescriptor[i].name;
			idx->openFile(snew.c_str(), ixFileHandle);
			int y = *(int*)(tkey);
//			cout<<"\n Insert (1)" << y ;
			idx->insertEntry(ixFileHandle, recordDescriptor[i], tkey, rid);
			idx->closeFile(ixFileHandle);
		}
		rmsi2.close();
	}
	free(key5);
	free(key1);
	free(tkey);

	rbfm->closeFile(pFile);
	tablesCloseHandler();
	return status;
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid) {

	int offset, size;
	int noOfNullbytes = 1;
	if (tableName.compare("Tables") == 0 || tableName.compare("Columns") == 0) {
		return FAIL;
	}

	FileHandle pFile;
	int statusOpen = rbfm->openFile(tableName + ".tbl", pFile);
	if (statusOpen != 0) {
		return FAIL;
	}
	vector<Attribute> recordDescriptor;
	getAttributes(tableName, recordDescriptor);
	void *data;
	data = (char *) malloc(PAGE_SIZE);

	rbfm->readRecord(pFile, recordDescriptor, rid, data);
	char *tkey = (char*) malloc(PAGE_SIZE);
	void *key, *key1;
	key = (char *) malloc(PAGE_SIZE);
	key1 = (char *) malloc(PAGE_SIZE);
	tablesOpenHandler();
	for (int i = 0; i < recordDescriptor.size(); i++) {
		RID rid1;
		RBFM_ScanIterator rmsi2;

		int check = 0;

		rbfm->scan(catalogHandle, Tables, "table-name", NO_OP,
				tableName.c_str(), tableAttr, rmsi2);
		for (; rmsi2.getNextRecord(rid1, key) != RBFM_EOF;) {

			offset = 5;

			size = *((int *) ((char *) key + offset));
			offset += 4;
			string name((char *) ((char *) key + offset), size);
			if (name.find(tableName) != std::string::npos
					&& name.find(recordDescriptor[i].name)
					!= std::string::npos) {
				check = 1;
				break;
			}

		}

		if (check == 1) {
			rbfm->readAttribute(pFile, recordDescriptor, rid,
					recordDescriptor[i].name, key1);
			char c;
			memcpy(&c, key1, noOfNullbytes);
			if (c == NOT_NULL_BYTE1) {
				if (recordDescriptor[i].type == 0
						|| recordDescriptor[i].type == 1) {
					memcpy(tkey, (char *) key1 + noOfNullbytes, sizeof(int));
				} else {
					offset = 1;
					size = *((int *) ((char *) key1 + offset));
					memcpy(tkey, &size, sizeof(int));
					offset += sizeof(int);

					memcpy(tkey + sizeof(int), (char *) key1 + offset, size);
				}
				//				IXFileHandle ixFileHandle;
				//				idx->deleteEntry(ixFileHandle, recordDescriptor[i], tkey, rid);
				IXFileHandle ixFileHandle;
				string snew = tableName + "_" + recordDescriptor[i].name;
				idx->openFile(snew.c_str(), ixFileHandle);
				idx->deleteEntry(ixFileHandle, recordDescriptor[i], tkey, rid);
				idx->closeFile(ixFileHandle);
			}
		}
		rmsi2.close();

	}
	RC status = rbfm->deleteRecord(pFile, recordDescriptor, rid);
	free(key);
	free(key1);
	free(tkey);
	free(data);
	tablesCloseHandler();
	rbfm->closeFile(pFile);
	return status;

}

RC RelationManager::updateTuple(const string &tableName, const void *data,

		const RID &rid) {



	int noOfNullbytes = 1;

	if (tableName.compare("Tables") == 0 || tableName.compare("Columns") == 0) {

		return FAIL;

	}



	FileHandle pFile;

	int statusOpen = rbfm->openFile(tableName + ".tbl", pFile);

	if (statusOpen != 0) {

		return FAIL;

	}

	vector<Attribute> recordDescriptor;

	vector<Attribute>::iterator it;

	getAttributes(tableName, recordDescriptor);



	char *tkey = (char*) malloc(PAGE_SIZE);

	void *key, *key1;

	key = (char *) malloc(PAGE_SIZE);

	key1 = (char *) malloc(PAGE_SIZE);

	int offset, size;

	tablesOpenHandler();

	for (int i = 0; i < recordDescriptor.size(); i++) {

		RID rid1;

		RBFM_ScanIterator rmsi2;



		int check = 0;



		rbfm->scan(catalogHandle, Tables, "table-name", NO_OP,

				tableName.c_str(), tableAttr, rmsi2);

		for (; rmsi2.getNextRecord(rid1, key) != RBFM_EOF;) {



			offset = 5;



			size = *((int *) ((char *) key + offset));

			offset += 4;

			string name((char *) ((char *) key + offset), size);

			if (name.find(tableName) != std::string::npos

					&& name.find(recordDescriptor[i].name)

					!= std::string::npos) {

				check = 1;

				break;

			}



		}



		if (check == 1) {

			rbfm->readAttribute(pFile, recordDescriptor, rid,

					recordDescriptor[i].name, key1);

			char c;

			memcpy(&c, key1, noOfNullbytes);

			if (c == NOT_NULL_BYTE1) {

				if (recordDescriptor[i].type == 0

						|| recordDescriptor[i].type == 1) {

					memcpy(tkey, (char *) key1 + noOfNullbytes, sizeof(int));

				} else {

					offset = 1;

					size = *((int *) ((char *) key1 + offset));

					memcpy(tkey, &size, sizeof(int));

					offset += sizeof(int);



					memcpy(tkey + sizeof(int), (char *) key1 + offset, size);

				}

				//	IXFileHandle ixFileHandle;

				//	idx->deleteEntry(ixFileHandle, recordDescriptor[i], tkey, rid);

				IXFileHandle ixFileHandle;

				string snew = tableName + "_" + recordDescriptor[i].name;

				idx->openFile(snew.c_str(), ixFileHandle);

				idx->deleteEntry(ixFileHandle, recordDescriptor[i], tkey, rid);

				idx->closeFile(ixFileHandle);

			}

		}

		rmsi2.close();



	}



	RC status = rbfm->updateRecord(pFile, recordDescriptor, data, rid);



	for (int i = 0; i < recordDescriptor.size(); i++) {

		RID rid1;

		RBFM_ScanIterator rmsi2;



		int check = 0;



		rbfm->scan(catalogHandle, Tables, "table-name", NO_OP,

				tableName.c_str(), tableAttr, rmsi2);

		for (; rmsi2.getNextRecord(rid1, key) != RBFM_EOF;) {



			offset = 5;



			size = *((int *) ((char *) key + offset));

			offset += 4;

			string name((char *) ((char *) key + offset), size);

			if (name.find(tableName) != std::string::npos

					&& name.find(recordDescriptor[i].name)

					!= std::string::npos) {

				check = 1;

				break;

			}



		}



		if (check == 1) {



			rbfm->readAttribute(pFile, recordDescriptor, rid,

					recordDescriptor[i].name, key1);

			char c;



			memcpy(&c, key1, noOfNullbytes);

			if (c == NOT_NULL_BYTE1) {

				if (recordDescriptor[i].type == 0

						|| recordDescriptor[i].type == 1) {

					memcpy(tkey, (char *) key1 + noOfNullbytes, sizeof(int));

				} else {

					offset = 1;

					size = *((int *) ((char *) key1 + offset));

					memcpy(tkey, &size, sizeof(int));

					offset += sizeof(int);



					memcpy(tkey + sizeof(int), (char *) key1 + offset, size);

				}

				IXFileHandle ixFileHandle;

				string snew = tableName + "_" + recordDescriptor[i].name;

				idx->openFile(snew.c_str(), ixFileHandle);

				//	int y = *(int*)(tkey);

				//	cout<<"\n READING TUPLE : " << y << flush;

				idx->insertEntry(ixFileHandle, recordDescriptor[i], tkey, rid);

				idx->closeFile(ixFileHandle);

			}

		}

		rmsi2.close();

	}



	free(key);

	free(key1);

	free(tkey);

	rbfm->closeFile(pFile);

	tablesCloseHandler();

	return status;

}

RC RelationManager::readTuple(const string &tableName, const RID &rid,
		void *data) {

	FileHandle pFile;
	int statusOpen = rbfm->openFile(tableName + ".tbl", pFile);
	if (statusOpen != 0) {
		return FAIL;
	}
	vector<Attribute> recordDescriptor;
	getAttributes(tableName, recordDescriptor);
	RC status = rbfm->readRecord(pFile, recordDescriptor, rid, data);

	rbfm->closeFile(pFile);
	return status;

}

RC RelationManager::printTuple(const vector<Attribute> &attrs,
		const void *data) {
	RC status = rbfm->printRecord(attrs, data);

	return status;
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid,
		const string &attributeName, void *data) {

	FileHandle pFile;
	int statusOpen = rbfm->openFile(tableName + ".tbl", pFile);
	if (statusOpen != 0) {
		return FAIL;
	}
	vector<Attribute> recordDescriptor;
	getAttributes(tableName, recordDescriptor);
	RC status = rbfm->readAttribute(pFile, recordDescriptor, rid, attributeName,
			data);
	if (status > 0) {
		status = PASS;
	}

	rbfm->closeFile(pFile);
	return status;

}

RC RelationManager::scan(const string &tableName,
		const string &conditionAttribute, const CompOp compOp,
		const void *value, const vector<string> &attributeNames,
		RM_ScanIterator &rm_ScanIterator) {

	FileHandle pFile;
	int statusOpen = rbfm->openFile(tableName + ".tbl", pFile);
	if(pFile.checkFileEmpty()){
		return FAIL;
	}
	if (statusOpen != 0) {
		return FAIL;
	}
	vector<Attribute> recordDescriptor;
	getAttributes(tableName, recordDescriptor);

	return rbfm->scan(pFile, recordDescriptor, conditionAttribute, compOp,
			value, attributeNames, rm_ScanIterator.rbfmsi);
}

RC RM_ScanIterator::getNextTuple(RID &rid, void *data) {

	int status = rbfmsi.getNextRecord(rid, data);
	//	cout<<"\n In get next tuple " << rid.pageNum << " and " << rid.slotNum << " and status "  << status;
	if (status == -5) {
		return RM_EOF;
	}
	return status;
}

RC RM_ScanIterator::close() {

	rbfm->closeFile(fileHandle);
	return 0;
}

RC RelationManager::createIndex(const string &tableName,
		const string &attributeName) {

	FileHandle pFile;
	int statusOpen = rbfm->openFile(tableName + ".tbl", pFile);
	if (statusOpen != 0) {
		return FAIL;
	}

	rbfm->closeFile(pFile);

	RBFM_ScanIterator rmsi;
	RID rid;

	void *data10 = (char *) malloc(PAGE_SIZE);

	int noOfNullbytes = 1;
	tablesOpenHandler();
	string s;

	s = tableName + "_" + attributeName;

	rbfm->scan(catalogHandle, Tables, "table-name", EQ_OP, s.c_str(), tableAttr,
			rmsi);

	for (; rmsi.getNextRecord(rid, data10) != RBFM_EOF;) {

		tablesCloseHandler();
		free(data10);
		return -1;
	}
	rmsi.close();
	idx->createFile(s);

	RBFM_ScanIterator rmsi2;
	int tableId = 0;
	rbfm->scan(catalogHandle, Tables, "table-name", NO_OP, s.c_str(), tableAttr,
			rmsi2);
	for (; rmsi2.getNextRecord(rid, data10) != RBFM_EOF;) {

		tableId = *((int *) ((char *) data10 + noOfNullbytes));
	}
	tableId++;
	rmsi2.close();

	//	free(data);
	//
	//	data = (char *) malloc(PAGE_SIZE);

	memset(data10, 0, noOfNullbytes);				//Null bytes

	memcpy((char *) data10 + noOfNullbytes, &tableId, sizeof(int));	//table -id

	int tableLength = s.length();
	const char *tableNameChar = s.c_str();

	memcpy((char *) data10 + noOfNullbytes + sizeof(int), &tableLength,
			sizeof(int));
	//table-name

	memcpy((char *) data10 + (sizeof(int) * 2) + noOfNullbytes,
			((char*) ((char *) tableNameChar)), tableLength);

	memcpy((char *) data10 + (sizeof(int) * 2) + tableLength + noOfNullbytes,
			&tableLength, sizeof(int));
	//file-name

	memcpy((char *) data10 + (sizeof(int) * 3) + tableLength + noOfNullbytes,
			((char*) ((char *) tableNameChar)), tableLength);

	RC status = rbfm->insertRecord(catalogHandle, Tables, data10, rid);
	if (status != 0)
		return -1;

	free(data10);
	tablesCloseHandler();

	IXFileHandle ixFileHandle;

	if (idx->openFile(s, ixFileHandle) != 0)
		return -1;

	vector<Attribute> attrs;
	if (getAttributes(tableName, attrs) != 0)
		return -1;
	unsigned int attrI;
	for (attrI = 0; attrI < attrs.size(); attrI++)
	{	if (attrs[attrI].name.compare(attributeName) == 0)
		break;
	if (attrI == attrs.size())
		return -1;
	}
	RM_ScanIterator rmsi_itr;
	vector<string> attributeNames;
	attributeNames.push_back(attributeName);
	string conditionAttribute;
	bool scanf = false;
	if (this->scan(tableName, conditionAttribute, NO_OP, NULL, attributeNames,
			rmsi_itr) != 0){
		scanf = true;
	}
	void *key = (void *) malloc(PAGE_SIZE);

	char *tuple = (char *)malloc(PAGE_SIZE);



	while (rmsi_itr.getNextTuple(rid, tuple) == 0 && scanf == false) {
		rbfm->readAttribute(rmsi_itr.fileHandle,attrs,rid,attrs[attrI].name, key);
		if (idx->insertEntry(ixFileHandle, attrs[attrI], (key + 1), rid) != 0) {
			rmsi_itr.close();
			return -1;
		}
	}
	free(key);
	free(tuple);
	rmsi_itr.close();
	return PASS;
}

RC RelationManager::destroyIndex(const string &tableName,
		const string &attributeName) {
	string s;

	s = tableName + "_" + attributeName;

	if (tableName.compare("Tables") == 0 || tableName.compare("Columns") == 0) {
		return FAIL;
	}
	int tableId = tableNameToTableId(s);

	tablesOpenHandler();
	// storing all RIDs of records to be deleted
	vector<RID> table;
	vector<RID> column;
	RID rid;
	RBFM_ScanIterator rmsi;

	void *data = (char *) malloc(PAGE_SIZE);

	rbfm->scan(catalogHandle, Tables, "table-id", EQ_OP, &tableId, tableAttr,
			rmsi);

	for (; rmsi.getNextRecord(rid, data) != RBFM_EOF;) {

		table.push_back(rid);
	}

	rmsi.close();

	//deleting all rids from catalog
	for (std::vector<RID>::const_iterator it = table.begin(); it != table.end();
			it++) {

		if (rbfm->deleteRecord(catalogHandle, Tables, *it) != 0) {

			return -1;
		}
	}
	tablesCloseHandler();

	free(data);
	return idx->destroyFile(s);

}

RC RelationManager::indexScan(const string &tableName,
		const string &attributeName, const void *lowKey, const void *highKey,
		bool lowKeyInclusive, bool highKeyInclusive,
		RM_IndexScanIterator &rm_IndexScanIterator) {

	string s;

	s = tableName + "_" + attributeName;

	IXFileHandle ixFileHandle;

	if (idx->openFile(s, ixFileHandle) != 0)
		return FAIL;

	vector<Attribute> attrs;
	if (getAttributes(tableName, attrs) != 0)
		return -1;
	unsigned int attrI;
	for (attrI = 0; attrI < attrs.size(); attrI++)
		if (attrs[attrI].name.compare(attributeName) == 0)
			break;
	if (attrI == attrs.size())
		return -1;

	return idx->scan(ixFileHandle, attrs[attrI], lowKey, highKey,
			lowKeyInclusive, highKeyInclusive, rm_IndexScanIterator.ixs);

}

RC RM_IndexScanIterator::getNextEntry(RID& rid, void* key) {
	int status = ixs.getNextEntry(rid, key);
	if (status == -5 || status == -1) {
		return FAIL;
	} else {
		return PASS;
	}
}

//RC RM_IndexScanIterator::getNextEntry(RID& rid, void* key) {
//	return ixs.getNextEntry(rid, key);
//	//return true;   // ask prashanth
//}

RC RM_IndexScanIterator::close() {

	return ixs.close();
}
