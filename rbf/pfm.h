/*******************************************************
 * @author: Prashanth Reddy Billa
 * @author: Rajesh Yarlagadda
 * Team Name - Knights, Team Number - 10
 * Project2 - CS222
 */

#ifndef _pfm_h_
#define _pfm_h_

typedef unsigned PageNum;
typedef int RC;
typedef char byte;

#define PAGE_SIZE 4096
#include <string>
#include <climits>
#include <map>
#include<fstream>
using namespace std;

class FileHandle;

class PagedFileManager
{
public:
    static PagedFileManager* instance();                     			// Access to the _pf_manager instance

    RC createFile    (const string &fileName);                         	// Create a new file
    RC destroyFile   (const string &fileName);                         	// Destroy a file
    RC openFile      (const string &fileName, FileHandle &fileHandle); 	// Open a file
    RC closeFile     (FileHandle &fileHandle);                        	// Close a file

	const std::map<string, int>& getFileAccessCount() const {
		return fileAccessCount;
	}

	void setFileAccessCount(const std::map<string, int>& fileAccessCount) {
		this->fileAccessCount = fileAccessCount;
	}

	RC appendData(string fileName, char *d);
	RC readRidData(string fileName, char *data, int pos);

protected:
    PagedFileManager();                                   				// Constructor
    ~PagedFileManager();                                  				// Destructor

private:
    static PagedFileManager *_pf_manager;
    std::map <string, int> fileAccessCount;
    //data member declaration

};


class FileHandle
{
public:

	static FileHandle* instance();
    // variables to keep the counter for each operation
	unsigned readPageCounter;
	unsigned writePageCounter;
	unsigned appendPageCounter;
	fstream *currentPointer;
	string currentFileName;

	int currentFreePageNumber;

    FileHandle();                                                    	// Default constructor
    ~FileHandle();                                                   	// Destructor

    RC readPage(PageNum pageNum, void *data);                           // Get a specific page
    RC writePage(PageNum pageNum, const void *data);                    // Write a specific page
    RC appendPage(const void *data);                                    // Append a specific page
    unsigned getNumberOfPages();                                        // Get the number of pages in the file
    RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);  // Put the current counter values into variables

    bool checkFileEmpty();
    bool pageExists(PageNum);

	const string& getCurrentFileName() const {
		return currentFileName;
	}

	void setCurrentFileName(const string& currentFileName) {
		this->currentFileName = currentFileName;
	}

	long getCurrentFileSize() const {
		return currentFileSize;
	}

	void setCurrentFileSize(long currentFileSize) {
		this->currentFileSize = currentFileSize;
	}

	fstream* getCurrentPointer(){
		return currentPointer;
	}

	void setCurrentPointer(fstream& currentPointer) {
		this->currentPointer = &currentPointer;
	}

private:
    //data member

	static FileHandle *_pf_manager;
    long currentFileSize;



};

#endif
