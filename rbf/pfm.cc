/*******************************************************
 * @author: Prashanth Reddy Billa
 * @author: Rajesh Yarlagadda
 * Team Name - Knights, Team Number - 10
 * Project2 - CS222
 */

#include "pfm.h"
#include <fstream>
#include <string.h>
#include <cstring>
#include <stdlib.h>
#include <iostream>
#define FAIL -1
#define PASS 0
#define PAGE_SIZE 4096

PagedFileManager* PagedFileManager::_pf_manager = 0;

PagedFileManager* PagedFileManager::instance()
{
	if(!_pf_manager)
		_pf_manager = new PagedFileManager();

	return _pf_manager;
}


PagedFileManager::PagedFileManager()
{
}


PagedFileManager::~PagedFileManager()
{
}


RC PagedFileManager::createFile(const string &fileName)
{
	if(fileName.empty() || fileName == ""){
		return -1;
	}
	ifstream file;
	file.open(fileName.c_str(), ifstream::binary);
	if(file.is_open()){
		file.close();
		return FAIL; //file already exists
	}
	file.close();
	fstream newFile;
	newFile.open(fileName.c_str(), fstream::binary|fstream::out); //file is now ready and open
	newFile.close();
	return PASS;
}


RC PagedFileManager::destroyFile(const string &fileName)
{
	ifstream file;
	file.open(fileName.c_str(), ifstream::in);
	map <string, int> :: iterator it;
	map <string, int> fileAccessCount = (_pf_manager->getFileAccessCount());
	it = fileAccessCount.find(fileName);
	if(file.is_open() && it == fileAccessCount.end()){
		std::remove(fileName.c_str());
		file.close();
		return PASS;
	}
	return FAIL;
}


RC PagedFileManager::openFile(const string &fileName, FileHandle &fileHandle)
{
	fstream file;
	file.open(fileName.c_str(), ios::binary | fstream::out | fstream ::in);
	if(!file.is_open()){
		file.close();
		return FAIL; //file does not exist
	}

	if(fileHandle.currentPointer != NULL){
		fstream f;
		f.open(fileHandle.getCurrentFileName().c_str(), ios::in | ios::binary);
		f.close();
		return FAIL;
	}

	map <string, int> :: iterator it;
	map <string, int> fileAccessCount = (_pf_manager->getFileAccessCount());
	it = fileAccessCount.find(fileHandle.getCurrentFileName());
	if(it != _pf_manager->getFileAccessCount().end()){
		int count = it->second;
		count = count + 1;
		map <string, int> fileAccessCount = (_pf_manager->getFileAccessCount());
		fileAccessCount[fileName] = count;  //increment the Access Count for the file
	}else{
		fileAccessCount[fileName] = 1;   //first time the page is being opened. Set the value to 1
	}
	fileHandle.setCurrentPointer(file);

	fileHandle.setCurrentFileName(fileName);
	return PASS;
}


RC PagedFileManager::closeFile(FileHandle &fileHandle)
{
	if(fileHandle.currentPointer == NULL){
		return FAIL;
	}

	fstream f;
	f.open(fileHandle.getCurrentFileName().c_str(), ios::in | ios::binary);
	if(!f.is_open()){
		return FAIL;
	}

	string fileName = fileHandle.getCurrentFileName();
	map <string, int> :: iterator it;
	map <string, int> fileAccessCount = (_pf_manager->getFileAccessCount());
	it = fileAccessCount.find(fileName);
	if(it != _pf_manager->fileAccessCount.end()){
		int count = it->second;
		count = count - 1;
		fileAccessCount[fileName] = count;
		_pf_manager->fileAccessCount.erase(fileName);
		fileHandle.currentPointer = NULL;
		fileHandle.currentFileName.clear();
		return PASS;
	}
	return FAIL;
}

FileHandle::FileHandle()
{
	readPageCounter = 0;
	writePageCounter = 0;
	appendPageCounter = 0;
	currentFileSize = 0;
	currentPointer = NULL;
	currentFreePageNumber = 0;
}


FileHandle::~FileHandle()
{
}

RC FileHandle::readPage(PageNum pageNum, void *data)
{
	readPageCounter++;
	if(!pageExists(pageNum)){
		return FAIL;
	}
	string fileName = this->getCurrentFileName();
	fstream pageFile;
	this->setCurrentPointer(pageFile);
	pageFile.open(fileName.c_str(), fstream :: in | fstream :: out | fstream ::binary);
	if(pageFile.is_open()){
		pageFile.seekg(pageNum*PAGE_SIZE);
		pageFile.read((char*)data, PAGE_SIZE);
		pageFile.close();
		return PASS;
	}
	pageFile.close();
	return FAIL;
}

RC FileHandle::writePage(PageNum pageNum, const void *data)
{
	writePageCounter++;
	fstream pageFile;
	string fileName = this->getCurrentFileName();
	pageFile.open(fileName.c_str(), fstream :: in | fstream :: out | fstream ::binary);
	this->setCurrentPointer(pageFile);
	if(pageFile.is_open()){
		pageFile.seekp(pageNum*PAGE_SIZE);
		pageFile.write((char*)data, PAGE_SIZE);
		pageFile.close();
		return PASS;
	}
	pageFile.flush();
	pageFile.close();
	return FAIL;
}

RC FileHandle::appendPage(const void *data)
{
	appendPageCounter++;
	this->writePage(this->getNumberOfPages(), data);
	return PASS;
}

RC FileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
	readPageCount = readPageCounter;
	writePageCount = writePageCounter;
	appendPageCount = appendPageCounter;
	return PASS;
}

bool FileHandle::pageExists(PageNum num){
	if(num > getNumberOfPages()){
		return false;
	}
	return true;
}

bool FileHandle::checkFileEmpty(){
	ifstream file;

	file.open(this->getCurrentFileName().c_str(), ios::binary|ios::in);
	file.seekg(0, ios::end);

	long file_size = file.tellg();

	if(file_size <= 0){
		file.close();
		return true;
	}else{
		file.close();
		return false;
	}
}

unsigned FileHandle::getNumberOfPages(){
	ifstream in(this->getCurrentFileName().c_str(), std::ifstream::in | std::ifstream::binary);
	in.seekg(0, std::ifstream::end);
	return ((double) in.tellg()) / (double) PAGE_SIZE;
}
