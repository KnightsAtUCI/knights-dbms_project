/*******************************************************
 * @author: Prashanth Reddy Billa
 * @author: Rajesh Yarlagadda
 * Team Name - Knights, Team Number - 10
 * Project2 - CS222
 */

#include "rbfm.h"
#include <iostream>
#include <fstream>
#include <bitset>
#include <vector>
#include <cstdio>
#include <sstream>
#include <cstring>
#include <string.h>
#include <stdlib.h>
#include <limits>
#include <map>
#define FAIL -1
#define PASS 0
#define FREE_SPACE_MARKER_SIZE 2
#define RECORD_UPDATED numeric_limits<unsigned short>::max() - 1
#define RECORD_DELETED numeric_limits<unsigned short>::max()

unsigned char IS_NULL_BYTE = 0x1;
unsigned char NOT_NULL_BYTE = 0x2;

int rty = 0;

RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = 0;

RecordBasedFileManager* RecordBasedFileManager::instance()
{
	if(!_rbf_manager)
		_rbf_manager = new RecordBasedFileManager();

	return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
	_pf_manager = PagedFileManager::instance();
	tempData = (char*)malloc(PAGE_SIZE);
}

RecordBasedFileManager::~RecordBasedFileManager()
{
	free(tempData);
}

RC RecordBasedFileManager::createFile(const string &fileName) {
	return _pf_manager->createFile(fileName);

}

RC RecordBasedFileManager::destroyFile(const string &fileName) {
	return _pf_manager->destroyFile(fileName);
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle) {
	return _pf_manager->openFile(fileName, fileHandle);
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) {
	return _pf_manager->closeFile(fileHandle);
}

int RecordBasedFileManager::getFreePageNumberToInsertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, int recordLength) {
	short freeSpaceValue;
	bool isEmpty = fileHandle.checkFileEmpty();
	short numberOfSlots = 0;
	short v = 0;
	if(isEmpty){
		fileHandle.currentFreePageNumber = 0;
		v = recordLength + FREE_SPACE_MARKER_SIZE;
		if((v + sizeof(short) + sizeof(short)) <= PAGE_SIZE){
			fileHandle.readPage(0, tempData);
			memcpy(tempData, &v, sizeof(short)); // write the free space position
			v = 0;
			memcpy(tempData + PAGE_SIZE - sizeof(short), &v, sizeof(short)); // write 0 for the size of the slot directory
			fileHandle.writePage(fileHandle.currentFreePageNumber, tempData);
			return fileHandle.currentFreePageNumber;
		}else{
			return FAIL;
		}

	}

	fileHandle.readPage(fileHandle.currentFreePageNumber, tempData);
	freeSpaceValue = *((short*)((char*)tempData));
	numberOfSlots = *((short*)((char*)tempData + PAGE_SIZE - sizeof(short)));
	if((freeSpaceValue + recordLength + sizeof(short) + (sizeof(short) * numberOfSlots))  + sizeof(short) <= PAGE_SIZE){
		v = freeSpaceValue + recordLength;
		memcpy(tempData, &v, sizeof(short));
		fileHandle.writePage(fileHandle.currentFreePageNumber, tempData);
		return fileHandle.currentFreePageNumber;
	}

	for(unsigned int currentPage = 0; currentPage < fileHandle.getNumberOfPages(); currentPage++){
		memset(tempData, 0, PAGE_SIZE);
		fileHandle.readPage(currentPage, tempData);
		freeSpaceValue = *((short*)((char*)tempData));
		numberOfSlots = *((short*)((char*)tempData + PAGE_SIZE - sizeof(short)));
		if((freeSpaceValue + recordLength + sizeof(short) + (sizeof(short) * numberOfSlots))  + sizeof(short) <= PAGE_SIZE){
			fileHandle.currentFreePageNumber = currentPage;
			v = freeSpaceValue + recordLength;
			memcpy(tempData, &v, sizeof(short));
			fileHandle.writePage(fileHandle.currentFreePageNumber, tempData);
			return fileHandle.currentFreePageNumber;
		}
	}

	fileHandle.currentFreePageNumber = fileHandle.getNumberOfPages();
	v = recordLength + FREE_SPACE_MARKER_SIZE;
	if((v + sizeof(short) + sizeof(short)) <= PAGE_SIZE){
		memset(tempData, 0, PAGE_SIZE);
		memcpy(tempData, &v, sizeof(short));
		v = 0;
		memcpy(tempData + PAGE_SIZE - sizeof(short), &v, sizeof(short));
		fileHandle.appendPage(tempData);
		return fileHandle.currentFreePageNumber;
	}else{
		return FAIL; //impossible to insert in a single page
	}
	return fileHandle.currentFreePageNumber;
}

int RecordBasedFileManager :: getNewRecordIdentifier(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, RID currentRecordIdentifier, RID &newRID){
	int pageNumber = currentRecordIdentifier.pageNum;
	short slotNumber = currentRecordIdentifier.slotNum;
	fileHandle.readPage(pageNumber, tempData);
	unsigned short slotValue = *((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - (slotNumber * sizeof(short)))));
	if(slotValue == RECORD_DELETED){
		return FAIL;
	}
	unsigned short markerBytes =  *((short*)((char*)(tempData + slotValue)));
	newRID.pageNum = pageNumber;
	newRID.slotNum = slotNumber;
	while(markerBytes == RECORD_UPDATED){
		pageNumber = *((int*)((char*)(tempData + slotValue + sizeof(short))));
		slotNumber = *((short*)((char*)(tempData + slotValue + sizeof(short) + sizeof(int))));
		fileHandle.readPage(pageNumber, tempData);
		slotValue = *((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - (slotNumber * sizeof(short)))));
		if(slotValue == RECORD_DELETED){
			return FAIL;
		}
		markerBytes =  *((short*)((char*)(tempData + slotValue)));
		newRID.pageNum = pageNumber;
		newRID.slotNum = slotNumber;
	}
	return PASS;
}

int RecordBasedFileManager::getRecordLength(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, bool isModifiedFormat){

	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8; //ceil of num of fields / 8
	int recordLength = 0;
	int numOfBytesToRead = 0;
	AttrType attributeType;
	vector <int> nullBits;
	char temp;
	int p = 0;

	if(isModifiedFormat){
		p = sizeof(short);
	}else{
		p = 0;
	}
	for(int t = 0; t < numOfNullBytes; t++){
		temp = *((char*)((char*)data + t + p));
		for(int pos = 1; pos <= 8; pos++){
			if((temp >> pos) & 1){
				nullBits.push_back((8 - pos) + (8 * t) - 1);
			}
		}
	}
	int nullBitsSize=nullBits.size();
	bool flagNull = false;

	short start = 0;
	if(isModifiedFormat){
		start = numOfNullBytes + (numOfFields * sizeof(short)) + sizeof(short);
	}else{
		start = numOfNullBytes;
	}



	for(unsigned int i = 0; i < recordDescriptor.size(); i++){
		flagNull = false;
		attributeType = recordDescriptor[i].type;
		switch(attributeType){
		case TypeInt:
			for(int u = 0; u < nullBitsSize; u++){
				if((unsigned)nullBits[u] == (unsigned)i){
					flagNull = true;
					break;
				}

			}
			if(!flagNull){
				recordLength += sizeof(int);
				start = start + recordDescriptor[i].length;
			}

			break;
		case TypeReal:
			for(int u = 0; u < nullBitsSize; u++){
				if((unsigned)nullBits[u] == (unsigned)i){
					flagNull = true;
					break;
				}

			}
			if(!flagNull){
				recordLength += sizeof(float);
				start = start + recordDescriptor[i].length;
			}
			break;
		case TypeVarChar:
			for(int u = 0; u < nullBitsSize; u++){
				if((unsigned)nullBits[u] == (unsigned)i){
					flagNull = true;
					break;
				}

			}
			if(!flagNull){
				numOfBytesToRead = *((int*)((char*)data+start));
				recordLength = recordLength + numOfBytesToRead + sizeof(int);
				start += numOfBytesToRead + sizeof(int);
			}
			break;
		default: return FAIL;
		}
	}
	return recordLength;

}

//record structure - Length of record (2 bytes), Null Bytes, Address Bytes, Data Bytes

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid) {

	rty++;
	if(rty == 40){
		//		exit(0);
	}
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int numberOfBytesAddressField = sizeof(short) * numOfFields;
	int varCharSize = 0;
	short temp = 0;
	int start = 0;
	short lengthOfRecord = (short)(this->getRecordLength(fileHandle, recordDescriptor, data, false) + numberOfBytesAddressField + numOfNullBytes + sizeof(short));
	fileHandle.currentFreePageNumber = getFreePageNumberToInsertRecord(fileHandle, recordDescriptor, data, lengthOfRecord);
	//cout<<"\n in insert current page number " << currentFreePageNumber;
	short freeSpacePosition = 0;
	short slotDirectorySizeValue = 0;
	fileHandle.readPage(fileHandle.currentFreePageNumber, tempData);

	freeSpacePosition = *((short *)((char *)tempData));
	//cout<<"\n Free Space Position : " << freeSpacePosition;
	slotDirectorySizeValue = *((short *)((char *)tempData + PAGE_SIZE - sizeof(short)));
	//insert a new slot
	temp = freeSpacePosition - lengthOfRecord;

	memcpy(tempData + PAGE_SIZE - sizeof(short) - (slotDirectorySizeValue * sizeof(short)) - (sizeof(short)), &temp, sizeof(short));
	//cout<<"\n Page Number : " << fileHandle.currentFreePageNumber <<" and position : " << PAGE_SIZE - sizeof(short) - (slotDirectorySizeValue * sizeof(short)) - (sizeof(short));
	//increment slot size
	slotDirectorySizeValue = slotDirectorySizeValue + 1;
	memcpy(tempData + PAGE_SIZE - sizeof(short), &slotDirectorySizeValue, sizeof(short));
	rid.pageNum = fileHandle.currentFreePageNumber;
	rid.slotNum = slotDirectorySizeValue;

//	cout<<"\n insert " << rid.pageNum << " , " << rid.slotNum;
	//cout<<"\n Starting to write record at " << freeSpacePosition - lengthOfRecord;
	memcpy(tempData + temp, &lengthOfRecord, sizeof(short)); //copy length of record

	//cout<<"\n writing null bytes at " << freeSpacePosition - lengthOfRecord + sizeof(short);
	memcpy(tempData + temp + sizeof(short), (char *)data, numOfNullBytes); //copy null bytes
	start = numOfNullBytes;
	short address = numOfNullBytes + numberOfBytesAddressField + sizeof(short) + freeSpacePosition - lengthOfRecord;
	//cout<<"\n Initializing address to " << address;
	int tempStart = start;
	int intValue = 0;
	float realvalue = 0.0;
	string varcharValue = "";
	vector <int> nullBits;
	char c1;
	for(int t = 0; t < numOfNullBytes; t++){
		c1 = *((char*)((char*)data+t));
		for(int pos = 1; pos <= 8; pos++){
			if((c1 >> pos) & 1){
				nullBits.push_back((8 - pos) + (8 * t) - 1);
			}
		}
	}
	int nullBitsSize=nullBits.size();
	bool flagNull = false;
	start = 0;
	int index = 0;
	index = numOfNullBytes + freeSpacePosition - lengthOfRecord + sizeof(short);
	//cout<<"\n will write addresses from index : " << index;


	//for O(1) Field Access
	for(unsigned int i = 0; i < recordDescriptor.size(); i++){
		flagNull = false;
		switch(recordDescriptor[i].type){
		case TypeInt :
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(!flagNull){
				address = address + recordDescriptor[i].length;
				//cout<<"\n Writing address for Int " << address;
				memcpy(tempData + index, &address, sizeof(short));
				index += sizeof(short);
				tempStart += sizeof(int);
			}else{
				memcpy(tempData + index, &address, sizeof(short));
				index += sizeof(short);
			}

			break;
		case TypeReal :
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(!flagNull){
				address = address + recordDescriptor[i].length;
				memcpy(tempData + index, &address, sizeof(short));
				index += sizeof(short);
				//cout<<"\n Writing address for Real " << address;
				tempStart += sizeof(float);
			}else{
				memcpy(tempData + index, &address, sizeof(short));
				index += sizeof(short);
			}

			break;
		case TypeVarChar :
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(!flagNull){
				varCharSize = *((int*)((char*)data+tempStart));
				address = address + varCharSize + (short)sizeof(int);
				memcpy(tempData + index, &address, sizeof(short));
				index += sizeof(short);
				//cout<<"\n Writing address for string " << address;
				tempStart += varCharSize;
				tempStart += sizeof(int);
			}else{
				memcpy(tempData + index, &address, sizeof(short));
				index += sizeof(short);
			}

			break;
		default : return FAIL;
		}
	}


	start = numOfNullBytes;
	index = numOfNullBytes + freeSpacePosition - lengthOfRecord + numberOfBytesAddressField + sizeof(short);
	//cout<<"\n will start writing data from index " << index;
	AttrType attributeType;


	for(unsigned int i = 0; i < recordDescriptor.size(); i++){
		flagNull = false;
		attributeType = recordDescriptor[i].type;
		switch(attributeType){
		case TypeInt:
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(!flagNull){
				intValue =  *((int *)((char *)data + start));
				memcpy(tempData + index, &intValue, sizeof(int));
				start = start + recordDescriptor[i].length;
				index = index + recordDescriptor[i].length;

			}
			break;
		case TypeReal:
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(!flagNull){
				realvalue = *((float *)((char *)data + start));
				memcpy(tempData + index, &realvalue, sizeof(float));
				start = start + recordDescriptor[i].length;
				index = index + recordDescriptor[i].length;
			}
			break;
		case TypeVarChar:
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(!flagNull){
				varCharSize = *((int*)((char*)data+start));
				//cout<<"\n inserting int for v at index " << index;
				memcpy(tempData + index, &varCharSize, sizeof(int));
				index += sizeof(int);
				start = start + sizeof(int);
				memcpy(tempData + index, (char*)data + start, varCharSize);
				start = start + varCharSize;
				index += varCharSize;
			}

			break;
		default: return FAIL;
		}
	}


	memcpy(tempData + PAGE_SIZE - sizeof(short) - ((slotDirectorySizeValue -1 ) * sizeof(short)) - (sizeof(short)), &temp, sizeof(short));
	fileHandle.writePage(rid.pageNum, tempData);
	return PASS;
}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data) {
	int rc = fileHandle.readPage(rid.pageNum, tempData);
	if(rc == FAIL){
		return FAIL;
	}
	int numOfFields = recordDescriptor.size();
	unsigned short slotValue = 0;
	int pageNumber = 0;
	short slotNumber = 0;
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;

	short slotPosition = PAGE_SIZE - sizeof(short) - (rid.slotNum * sizeof(short));
	slotValue = *((short *)((char *)(tempData + slotPosition)));
	int numberOfBytesForAddressFields = numOfFields * sizeof(short);
	if(slotValue == RECORD_DELETED){
		return FAIL;
	}

	if(slotValue > 0){
		RID ridNew;
		if(this->getNewRecordIdentifier(fileHandle, recordDescriptor, rid, ridNew) == FAIL){
			return FAIL;
		}
		slotNumber = ridNew.slotNum;
		pageNumber = ridNew.pageNum;
//		cout<<"\n reading page !: " << pageNumber << " and slot " << slotNumber;
		fileHandle.readPage(pageNumber, tempData);
		slotValue = *((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - (slotNumber * sizeof(short)))));
		int dataLengthOfRecord = this->getRecordLength(fileHandle, recordDescriptor, tempData + slotValue, true);
		memcpy((char *)data, tempData + slotValue + sizeof(short), numOfNullBytes);
		memcpy(((char *)data + numOfNullBytes), tempData + slotValue + numOfNullBytes + numberOfBytesForAddressFields + sizeof(short), dataLengthOfRecord);
		return PASS;
	}else{
//		cout<<"\n Need to handle the slotvalue in read " << slotValue;
		return FAIL;
	}
	return FAIL;

}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data) {

	AttrType attributeType;
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int start = 0;

	char temp;
	vector <int> nullBits;
	for(int t = 0; t < numOfNullBytes; t++){
		temp = *((char*)((char*)data+t));
		for(int pos = 1; pos <= 8; pos++){
			if((temp >> pos) & 1){
				nullBits.push_back((8 - pos) + (8 * t) - 1);
			}
		}
	}
	int nullBitsSize=nullBits.size();

	start = (numOfFields + 8 - 1) / 8;
	int intValue = 0;
	float realvalue = 0.0;
	int noOfBytesToRead = 0;
	bool flagNull = false;
	for(unsigned int i = 0; i < recordDescriptor.size(); i++){
		flagNull = false;
		attributeType = recordDescriptor[i].type;
		cout<<recordDescriptor[i].name<<":";
		switch(attributeType){
		case TypeInt:
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(flagNull){
				cout<<"NULL\t";
			}else{
				intValue =  *((int *)((char *)data + start));
				cout<<intValue<<"\t";
				start = start + recordDescriptor[i].length;
			}
			break;
		case TypeReal:
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(flagNull){
				cout<<"NULL\t";
			}else{
				realvalue = *((float *)((char *)data + start));
				cout<<realvalue<<"\t";
				start = start + recordDescriptor[i].length;
			}

			break;
		case TypeVarChar:
			for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
			if(flagNull){
				cout<<"NULL\t";
			}else{
				noOfBytesToRead = *((int*)((char*)data+start));
				start += sizeof(int);
				printf("%.*s\t", noOfBytesToRead, ((char*)((char *)data + start)));
				start = start + noOfBytesToRead;
			}


			break;
		default: return FAIL;
		}

	}
	cout<<endl;
	return PASS;
}

RC RecordBasedFileManager :: deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid){
//	cout<<"\n File Name : " << fileHandle.getCurrentFileName().c_str();
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int numberOfBytesForAddressFields = numOfFields * sizeof(short);

	int rc = fileHandle.readPage(rid.pageNum, tempData);
	if(rc == FAIL){
		return FAIL;
	}
	int numberOfSlots = *((short *)((char *)(tempData + PAGE_SIZE - sizeof(short))));
	short slotPosition = 0;
	unsigned short slotValue = *((short *)((char *)(tempData + slotPosition)));
	short slotNumber = 0;
	int pageNumber = 0;
	if(slotValue == RECORD_DELETED){
		return FAIL; //record can not b deleted
	}
	if(slotValue > 0){
		RID ridNew;
		if(this->getNewRecordIdentifier(fileHandle, recordDescriptor, rid, ridNew) ==FAIL){
			return FAIL;
		}
		slotNumber = ridNew.slotNum;
		pageNumber = ridNew.pageNum;
		fileHandle.readPage(pageNumber, tempData);
		slotPosition = PAGE_SIZE - sizeof(short) - (slotNumber * sizeof(short));
		slotValue = *((short*)((char*)(tempData + slotPosition)));
		numberOfSlots = *((short *)((char *)(tempData + PAGE_SIZE - sizeof(short))));

	}else{
//		cout<<"\n Need to handle the slotvalue in delete" << slotValue;
		return FAIL;
	}

	short freeSpacePosition = *((short *)((char *)(tempData)));
	int lengthOfRecord = this->getRecordLength(fileHandle, recordDescriptor, tempData + slotValue, true) + numOfNullBytes + numberOfBytesForAddressFields + sizeof(short);
	unsigned short marker = RECORD_DELETED;


	memcpy(tempData + slotPosition, &marker, sizeof(short));
	memmove(tempData + slotValue, tempData + slotValue + lengthOfRecord, freeSpacePosition - (slotValue + lengthOfRecord));

	freeSpacePosition -= lengthOfRecord;
	memcpy(tempData, &freeSpacePosition, sizeof(short));

	unsigned short currentSlotValue = 0;
	int pos = PAGE_SIZE - sizeof(short);
	for(int i = 1; i <= numberOfSlots; i++){
		pos -= sizeof(short);
		currentSlotValue = *((short *)((char *)(tempData + pos)));
		if(currentSlotValue == RECORD_DELETED){
			continue;
		}else if(currentSlotValue == 0){
//			cout<<"\n Need to handle the slot 0 from deletion";
		}else if(currentSlotValue > 0 && currentSlotValue > slotValue){
			currentSlotValue = currentSlotValue - lengthOfRecord;
			memcpy(tempData + pos, &currentSlotValue, sizeof(short));
		}
	}

	fileHandle.writePage(pageNumber, tempData);
	return PASS;
}

RC RecordBasedFileManager:: updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid){

	int rc = fileHandle.readPage(rid.pageNum, tempData);
	if(rc == FAIL){
		return FAIL;
	}
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int numberOfBytesForAddressFields = numOfFields * sizeof(short);
	if(*((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - (rid.slotNum * sizeof(short))))) == RECORD_DELETED){
		return FAIL; //record can not b updated
	}
	if(*((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - (rid.slotNum * sizeof(short))))) <= 0){
		return FAIL;
	}

	RID ridNew;
	if(this->getNewRecordIdentifier(fileHandle, recordDescriptor, rid, ridNew) ==FAIL){
		return FAIL;
	}
	fileHandle.readPage(ridNew.pageNum, tempData);
	short slotValue = *((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - ((ridNew.slotNum) * sizeof(short)))));
	short freeSpaceValue = *((short*)((char*)(tempData)));
	short address = sizeof(short) + numOfNullBytes + numberOfBytesForAddressFields + slotValue;
	short oldDataLengthOfRecord = this->getRecordLength(fileHandle, recordDescriptor, tempData + slotValue, true);
	short oldLengthOfRecord = oldDataLengthOfRecord + numOfNullBytes + numberOfBytesForAddressFields + sizeof(short);
	short newDataLengthOfRecord = this->getRecordLength(fileHandle, recordDescriptor, data, false);
	short newLengthOfRecord = newDataLengthOfRecord + numOfNullBytes + numberOfBytesForAddressFields + sizeof(short);
	int recordChangedIdentifier = 0;
	if(newLengthOfRecord < oldLengthOfRecord){ //just rewrite the new record in the same place
		fileHandle.readPage(ridNew.pageNum, tempData);
		memcpy(tempData + slotValue, &newLengthOfRecord, numOfNullBytes);
		memcpy(tempData + slotValue + sizeof(short), (char*)data, numOfNullBytes);
		vector <int> nullBits;
		char c1;
		for(int t = 0; t < numOfNullBytes; t++){
			c1 = *((char*)((char*)data+t));
			for(int pos = 1; pos <= 8; pos++){
				if((c1 >> pos) & 1){
					nullBits.push_back((8 - pos) + (8 * t) - 1);
				}
			}
		}
		int varCharSize = 0;
		int nullBitsSize=nullBits.size();
		bool flagNull = false;
		int tempStart = numOfNullBytes;
		int index = 0;
		index = slotValue + sizeof(short) + numOfNullBytes;
		for(unsigned int i = 0; i < recordDescriptor.size(); i++){
			flagNull = false;
			switch(recordDescriptor[i].type){
			case TypeInt :
				for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
				if(!flagNull){
					address = address + recordDescriptor[i].length;
					//cout<<"\n Writing address for Int " << address;
					memcpy(tempData + index, &address, sizeof(short));
					index += sizeof(short);
					tempStart += sizeof(int);
				}else{
					memcpy(tempData + index, &address, sizeof(short));
					index += sizeof(short);
				}

				break;
			case TypeReal :
				for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
				if(!flagNull){
					address = address + recordDescriptor[i].length;
					memcpy(tempData + index, &address, sizeof(short));
					index += sizeof(short);
					//cout<<"\n Writing address for Real " << address;
					tempStart += sizeof(float);
				}else{
					memcpy(tempData + index, &address, sizeof(short));
					index += sizeof(short);
				}

				break;
			case TypeVarChar :
				for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ flagNull = true; break; } }
				if(!flagNull){
					varCharSize = *((int*)((char*)data+tempStart));
					address = address + varCharSize + (short)sizeof(int);
					memcpy(tempData + index, &address, sizeof(short));
					index += sizeof(short);
					//cout<<"\n Writing address for string " << address;
					tempStart += varCharSize;
					tempStart += sizeof(int);
				}else{
					memcpy(tempData + index, &address, sizeof(short));
					index += sizeof(short);
				}
				break;
			default : return FAIL;
			}
		}
		memcpy(tempData + slotValue + numOfNullBytes + numberOfBytesForAddressFields + sizeof(short), (char *)data + numOfNullBytes, oldDataLengthOfRecord);
		fileHandle.writePage(ridNew.pageNum, tempData);
	}else{
		RID ridInserted;
		this->insertRecord(fileHandle, recordDescriptor, data, ridInserted);
		if(this->getNewRecordIdentifier(fileHandle, recordDescriptor, rid, ridNew) ==FAIL){
			return FAIL;
		}
		fileHandle.readPage(ridNew.pageNum, tempData);
		recordChangedIdentifier = sizeof(short) + sizeof(int) + sizeof(short); //info size to mention the update
		unsigned short marker = RECORD_UPDATED;
		freeSpaceValue = *((short*)((char*)(tempData)));
		int numberOfSlots = *((short*)((char*)(tempData + PAGE_SIZE - sizeof(short))));
		int newPageNumber = ridInserted.pageNum;
		slotValue = *((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - ((ridNew.slotNum) * sizeof(short)))));
		short newSlotNumber = ridInserted.slotNum;
//		cout<<"\n New page : " << newPageNumber << " and " << newSlotNumber << " old " << ridNew.pageNum << " and " << ridNew.slotNum;
		numOfFields = recordDescriptor.size();
        numOfNullBytes = (numOfFields + 8 - 1) / 8;
        numberOfBytesForAddressFields = numOfFields * sizeof(short);
		oldDataLengthOfRecord = this->getRecordLength(fileHandle, recordDescriptor, tempData + slotValue, true);
		oldLengthOfRecord = oldDataLengthOfRecord + numOfNullBytes + numberOfBytesForAddressFields + sizeof(short);
		if(oldLengthOfRecord >= recordChangedIdentifier){
			memcpy(tempData + slotValue, &marker, sizeof(short));
			memcpy(tempData + slotValue + sizeof(short), &newPageNumber, sizeof(int));
			memcpy(tempData + slotValue + sizeof(short) + sizeof(int), &newSlotNumber, sizeof(short));
			memmove(tempData + slotValue + recordChangedIdentifier, tempData + slotValue + oldLengthOfRecord, freeSpaceValue - (slotValue + oldLengthOfRecord - recordChangedIdentifier));
			freeSpaceValue -= (oldLengthOfRecord - recordChangedIdentifier);
			memcpy(tempData, &freeSpaceValue, sizeof(short));
			short currentSlotValue = 0;

			int pos = PAGE_SIZE - sizeof(short);
				for(int i = 1; i <= numberOfSlots; i++){
					pos -= sizeof(short);
					currentSlotValue = *((short *)((char *)(tempData + pos)));
					if(currentSlotValue == RECORD_DELETED || currentSlotValue <= 0){
						continue;
					}else if(currentSlotValue > 0 && currentSlotValue > slotValue){
						currentSlotValue = (currentSlotValue - (oldLengthOfRecord - recordChangedIdentifier));
						memcpy(tempData + pos, &currentSlotValue, sizeof(short));
					}
				}
			fileHandle.writePage(ridNew.pageNum, tempData);
		}else{
//			cout<<"\n NEED TO CHECK DIFFERENT METHOD";
		}
	}
	return PASS;
}

RC RecordBasedFileManager :: readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string &attributeName, void *data){

	int pageNumber = rid.pageNum;
	short slotNumber = rid.slotNum;
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int numberOfBytesForAddressFields = numOfFields * sizeof(short);
	unsigned short slotValue = 0;
	fileHandle.readPage(pageNumber, tempData);
	slotValue = *((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - (slotNumber * sizeof(short)))));
	if(slotValue == RECORD_DELETED){
		return FAIL; //record can not b updated
	}
	if(slotValue > 0){
		RID ridNew;
		if(this->getNewRecordIdentifier(fileHandle, recordDescriptor, rid, ridNew) ==FAIL){
			return FAIL;
		}
		slotNumber = ridNew.slotNum;
		pageNumber = ridNew.pageNum;
		fileHandle.readPage(pageNumber, tempData);
		slotValue = *((short*)((char*)(tempData + (PAGE_SIZE) - sizeof(short) - (slotNumber * sizeof(short)))));
	}else{
//		cout<<"\n Need to handle the slotvalue in read attribute" << slotValue;
		return FAIL;
	}


	this->readRecord(fileHandle, recordDescriptor, rid, tempData);
	char tt;
		vector <int> nullBits;
		for(int t = 0; t < numOfNullBytes; t++){
			tt = *((char*)((char*)tempData+t));
			for(int pos = 1; pos <= 8; pos++){
				if((tt >> pos) & 1){
					nullBits.push_back((8 - pos) + (8 * t) - 1);
				}
			}
		}

	fileHandle.readPage(pageNumber, tempData);
	int start = numOfNullBytes + sizeof(short) + numberOfBytesForAddressFields + slotValue;
	AttrType attributeType;
	int varCharSize = 0;
	bool found = false;
	int intValue = 0;
	float floatValue = 0.0;
	string varcharText = "";

	bool isNull = false;
	int nullBitsSize=nullBits.size();
	int returnValue = 0;
	for(unsigned int i = 0; i < recordDescriptor.size(); i++){
		attributeType = recordDescriptor[i].type;
		isNull = false;
		if(attributeName.compare("") == 0){
			return FAIL;
		}
		if(attributeName.compare(recordDescriptor[i].name) != 0){
			switch(attributeType){
			case TypeInt:
				start = start + sizeof(int);
				break;
			case TypeReal:
				start = start + sizeof(float);
				break;
			case TypeVarChar:
				varCharSize = *((int*)((char*)tempData+start));
				start = start + sizeof(int);
				start = start + varCharSize;
				break;
			case TypeNone:
				return FAIL;
			}
			continue;
		}
		found = true;


		switch(attributeType){
		case TypeInt :
			if(found){
				for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }

				if(isNull){
					memcpy(data, &IS_NULL_BYTE, sizeof(char));
				}else{
					memcpy(data, &NOT_NULL_BYTE, sizeof(char));
				}
				intValue = *((int*)((char*)tempData+start));
				memcpy((char *)data + sizeof(char), &intValue, sizeof(int));
				returnValue = sizeof(int);
				return returnValue;
			}
			start = start + sizeof(int);
			break;
		case TypeReal :
			if(found){
				for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }

				if(isNull){
					memcpy(data, &IS_NULL_BYTE, sizeof(char));
				}else{
					memcpy(data, &NOT_NULL_BYTE, sizeof(char));
				}
				floatValue = *((float*)((char*)tempData+start));
				memcpy((char*)data + sizeof(char), &floatValue, sizeof(float));
				returnValue = sizeof(float);
				return returnValue;
			}
			start = start + sizeof(float);
			break;
		case TypeVarChar:
			if(found){
				for(int u = 0; u < nullBitsSize; u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
				if(isNull){
					memcpy(data, &IS_NULL_BYTE, sizeof(char));
				}else{
					memcpy(data, &NOT_NULL_BYTE, sizeof(char));
				}
				varCharSize = *((int*)((char*)tempData+start));
				memcpy((char *)data + sizeof(char), &varCharSize, sizeof(int));
				start += sizeof(int);
				memcpy((char *)data + sizeof(char) + sizeof(int), ((char*)((char *)tempData + start)), varCharSize);
				returnValue = varCharSize + sizeof(int);
				return returnValue;
			}
			start = start + varCharSize;
			break;
		default : return FAIL;
		}
	}
}

bool RecordBasedFileManager :: isRecordMatching(FileHandle &fileHandle, RID &rid, string conditionAttribute,
		CompOp oper, char * value, vector <string> attributeNames, vector<Attribute> &recordDescriptor, AttrType attributeType){

	if(conditionAttribute.compare("") == 0 || conditionAttribute == ""){
		return true;
	}
	int ret = this->readAttribute(fileHandle, recordDescriptor, rid, conditionAttribute, tempData);
	if(ret == FAIL){
		return false;;
	}
	char nullByte = *((char*)((char*)tempData));
	if(nullByte != IS_NULL_BYTE){
		if(attributeValueEqualToGivenValue(attributeType, oper, (char*)tempData + 1, value) == true)
		{
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
	return false;
}

RC RecordBasedFileManager :: returnMatchingRecordIdentifier(RBFM_ScanIterator &rbfm_ScanIterator){


	bool isLastPage = false;
	if(rbfm_ScanIterator.getAttributeType(rbfm_ScanIterator.attributeInCondition) == TypeNone){
		if(rbfm_ScanIterator.attributeInCondition.compare("") != 0 || rbfm_ScanIterator.attributeInCondition != ""){
			return FAIL;
		}
	}
	if((signed)(rbfm_ScanIterator.fileHandle.getNumberOfPages() - 1) <= (signed)rbfm_ScanIterator.rid.pageNum){
			isLastPage = true;
		}

		unsigned short start = 0;
		short slotDirectorySize = 0;
		unsigned short slotValue = 0;
		rbfm_ScanIterator.fileHandle.readPage(rbfm_ScanIterator.rid.pageNum, tempData);
		slotDirectorySize = *((short*)((char*)tempData + PAGE_SIZE - sizeof(short)));
		if(rbfm_ScanIterator.rid.slotNum > slotDirectorySize){
			if(isLastPage){
				return RBFM_EOF;
			}else{
				return -3;
			}
		}
		int numSlot = rbfm_ScanIterator.rid.slotNum;
		for(unsigned int i = numSlot; i <= slotDirectorySize; i++){
			rbfm_ScanIterator.rid.slotNum = i;
			slotValue = *((short*)((char*)tempData + PAGE_SIZE - sizeof(short) - (i * sizeof(short))));
			if(slotValue == RECORD_DELETED){
				continue;
			}else if(slotValue > 0){
				start = *((short *)((char *)(tempData + slotValue)));
				if(start == RECORD_UPDATED){
					continue;
				}

				if(isRecordMatching(rbfm_ScanIterator.fileHandle, rbfm_ScanIterator.rid, rbfm_ScanIterator.attributeInCondition, rbfm_ScanIterator.oper, rbfm_ScanIterator.value, rbfm_ScanIterator.attributeNames, rbfm_ScanIterator.recordDescriptor, rbfm_ScanIterator.getAttributeType(rbfm_ScanIterator.attributeInCondition)) == true){
					rbfm_ScanIterator.rid.slotNum = i;
					return PASS;
				}
			}
		}

		if(isLastPage){
			return RBFM_EOF; //to indicate that the search is over
		}
		return -3; //new page required to check

	}


RC RecordBasedFileManager :: scan(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor,const string &conditionAttribute,const CompOp compOp,const void *value,const vector<string> &attributeNames,
		RBFM_ScanIterator &rbfm_ScanIterator){
	rbfm_ScanIterator.result.clear();
	rbfm_ScanIterator.currentRIDindex = 0;
	rbfm_ScanIterator.rid.pageNum = 0;
	rbfm_ScanIterator.rid.slotNum = 1;
	rbfm_ScanIterator.attributeInCondition = conditionAttribute;
	rbfm_ScanIterator.recordDescriptor = recordDescriptor;
	rbfm_ScanIterator.oper = compOp;
	rbfm_ScanIterator.attributeNames = attributeNames;
	rbfm_ScanIterator.value = (char *)value ;
	rbfm_ScanIterator.fileHandle = fileHandle;
	_pf_manager->openFile(fileHandle.getCurrentFileName().c_str(), rbfm_ScanIterator.fileHandle);
	return PASS;
}

RC RBFM_ScanIterator::close(){
	this->recordDescriptor.clear();
	this->rid.pageNum = 0;
	this->rid.slotNum = 1;
	PagedFileManager *_pf_manager = PagedFileManager::instance();
	return _pf_manager->closeFile(fileHandle);
}

RC RBFM_ScanIterator::getNextRecord(RID &rid, void *data){
	RecordBasedFileManager *rbf_manager = RecordBasedFileManager::instance();
	rid = this->rid;
	int returnValue = rbf_manager->getNextRecord(*this, data);
	if(returnValue == RBFM_EOF){
		return RBFM_EOF;
	}else if(returnValue == FAIL){
		return RBFM_EOF;
	}
	rid = this->rid;
	this->rid.slotNum = this->rid.slotNum + 1;
	return PASS;
}

//format of data is the format given by the instructor. Null Bytes and Data of the attributes specified
RC RecordBasedFileManager :: getNextRecord(RBFM_ScanIterator &rbfm_ScanIterator, void *data){

	int returnValue = this->returnMatchingRecordIdentifier(rbfm_ScanIterator);

	if(returnValue == -3){
		while(returnValue == -3){
			rbfm_ScanIterator.rid.pageNum = rbfm_ScanIterator.rid.pageNum + 1;
			rbfm_ScanIterator.rid.slotNum = 1;
			returnValue = this->returnMatchingRecordIdentifier(rbfm_ScanIterator);
		}
	}else if(returnValue == -2){
		return RBFM_EOF;
	}else if(returnValue == FAIL){
		return FAIL;
	}else if (returnValue == RBFM_EOF){
		return RBFM_EOF;
	}

	vector <string> attributeNames = rbfm_ScanIterator.attributeNames;
	if(attributeNames.size() == 0){
		return FAIL;
	}
	int numOfNullBytes = (attributeNames.size() + 8 - 1) / 8;
	int index = numOfNullBytes;
	char *nullByte = new char[numOfNullBytes];
	memset(nullByte, 0, numOfNullBytes);
	for(unsigned int i = 0; i < attributeNames.size(); i++){
		int ret = readAttribute(rbfm_ScanIterator.fileHandle, rbfm_ScanIterator.recordDescriptor , rbfm_ScanIterator.rid, attributeNames[i], tempData);
		if(ret < PASS){
			return FAIL;
		}

		char isnullByte = *((char*)((char*)tempData));
		int p = (8 - (i%8) - 1);
		if(isnullByte == IS_NULL_BYTE){
			nullByte[i%8] = nullByte[i%8] | (1 << p);
		}
		memcpy((char *)data + index, tempData + sizeof(char), ret);
		index = index + ret;
	}
	memcpy((char*)data, nullByte, numOfNullBytes);
	free(nullByte);
	return PASS;
}

AttrType RBFM_ScanIterator:: getAttributeType(string attributeName){
	if(attributeName.empty()){
		return (AttrType)FAIL;
	}
	for (unsigned int i = 0; i < this->recordDescriptor.size(); i++){
		if(attributeName.compare(this->recordDescriptor[i].name) == 0){
			return this->recordDescriptor[i].type;
		}
	}
	return (AttrType)FAIL;
}

bool RecordBasedFileManager :: attributeValueEqualToGivenValue(AttrType& attributeType, const CompOp& oper, char* attributeValue, void* givenValue) {
	int varCharLength = 0;
	switch (attributeType) {
	case TypeInt:
		switch (oper) {
		case EQ_OP:
			if (*(int*) attributeValue == *(int*) givenValue) return true;
			break;
		case LT_OP:
			if (*(int*) attributeValue < *(int*) givenValue) return true;
			break;
		case GT_OP:
			if (*(int*) attributeValue > *(int*) givenValue) return true;
			break;
		case LE_OP:
			if (*(int*) attributeValue <= *(int*) givenValue) return true;
			break;
		case GE_OP:
			if (*(int*) attributeValue >= *(int*) givenValue) return true;
			break;
		case NE_OP:
			if (*(int*) attributeValue != *(int*) givenValue) return true;
			break;
		case NO_OP: return true;
		}
		break;
		case TypeReal:
			switch (oper) {
			case EQ_OP:
				if (*(float*) attributeValue == *(float*) givenValue) return true;
				break;
			case LT_OP:
				if (*(float*) attributeValue < *(float*) givenValue) return true;
				break;
			case GT_OP:
				if (*(float*) attributeValue > *(float*) givenValue) return true;
				break;
			case LE_OP:
				if (*(float*) attributeValue <= *(float*) givenValue) return true;
				break;
			case GE_OP:
				if (*(float*) attributeValue >= *(float*) givenValue) return true;
				break;
			case NE_OP:
				if (*(float*) attributeValue != *(float*) givenValue) return true;
				break;
			case NO_OP: return true;
			}
			break;
			case TypeVarChar:
				varCharLength = *((int*) attributeValue);
				string attributeValueInString((char*) attributeValue + sizeof(int), varCharLength);
				string givenValueInString((char*)givenValue);
//				cout<<"\n A : " << attributeValueInString << " and " << " B : " << givenValueInString << flush;


				switch (oper) {
				case EQ_OP:
					if (attributeValueInString.compare(givenValueInString) == 0 || attributeValueInString == givenValueInString) {
						return true;
					}
					break;
				case LT_OP:
					if (attributeValueInString < givenValueInString) return true;
					break;
				case GT_OP:
					if (attributeValueInString > givenValueInString) return true;
					break;
				case LE_OP:
					if (attributeValueInString <= givenValueInString) return true;
					break;
				case GE_OP:
					if (attributeValueInString >= givenValueInString) return true;
					break;
				case NE_OP:
					if (attributeValueInString != givenValueInString) return true;
					break;
				case NO_OP: return true;
				}
				break;
	}
	return false;
}
