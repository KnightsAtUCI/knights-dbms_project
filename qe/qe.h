#ifndef _qe_h_
#define _qe_h_

#include <vector>

#include "../rbf/rbfm.h"
#include "../rm/rm.h"
#include "../ix/ix.h"

#define QE_EOF (-1)  // end of the index scan

using namespace std;

typedef enum{ MIN=0, MAX, COUNT, SUM, AVG } AggregateOp;

// The following functions use the following
// format for the passed data.
//    For INT and REAL: use 4 bytes
//    For VARCHAR: use 4 bytes for the length followed by the characters

struct Value {
	AttrType type;          // type of value
	void     *data;         // value
};


struct Condition {
	string  lhsAttr;        // left-hand side attribute
	CompOp  op;             // comparison operator
	bool    bRhsIsAttr;     // TRUE if right-hand side is an attribute and not a value; FALSE, otherwise.
	string  rhsAttr;        // right-hand side attribute if bRhsIsAttr = TRUE
	Value   rhsValue;       // right-hand side value if bRhsIsAttr = FALSE
};


class Iterator {
	// All the relational operators and access methods are iterators.
public:
	virtual RC getNextTuple(void *data) = 0;
	virtual void getAttributes(vector<Attribute> &attrs) const = 0;
	virtual ~Iterator() {};
	RC readAttributeValueFromPage(void* pageData, const vector<Attribute> &recordDescriptor, const string attributeName, void *data);
};


class TableScan : public Iterator
{
	// A wrapper inheriting Iterator over RM_ScanIterator
public:
	RelationManager &rm;
	RM_ScanIterator *iter;
	string tableName;
	vector<Attribute> attrs;
	vector<string> attrNames;
	RID rid;

	TableScan(RelationManager &rm, const string &tableName, const char *alias = NULL):rm(rm)
	{
		//Set members
		this->tableName = tableName;

		// Get Attributes from RM
		rm.getAttributes(tableName, attrs);

		// Get Attribute Names from RM
		unsigned i;
		for(i = 0; i < attrs.size(); ++i)
		{
			// convert to char *
			attrNames.push_back(attrs.at(i).name);
		}

		// Call RM scan to get an iterator
		iter = new RM_ScanIterator();
		rm.scan(tableName, "", NO_OP, NULL, attrNames, *iter);

		// Set alias
		if(alias) this->tableName = alias;
	};

	// Start a new iterator given the new compOp and value
	void setIterator()
	{
		iter->close();
		delete iter;
		iter = new RM_ScanIterator();
		rm.scan(tableName, "", NO_OP, NULL, attrNames, *iter);
	};

	RC getNextTuple(void *data)
	{
		return iter->getNextTuple(rid, data);
	};

	void getAttributes(vector<Attribute> &attrs) const
	{
		attrs.clear();
		attrs = this->attrs;
		unsigned i;

		// For attribute in vector<Attribute>, name it as rel.attr
		for(i = 0; i < attrs.size(); ++i)
		{
			string tmp = tableName;
			tmp += ".";
			tmp += attrs.at(i).name;
			attrs.at(i).name = tmp;
		}
	};

	~TableScan()
	{
		iter->close();
	};
};


class IndexScan : public Iterator
{
	// A wrapper inheriting Iterator over IX_IndexScan
public:
	RelationManager &rm;
	RM_IndexScanIterator *iter;
	string tableName;
	string attrName;
	vector<Attribute> attrs;
	char key[PAGE_SIZE];
	RID rid;

	IndexScan(RelationManager &rm, const string &tableName, const string &attrName, const char *alias = NULL):rm(rm)
	{
		// Set members
		this->tableName = tableName;
		this->attrName = attrName;


		// Get Attributes from RM
		rm.getAttributes(tableName, attrs);

		// Call rm indexScan to get iterator
		iter = new RM_IndexScanIterator();
		rm.indexScan(tableName, attrName, NULL, NULL, true, true, *iter);

		// Set alias
		if(alias) this->tableName = alias;
	};

	// Start a new iterator given the new key range
	void setIterator(void* lowKey,
			void* highKey,
			bool lowKeyInclusive,
			bool highKeyInclusive)
	{
		iter->close();
		//		delete iter;
		iter = new RM_IndexScanIterator();
		rm.indexScan(tableName, attrName, lowKey, highKey, lowKeyInclusive,
				highKeyInclusive, *iter);
	};

	RC getNextTuple(void *data)
	{
		int rc = iter->getNextEntry(rid, key);
		if(rc == 0)
		{
			rc = rm.readTuple(tableName.c_str(), rid, data);
		}
		return rc;
	};

	void getAttributes(vector<Attribute> &attrs) const
	{
		attrs.clear();
		attrs = this->attrs;
		unsigned i;

		// For attribute in vector<Attribute>, name it as rel.attr
		for(i = 0; i < attrs.size(); ++i)
		{
			string tmp = tableName;
			tmp += ".";
			tmp += attrs.at(i).name;
			attrs.at(i).name = tmp;
		}
	};

	~IndexScan()
	{
		iter->close();
	};
};


class Filter : public Iterator {
	// Filter operator
public:
	Filter(Iterator *input,               // Iterator of input R
			const Condition &condition     // Selection condition
	);
	~Filter(){
		free(tempData);
	};

	RC getNextTuple(void *data);
	// For attribute in vector<Attribute>, name it as rel.attr
	void getAttributes(vector<Attribute> &attrs) const;
	RC getAttribute(vector<Attribute> rightAttributes, string attributeName, Attribute &myAttribute);
	Iterator *filterIterator;
	Condition filterCondition;

	mutable vector <Attribute> allAttributes;
	char *tempData;
};


class Project : public Iterator {

	// Projection operator

public:
	Iterator *iter;
	vector<string> attrNames;
	Project(Iterator *input,                    // Iterator of input R
			const vector<string> &attrNames);  // vector containing attribute names
	~Project();
	RC getNextTuple(void *data) ;

	// For attribute in vector<Attribute>, name it as rel.attr

	void getAttributes(vector<Attribute> &attrs) const;

	void projectSpecificAttrs(void* data);

};

// Optional for the undergraduate solo teams: 5 extra-credit points
class BNLJoin : public Iterator {
	// Block nested-loop join operator
public:
	BNLJoin(Iterator *leftIn,            // Iterator of input R
			TableScan *rightIn,           // TableScan Iterator of input S
			const Condition &condition,   // Join condition
			const unsigned numPages       // # of pages that can be loaded into memory,
	){
		this->leftTable = leftIn;
		this->rightTable = rightIn;
		this->BlockNLJoinCondition = condition;
		this->numberOfPages = numPages;
		this->initial = false;
		this->getAttributes(this->allAttributes);
		rightTableData = (char*)malloc(PAGE_SIZE);
		leftTableDataValue = (char*)malloc(PAGE_SIZE);
		rightTableDataValue = (char*)malloc(PAGE_SIZE);
		leftTableData = (char*)malloc(PAGE_SIZE);
		tempVsize = 0;
		inv = false;
		mapSize = 0;
	}
	~BNLJoin(){
		free(leftTableData);
		free(rightTableData);
		free(leftTableDataValue);
		free(rightTableDataValue);

		for(int u = 0; u < this->floatMap.size(); u++){
			for(int y = 0; y < this->floatMap[u].size(); y++){
				free(this->floatMap[u][y]);
			}

		}
	}

	RC getNextTuple(void *data);
	// For attribute in vector<Attribute>, name it as rel.attr
	void getAttributes(vector<Attribute> &attrs) const;
	RC getAttribute(vector<Attribute> rightAttributes, string attributeName, Attribute &myAttribute);
	Iterator*	 leftTable;
	TableScan* rightTable;
	Condition BlockNLJoinCondition;
	int numberOfPages;
	bool initial;

	mutable vector <Attribute> allAttributes;
	mutable vector <Attribute> leftAttributes;
	mutable vector <Attribute> rightAttributes;
	mutable int mapIndex;

	char *leftTableData;
	char *rightTableData;
	char *leftTableDataValue;
	char *rightTableDataValue;

	bool inv;
	int mapSize;

	mutable map <float, vector<char *> > floatMap;
	int tempVsize;
	mutable map <string, vector<char *> > stringMap;
	vector <char*> store;
};


class INLJoin : public Iterator {
	// Index nested-loop join operator
public:
	INLJoin(Iterator *leftIn,           // Iterator of input R
			IndexScan *rightIn,          // IndexScan Iterator of input S
			const Condition &condition   // Join condition
	);
	~INLJoin(){
		free(leftTableData);
		free(rightTableData);
		free(leftTableDataValue);
		free(rightTableDataValue);
	};

	RC getNextTuple(void *data);
	// For attribute in vector<Attribute>, name it as rel.attr
	void getAttributes(vector<Attribute> &attrs) const;
	RC getAttribute(vector<Attribute> rightAttributes, string attributeName, Attribute &myAttribute);
	Iterator* leftTable;
	IndexScan* rightTable;
	Condition innerNLJoinCondition;
	bool initial;
	mutable vector <Attribute> allAttributes;
	mutable vector <Attribute> leftAttributes;
	mutable vector <Attribute> rightAttributes;
	char *leftTableData;
	char *rightTableData;
	char *leftTableDataValue;
	char *rightTableDataValue;
};

// Optional for everyone. 10 extra-credit points
class GHJoin : public Iterator {
	// Grace hash join operator
public:
	GHJoin(Iterator *leftIn,               // Iterator of input R
			Iterator *rightIn,               // Iterator of input S
			const Condition &condition,      // Join condition (CompOp is always EQ)
			const unsigned numPartitions     // # of partitions for each relation (decided by the optimizer)
	){};
	~GHJoin(){};

	RC getNextTuple(void *data){return QE_EOF;};
	// For attribute in vector<Attribute>, name it as rel.attr
	void getAttributes(vector<Attribute> &attrs) const{};
};

class Aggregate : public Iterator {
	// Aggregation operator
public:
	// Mandatory for graduate teams/solos. Optional for undergrad solo teams: 5 extra-credit points
	// Basic aggregation
	Aggregate(Iterator *input,          // Iterator of input R
			Attribute aggAttr,        // The attribute over which we are computing an aggregate
			AggregateOp op            // Aggregate operation
	);

	// Optional for everyone: 5 extra-credit points
	// Group-based hash aggregation
	Aggregate(Iterator *input,             // Iterator of input R
			Attribute aggAttr,           // The attribute over which we are computing an aggregate
			Attribute groupAttr,         // The attribute over which we are grouping the tuples
			AggregateOp op              // Aggregate operation
	);
	~Aggregate(){
		free(tempData);
		free(buffer);
		free(tempData1);
	};

	RC getNextTuple(void *data);
	// Please name the output attribute as aggregateOp(aggAttr)
	// E.g. Relation=rel, attribute=attr, aggregateOp=MAX
	// output attrname = "MAX(rel.attr)"
	void getAttributes(vector<Attribute> &attrs) const;
	RC writeAttribute(void *data, string name, AttrType type, void* value);
	RC writeAttribute1(void *data, string name, AttrType type, void* value);

	RC writeGroupBy(void *record, string attributeName, AttrType type, void* value);
	Iterator* aggregateIterator;
	Attribute aggregateAttribute;
	AggregateOp operation;
	Attribute groupByAttribute;
	mutable bool initial;
	mutable vector <Attribute> allAttributes;

	float attributeSumFloat;
	int attributeSumInt;
	float recordCount;
	char *tempData;
	char *tempData1;
	float minimumFloat;
	float maximumFloat;
	char *buffer;

	mutable map<float, float> floatFloatMap;
	mutable map<string, float> stringFloatMap;
	map<float, float>::iterator floatIt;
	map<string, float>::iterator stringIt;
};

#endif
