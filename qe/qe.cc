
#include "qe.h"
#include <limits>
#define _relationManager RelationManager::instance()
#define _rbfm RecordBasedFileManager::instance()
Filter::Filter(Iterator* input, const Condition &condition) {
	this->filterIterator = input;
	this->filterCondition = condition;
	this->getAttributes(this->allAttributes);
	tempData = (char*)malloc(PAGE_SIZE);
}

void Filter::getAttributes(vector<Attribute> &attrs) const {
	this->filterIterator->getAttributes(attrs);
	this->allAttributes = attrs;
}

RC Filter::getAttribute(vector<Attribute> allAttributes, string attributeName, Attribute &myAttribute){
	for(unsigned int i = 0; i < allAttributes.size(); i++){
		size_t pos = allAttributes[i].name.find_last_of(".");
		size_t pos1 = attributeName.find_last_of(".");
		if (pos != string::npos){
			if(allAttributes[i].name.substr(pos+1) == attributeName.substr(pos1+1) || attributeName.substr(pos1+1).compare(allAttributes[i].name.substr(pos+1)) == 0){
				myAttribute = allAttributes[i];
				return PASS;
			}
		}
	}
	return FAIL;
}

RC Filter::getNextTuple(void* data) {
	if (this->filterCondition.bRhsIsAttr == true){
		return FAIL;
	}
	Attribute filterAttribute;
	this->getAttribute(this->allAttributes, this->filterCondition.lhsAttr, filterAttribute);

	if (filterAttribute.type != this->filterCondition.rhsValue.type){
		return FAIL;
	}
	int n = this->allAttributes.size();
	int slen = 0;
	while(1){
		if (this->filterIterator->getNextTuple(data) != QE_EOF || this->filterIterator->getNextTuple(data) == PASS) {
			readAttributeValueFromPage(data, this->allAttributes, filterAttribute.name, tempData);
			if(filterAttribute.type == TypeInt || filterAttribute.type == TypeReal){
				if(_rbfm->attributeValueEqualToGivenValue(filterAttribute.type, this->filterCondition.op, (char*) tempData, (char*) this->filterCondition.rhsValue.data) == true){
					return PASS;
				}
			}else{
				slen = *(int*)(char*)(this->filterCondition.rhsValue.data);
				char *test = new char[slen];
				memcpy(test, (char*)((char *)this->filterCondition.rhsValue.data + sizeof(int)), slen);
				if(_rbfm->attributeValueEqualToGivenValue(filterAttribute.type, this->filterCondition.op, (char*) tempData, test) == true){
					free(test);
					return PASS;
				}
				free(test);
			}
		}else{
			return QE_EOF;
		}
	}
	return QE_EOF;
}

// ... the rest of your implementations go here
RC Iterator::readAttributeValueFromPage(void* record, const vector<Attribute> &recordDescriptor, const string attributeName, void *value) {
	unsigned int i = 0;
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int temp = numOfNullBytes;
	char tt;
	vector <int> nullBits;
	bool isNull = false;
	for(int t = 0; t < numOfNullBytes; t++){
		tt = *((char*)((char*)record+t));
		for(int pos = 1; pos <= 8; pos++){
			if((tt >> pos) & 1){
				nullBits.push_back((8 - pos) + (8 * t) - 1);
			}
		}
	}
	while (i < recordDescriptor.size() && (recordDescriptor[i].name.find(attributeName) == std::string::npos)) {
		if (recordDescriptor[i].type == TypeInt){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(int);
			}
		}
		else if (recordDescriptor[i].type == TypeReal){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(float);
			}
		}
		else if (recordDescriptor[i].type == TypeVarChar){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(int) + *(int*) ((char*)record + temp);
			}
		}
		i++;
	}
	if (i == recordDescriptor.size())
		return FAIL;

	for(unsigned int u = 0; u < nullBits.size(); u++){
		if((unsigned)nullBits[u] == (unsigned)i){
			return FAIL;
		}
	}

	int sl = 0;
	if (recordDescriptor[i].type == TypeInt){
		int vv = *(int*)((char *)record + temp);
		memcpy(value, &vv, sizeof(int));
	}
	else if (recordDescriptor[i].type == TypeReal){
		float vf = *(float*)((char *)record + temp);
		memcpy(value, &vf, sizeof(float));
	}
	else if (recordDescriptor[i].type == TypeVarChar){
		sl =  *((int*)((char*)record + temp));
		memcpy(value, &sl, sizeof(int));
		memcpy((char *)value + sizeof(int), (char*)record + temp + 4, sl);
	}

	return PASS;
}

int getRecordLengthNew(const vector<Attribute> &recordDescriptor, const void *data){

	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8; //ceil of num of fields / 8
	int recordLength = 0;
	int numOfBytesToRead = 0;
	AttrType attributeType;
	vector <int> nullBits;
	char temp;
	for(int t = 0; t < numOfNullBytes; t++){
		temp = *((char*)((char*)data + t));
		for(int pos = 1; pos <= 8; pos++){
			if((temp >> pos) & 1){
				nullBits.push_back((8 - pos) + (8 * t) - 1);
			}
		}
	}
	int nullBitsSize=nullBits.size();
	bool flagNull = false;

	short start = 0;
	start = numOfNullBytes;



	for(unsigned int i = 0; i < recordDescriptor.size(); i++){
		flagNull = false;
		attributeType = recordDescriptor[i].type;
		switch(attributeType){
		case TypeInt:
			for(int u = 0; u < nullBitsSize; u++){
				if((unsigned)nullBits[u] == (unsigned)i){
					flagNull = true;
					break;
				}

			}
			if(!flagNull){
				recordLength += sizeof(int);
				start = start + recordDescriptor[i].length;
			}

			break;
		case TypeReal:
			for(int u = 0; u < nullBitsSize; u++){
				if((unsigned)nullBits[u] == (unsigned)i){
					flagNull = true;
					break;
				}

			}
			if(!flagNull){
				recordLength += sizeof(float);
				start = start + recordDescriptor[i].length;
			}
			break;
		case TypeVarChar:
			for(int u = 0; u < nullBitsSize; u++){
				if((unsigned)nullBits[u] == (unsigned)i){
					flagNull = true;
					break;
				}

			}
			if(!flagNull){
				numOfBytesToRead = *((int*)((char*)data+start));
				recordLength = recordLength + numOfBytesToRead + sizeof(int);
				start += numOfBytesToRead + sizeof(int);
			}
			break;
		default: return FAIL;
		}
	}
	return recordLength;

}

void BNLJoin::getAttributes(vector<Attribute> &attrs) const{
	this->leftTable->getAttributes(attrs);
	this->leftAttributes = attrs;
	vector<Attribute> rightAttributes;
	this->rightTable->getAttributes(rightAttributes);
	this->rightAttributes = rightAttributes;
	//	for(int i = 0; i < this->rightAttributes.size(); i++){
	//		cout<<"\n v : " << this->rightAttributes[i].name <<flush;
	//	}
	attrs.insert(attrs.end(), rightAttributes.begin(), rightAttributes.end());
	this->allAttributes = attrs;
}

RC BNLJoin::getAttribute(vector<Attribute> allAttributes, string attributeName, Attribute &myAttribute){
	for(unsigned int i = 0; i < allAttributes.size(); i++){
		size_t pos = allAttributes[i].name.find_last_of(".");
		size_t pos1 = attributeName.find_last_of(".");
		if (pos != string::npos){
			if(allAttributes[i].name.substr(pos+1) == attributeName.substr(pos1+1) || attributeName.substr(pos1+1).compare(allAttributes[i].name.substr(pos+1)) == 0){
				myAttribute = allAttributes[i];
				return PASS;
			}
		}
	}
	return FAIL;
}

RC BNLJoin::getNextTuple(void *data){
	Attribute leftAttribute;
	Attribute rightAttribute;
	map <float, vector<char *> > :: iterator it;
	map <string, vector<char*> > :: iterator its;
	this->getAttribute(this->leftAttributes, this->BlockNLJoinCondition.lhsAttr, leftAttribute);
	this->getAttribute(this->rightAttributes, this->BlockNLJoinCondition.rhsAttr, rightAttribute);
	int numOfFields = this->allAttributes.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;

	int numNullBytesLeft = (this->leftAttributes.size() + 8 - 1)/8;
	int numNullBytesRight = (this->rightAttributes.size() + 8 - 1)/8;

	if(this->BlockNLJoinCondition.bRhsIsAttr == true){
		if(leftAttribute.type != rightAttribute.type){
			return FAIL;
		}
	}
	int entered = 1;
	int leftSize = 0;
	int rightSize = 0;
	if (this->initial == false) {
		BUILD:
		this->rightTable->setIterator();
		entered = 0;
		this->floatMap.clear();
		this->stringMap.clear();
		this->mapSize = 0;
		while(this->leftTable->getNextTuple(leftTableData) != QE_EOF){
			leftSize = getRecordLengthNew(this->leftAttributes, leftTableData);
			rightSize = getRecordLengthNew(this->rightAttributes, rightTableData);
			entered = 1;
			this->initial = true;
			if(leftAttribute.type == TypeVarChar){
				if(readAttributeValueFromPage(leftTableData, this->leftAttributes, leftAttribute.name, leftTableDataValue) == FAIL){
					return FAIL;
				}
				int llen = *(int*)((char*) leftTableDataValue);
				this->mapSize += llen + 4 + leftSize;
				char *ltemp = new char[llen];
				memcpy(ltemp, (char*) leftTableDataValue + sizeof(int), llen);
				string k(ltemp, llen);
				free(ltemp);
				its = this->stringMap.find(k);
				if(its != this->stringMap.end()){
					char *newData = (char*)malloc(leftSize + numNullBytesLeft);
					memcpy(newData, leftTableData, leftSize + numNullBytesLeft);
					its->second.push_back(newData);
				}else{
					char *newData = (char*)malloc(leftSize + numNullBytesLeft);
					memcpy(newData, leftTableData, leftSize + numNullBytesLeft);
					vector <char*> y;
					y.push_back(newData);
					this->stringMap.insert(pair<string, vector<char*> > (k, y));
				}
				if(this->mapSize >= (this->numberOfPages) * PAGE_SIZE){
					break;
				}
			}else{
				this->mapSize += 4 + leftSize;
				if(leftAttribute.type == TypeInt){
					if(readAttributeValueFromPage(leftTableData, this->leftAttributes, leftAttribute.name, leftTableDataValue) == FAIL){
						return FAIL;
					}
					it = this->floatMap.find((float)(*(int*)leftTableDataValue));
					if(it != this->floatMap.end()){
						char *newData = (char*)malloc(leftSize + numNullBytesLeft);
						memcpy(newData, leftTableData, leftSize + numNullBytesLeft);
						it->second.push_back(newData);

					}else{
						char *newData = (char*)malloc(leftSize + numNullBytesLeft);
						memcpy(newData, leftTableData, leftSize + numNullBytesLeft);
						int tr = (*(int*)leftTableDataValue);
						tr = (float)tr;
						vector <char*> y;
						y.push_back(newData);
						this->floatMap.insert(pair<float, vector<char*> > (tr, y));
					}
					if(this->mapSize >= (this->numberOfPages) * PAGE_SIZE){
						break;
					}
				}else{
					if(readAttributeValueFromPage(leftTableData, this->leftAttributes, leftAttribute.name, leftTableDataValue) == FAIL){
						return FAIL;
					}
					it = this->floatMap.find((float)*(float*)leftTableDataValue);
					if(it != this->floatMap.end()){
						char *newData = (char*)malloc(PAGE_SIZE);
						memcpy(newData, leftTableData, PAGE_SIZE);
						it->second.push_back(newData);
					}else{
						char *newData = (char*)malloc(PAGE_SIZE);
						memcpy(newData, leftTableData, PAGE_SIZE);
						float tr = (*(float*)leftTableDataValue);
						vector <char*> y;
						y.push_back(newData);
						this->floatMap.insert(pair<float, vector<char*> > (tr, y));
					}
					if(this->mapSize >= (this->numberOfPages) * PAGE_SIZE){
						break;
					}
				}
			}
		}
	}
	if(entered == 0){
		return 	QE_EOF;
	}
	bool flag = false;
	int rlen = 0;
	it = this->floatMap.begin();
	its = this->stringMap.begin();
	map <float, vector<char*> > :: iterator it1;

	if(inv == true){
		if(tempVsize == store.size()){
			goto LABELELSE;
		}
		leftTableData = store[tempVsize];
		tempVsize++;
		goto LABEL1;
	}else{
		LABELELSE:
		tempVsize = 0;
		store.clear();
	}

	LABEL2:

	while (this->rightTable->getNextTuple(rightTableData) != QE_EOF) {
		if(readAttributeValueFromPage(rightTableData, this->rightAttributes, rightAttribute.name, rightTableDataValue) == FAIL){
			return FAIL;
		}
		if(leftAttribute.type == TypeInt){
			it1 = this->floatMap.begin();
			bool ui = false;
			store.clear();
			tempVsize = 0;
			for(it1 = this->floatMap.begin(); it1 != this->floatMap.end(); ++it1){
				char *temp1 = new char[4];
				char *temp2 = new char[4];
				float uh = (float)it1->first;
				float uh1 = (float)(*(int*)rightTableDataValue);
				memcpy(temp1, &(uh), sizeof(int));
				memcpy(temp2, &(uh1), sizeof(int));
				if(_rbfm->attributeValueEqualToGivenValue(leftAttribute.type, this->BlockNLJoinCondition.op, temp1, temp2) == true){
					ui = true;
					store.insert(store.end(), it1->second.begin(), it1->second.end());
				}
				free(temp1);
				free(temp2);
			}
			if(ui == true){
				ui = false;
				flag = true;
				leftTableData = store[tempVsize];
				tempVsize = tempVsize + 1;
				inv = true;
				break;
			}
		}else if(leftAttribute.type == TypeReal){
			it1 = this->floatMap.begin();
			bool ui = false;
			store.clear();
			tempVsize = 0;
			for(it1 = this->floatMap.begin(); it1 != this->floatMap.end(); ++it1){
				char *temp1 = new char[4];
				char *temp2 = new char[4];
				float uh = (float)it1->first;
				float uh1 = (float)(*(float*)rightTableDataValue);
				memcpy(temp1, &(uh), sizeof(int));
				memcpy(temp2, &(uh1), sizeof(int));
				if(_rbfm->attributeValueEqualToGivenValue(leftAttribute.type, this->BlockNLJoinCondition.op, temp1, temp2) == true){
					ui = true;
					store.insert(store.end(), it1->second.begin(), it1->second.end());
				}
				free(temp1);
				free(temp2);
			}
			if(ui == true){
				ui = false;
				flag = true;
				leftTableData = store[tempVsize];
				tempVsize = tempVsize + 1;
				inv = true;
				break;
			}
		}
		else{
			its = this->stringMap.begin();
			bool ui = false;
			store.clear();
			rlen = *(int*)((char*) rightTableDataValue);
			char *rtemp = new char[rlen];
			memcpy(rtemp, (char*) rightTableDataValue + sizeof(int), rlen);
			tempVsize = 0;
			for(its = this->stringMap.begin(); its != this->stringMap.end(); ++its){
				int ll = its->first.length();
				char *lt = new char[ll + 4];
				memcpy(lt, &(ll), sizeof(int));
				//				memcpy(lt + 4, &(its->first), ll);
				its->first.copy(lt + 4, ll);
				if(_rbfm->attributeValueEqualToGivenValue(leftAttribute.type, this->BlockNLJoinCondition.op, (char*)lt, (char*)rtemp) == true){
					ui = true;
					store.insert(store.end(), its->second.begin(), its->second.end());
				}
				free(lt);
			}
			free(rtemp);
			if(ui == true){
				ui = false;
				flag = true;
				leftTableData = store[tempVsize];
				tempVsize = tempVsize + 1;
				inv = true;
				break;
			}
		}
	}

	if (flag == false){
		this->rightTable->setIterator();
		goto BUILD;
	}

	LABEL1:

	vector <int> leftNull;
	vector <int> rightNull;
	unsigned int bitPosition = 0;
	char temp;
	leftSize = getRecordLengthNew(this->leftAttributes, leftTableData);
	rightSize = getRecordLengthNew(this->rightAttributes, rightTableData);
	char *newNullByte = new char[numOfNullBytes];
	memset(newNullByte, 0, sizeof(newNullByte));
	int p = 0;
	int pos = 0;
	int t = 0;
	for(t = 0; t < numOfNullBytes; t++){
		if(bitPosition < this->leftAttributes.size()){
			temp = *((char*)((char*)leftTableData + t));
			for(pos = 1; pos <= 8; pos++){
				if(bitPosition >= this->leftAttributes.size()){
					break;
				}
				bitPosition += 1;
				if((temp >> (8 - pos)) & 1){
					newNullByte[t] |= 1 << (8 - pos);
				}
			}
		}
		if(bitPosition >= this->leftAttributes.size())
		{
			temp = *((char*)((char*)rightTableData + p));
			for(; pos <= 8; pos++){
				if(bitPosition >= this->leftAttributes.size() + this->rightAttributes.size()){
					break;
				}
				bitPosition += 1;
				if((temp >> (8 - pos)) & 1){
					newNullByte[t] |= 1 << (8 - pos);
				}
			}
			pos = 0;
			p = p + 1;
		}
	}
	memcpy((char*)data, newNullByte, numOfNullBytes);
	memcpy((char*)data + numOfNullBytes, leftTableData + numNullBytesLeft, leftSize);
	memcpy((char*)data + numOfNullBytes +leftSize , rightTableData + numNullBytesRight, rightSize);
	return PASS;
}

RC INLJoin::getAttribute(vector<Attribute> allAttributes, string attributeName, Attribute &myAttribute){
	for(unsigned int i = 0; i < allAttributes.size(); i++){
		size_t pos = allAttributes[i].name.find_last_of(".");
		size_t pos1 = attributeName.find_last_of(".");
		if (pos != string::npos){
			if(allAttributes[i].name.substr(pos+1) == attributeName.substr(pos1+1) || attributeName.substr(pos1+1).compare(allAttributes[i].name.substr(pos+1)) == 0){
				myAttribute = allAttributes[i];
				return PASS;
			}
		}
	}
	return FAIL;
}

INLJoin::INLJoin(Iterator *leftIn, IndexScan *rightIn, const Condition &condition){
	this->leftTable = leftIn;
	this->rightTable = rightIn;
	this->innerNLJoinCondition = condition;
	this->initial = false;
	this->getAttributes(this->allAttributes);
	leftTableData = (char*)malloc(PAGE_SIZE);
	leftTableDataValue = (char*)malloc(PAGE_SIZE);
	rightTableDataValue = (char*)malloc(PAGE_SIZE);
	rightTableData = (char*)malloc(PAGE_SIZE);
}

void INLJoin::getAttributes(vector<Attribute> &attributes) const{
	this->leftTable->getAttributes(attributes);
	this->leftAttributes = attributes;
	vector<Attribute> rightAttributes;
	this->rightTable->getAttributes(rightAttributes);
	this->rightAttributes = rightAttributes;
	attributes.insert(attributes.end(), rightAttributes.begin(), rightAttributes.end());
	this->allAttributes = attributes;
}

RC INLJoin::getNextTuple(void *data){
	int numOfFields = this->allAttributes.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int numNullBytesLeft = (this->leftAttributes.size() + 8 - 1)/8;
	int numNullBytesRight = (this->rightAttributes.size() + 8 - 1)/8;
	int leftSize = 0;
	int rightSize = 0;
	Attribute leftAttribute;
	Attribute rightAttribute;
	this->getAttribute(this->leftAttributes, this->innerNLJoinCondition.lhsAttr, leftAttribute);
	this->getAttribute(this->rightAttributes, this->innerNLJoinCondition.rhsAttr, rightAttribute);
	if(this->innerNLJoinCondition.bRhsIsAttr == true){
		if(leftAttribute.type != rightAttribute.type){
			return FAIL;
		}
	}

	if (this->initial == false) {
		if (this->leftTable->getNextTuple(leftTableData) == PASS){
			this->initial = true;
		}
		else
			return QE_EOF;
	}
	int llen = 0;
	int rlen = 0;
	int y = 0;
	while (this->rightTable->getNextTuple(rightTableData) == PASS) {
		y = y + 1;
		if(readAttributeValueFromPage(leftTableData, this->leftAttributes, leftAttribute.name, leftTableDataValue) == FAIL){
			return FAIL;
		}
		if(readAttributeValueFromPage(rightTableData, this->rightAttributes, rightAttribute.name, rightTableDataValue) == FAIL){
			return FAIL;
		}

		if(leftAttribute.type == TypeInt || leftAttribute.type == TypeReal){
			if(_rbfm->attributeValueEqualToGivenValue(leftAttribute.type, this->innerNLJoinCondition.op, (char*) leftTableDataValue, (char*) rightTableDataValue) == true){
				leftSize = getRecordLengthNew(this->leftAttributes, leftTableData);
				rightSize = getRecordLengthNew(this->rightAttributes, rightTableData);
				goto SUCCESS_LABEL;

			}
		}else{
			llen = *(int*)((char*) leftTableDataValue);
			rlen = *(int*)((char*) rightTableDataValue);
			char *ltemp = new char[llen];
			char *rtemp = new char[rlen];
			memcpy(ltemp, (char*) leftTableDataValue + sizeof(int), llen);
			memcpy(rtemp, (char*) rightTableDataValue + sizeof(int), rlen);
			if(_rbfm->attributeValueEqualToGivenValue(leftAttribute.type, this->innerNLJoinCondition.op, (char*) ltemp, (char*) rtemp) == true){
				leftSize = getRecordLengthNew(this->leftAttributes, leftTableData);
				rightSize = getRecordLengthNew(this->rightAttributes, rightTableData);
				goto SUCCESS_LABEL;
			}
			free(ltemp);
			free(rtemp);
		}
	}


	while (this->leftTable->getNextTuple(leftTableData) == PASS) {
		this->rightTable->setIterator(NULL, NULL, true, true);
		while (this->rightTable->getNextTuple(rightTableData) == PASS) {
			if(readAttributeValueFromPage(leftTableData, this->leftAttributes, leftAttribute.name, leftTableDataValue) == FAIL){
				return FAIL;
			}
			if(readAttributeValueFromPage(rightTableData, this->rightAttributes, rightAttribute.name, rightTableDataValue) == FAIL){
				return FAIL;
			}
			if(leftAttribute.type == TypeInt || leftAttribute.type == TypeReal){
				if(_rbfm->attributeValueEqualToGivenValue(leftAttribute.type, this->innerNLJoinCondition.op, (char*) leftTableDataValue, (char*) rightTableDataValue) == true){
					leftSize = getRecordLengthNew(this->leftAttributes, leftTableData);
					rightSize = getRecordLengthNew(this->rightAttributes, rightTableData);
					goto SUCCESS_LABEL;

				}
			}else{
				llen = *(int*)((char*) leftTableDataValue);
				rlen = *(int*)((char*) rightTableDataValue);
				char *ltemp = new char[llen];
				char *rtemp = new char[rlen];
				memcpy(ltemp, (char*) leftTableDataValue + sizeof(int), llen);
				memcpy(rtemp, (char*) rightTableDataValue + sizeof(int), rlen);
				if(_rbfm->attributeValueEqualToGivenValue(leftAttribute.type, this->innerNLJoinCondition.op, (char*) ltemp, (char*) rtemp) == true){
					leftSize = getRecordLengthNew(this->leftAttributes, leftTableData);
					rightSize = getRecordLengthNew(this->rightAttributes, rightTableData);
					goto SUCCESS_LABEL;
				}
				free(ltemp);
				free(rtemp);
			}
		}
	}
	return QE_EOF;

	SUCCESS_LABEL:
	unsigned int bitPosition = 0;
	char temp;
	char *newNullByte = (char*)malloc(numOfNullBytes);
	memset(newNullByte, 0, numOfNullBytes);
	int p = 0;
	int pos = 0;
	int t = 0;
	for(t = 0; t < numOfNullBytes; t++){
		if(bitPosition < this->leftAttributes.size()){
			temp = *((char*)((char*)leftTableData + t));
			for(pos = 1; pos <= 8; pos++){
				if(bitPosition >= this->leftAttributes.size()){
					break;
				}
				bitPosition += 1;
				if((temp >> (8 - pos)) & 1){
					newNullByte[t] |= 1 << (8 - pos);
				}
			}
		}
		if(bitPosition >= this->leftAttributes.size())
		{
			temp = *((char*)((char*)rightTableData + p));
			for(; pos <= 8; pos++){
				if(bitPosition >= this->leftAttributes.size() + this->rightAttributes.size()){
					break;
				}
				bitPosition += 1;
				if((temp >> (8 - pos)) & 1){
					newNullByte[t] |= 1 << (8 - pos);
				}
			}
			pos = 0;
			p = p + 1;
		}
	}
	memcpy(data, newNullByte, numOfNullBytes);
	memcpy((char*) data + numOfNullBytes, leftTableData + numNullBytesLeft, leftSize);
	memcpy((char*) data + numOfNullBytes + leftSize, rightTableData + numNullBytesRight, rightSize);
	return PASS;
}

Aggregate::Aggregate(Iterator *input,          // Iterator of input R
		Attribute aggAttr,        // The attribute over which we are computing an aggregate
		AggregateOp op){
	this->aggregateIterator = input;
	this->aggregateAttribute = aggAttr;
	this->operation = op;
	this->attributeSumInt = 0;
	this->attributeSumFloat = 0.0;
	this->recordCount = 0;
	tempData = (char*)malloc(PAGE_SIZE);
	tempData1 = (char*)malloc(PAGE_SIZE);
	buffer = (char*)malloc(PAGE_SIZE);
	minimumFloat = std::numeric_limits<float>::max();
	maximumFloat = std::numeric_limits<float>::min();
	this->initial = true;
	this->getAttributes(this->allAttributes);
}

Aggregate::Aggregate(Iterator *input,             // Iterator of input R
		Attribute aggAttr,           // The attribute over which we are computing an aggregate
		Attribute groupAttr,         // The attribute over which we are grouping the tuples
		AggregateOp op ){
	this->aggregateIterator = input;
	this->aggregateAttribute = aggAttr;
	this->operation = op;
	this->attributeSumInt = 0;
	this->attributeSumFloat = 0.0;
	this->recordCount = 0;
	tempData = (char*)malloc(PAGE_SIZE);
	buffer = (char*)malloc(PAGE_SIZE);
	minimumFloat = std::numeric_limits<float>::max();
	maximumFloat = std::numeric_limits<float>::min();
	tempData1 = (char*)malloc(PAGE_SIZE);
	this->initial = true;
	this->groupByAttribute = groupAttr;
	this->getAttributes(this->allAttributes);
}

string getOpName(AggregateOp op){
	if(op == MIN){ return "MIN";}
	else if(op == MAX){return "MAX";}
	else if(op == COUNT){return "COUNT";}
	else if(op == SUM){return "SUM";}
	else if(op == AVG){return "AVG";}
	return "";
}

void Aggregate::getAttributes(vector<Attribute> &attrs) const{
	if(this->groupByAttribute.length != 0){
		attrs.push_back(this->groupByAttribute);
		attrs.push_back(this->aggregateAttribute);
		attrs[1].name = string(getOpName(this->operation)) + "(" + this->aggregateAttribute.name + ")";
	}else{
		attrs.push_back(this->aggregateAttribute);
		attrs[0].name = string(getOpName(this->operation)) + "(" + this->aggregateAttribute.name + ")";
	}
}

RC isVarCharType(AttrType a){
	if(a == TypeVarChar){
		return true;
	}
	return false;
}

RC Aggregate::writeGroupBy(void *record, string attributeName, AttrType type, void* value){
	unsigned int i = 0;
	vector <Attribute> recordDescriptor = this->allAttributes;
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int temp = numOfNullBytes;
	char tt;
	vector <int> nullBits;
	bool isNull = false;
	for(int t = 0; t < numOfNullBytes; t++){
		tt = *((char*)((char*)record+t));
		for(int pos = 1; pos <= 8; pos++){
			if((tt >> pos) & 1){
				nullBits.push_back((8 - pos) + (8 * t) - 1);
			}
		}
	}
	while (i < recordDescriptor.size() && (recordDescriptor[i].name.find(attributeName) == std::string::npos)) {
		if (recordDescriptor[i].type == TypeInt){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(int);
			}
		}
		else if (recordDescriptor[i].type == TypeReal){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(float);
			}
		}
		else if (recordDescriptor[i].type == TypeVarChar){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(int) + *(int*) ((char*)record + temp);
			}
		}
		i++;
	}
	if (i == recordDescriptor.size())
		return FAIL;

	for(unsigned int u = 0; u < nullBits.size(); u++){
		if((unsigned)nullBits[u] == (unsigned)i){
			return FAIL;
		}
	}
	int intValue = 0;
	float floatValue = 0.0;
	if (type == TypeInt){
		intValue = (*(int*)value);
		memcpy((char*)record + temp, &intValue, sizeof(float));
	}
	else if (type == TypeReal){
		floatValue = *(float*)value;
		memcpy((char*)record + temp, &floatValue, sizeof(float));
	}
	else if (type == TypeVarChar){
		return FAIL;
	}
	return PASS;
}

RC Aggregate::writeAttribute(void *record, string attributeName, AttrType type, void* value){
	unsigned int i = 0;
	vector <Attribute> recordDescriptor = this->allAttributes;
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int temp = numOfNullBytes;
	char tt;
	vector <int> nullBits;
	bool isNull = false;
	for(int t = 0; t < numOfNullBytes; t++){
		tt = *((char*)((char*)record+t));
		for(int pos = 1; pos <= 8; pos++){
			if((tt >> pos) & 1){
				nullBits.push_back((8 - pos) + (8 * t) - 1);
			}
		}
	}
	while (i < recordDescriptor.size() && (recordDescriptor[i].name.find(attributeName) == std::string::npos)) {
		if (recordDescriptor[i].type == TypeInt){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(int);
			}
		}
		else if (recordDescriptor[i].type == TypeReal){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(float);
			}
		}
		else if (recordDescriptor[i].type == TypeVarChar){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(int) + *(int*) ((char*)record + temp);
			}
		}
		i++;
	}
	if (i == recordDescriptor.size())
		return FAIL;

	for(unsigned int u = 0; u < nullBits.size(); u++){
		if((unsigned)nullBits[u] == (unsigned)i){
			return FAIL;
		}
	}
	int intValue = 0;
	float floatValue = 0.0;
	if (type == TypeInt){
		floatValue = (float)(*(int*)value);
		memcpy((char*)record + temp, &floatValue, sizeof(float));
	}
	else if (type == TypeReal){
		floatValue = *(float*)value;
		memcpy((char*)record + temp, &floatValue, sizeof(float));
	}
	else if (type == TypeVarChar){
		return FAIL;
	}
	return PASS;
}

RC Aggregate::writeAttribute1(void *record, string attributeName, AttrType type, void* value){
	unsigned int i = 0;
	vector <Attribute> recordDescriptor = this->allAttributes;
	int numOfFields = recordDescriptor.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	int temp = numOfNullBytes;
	char tt;
	vector <int> nullBits;
	bool isNull = false;
	for(int t = 0; t < numOfNullBytes; t++){
		tt = *((char*)((char*)record+t));
		for(int pos = 1; pos <= 8; pos++){
			if((tt >> pos) & 1){
				nullBits.push_back((8 - pos) + (8 * t) - 1);
			}
		}
	}
	while (i < recordDescriptor.size() && (recordDescriptor[i].name.find(attributeName) == std::string::npos)) {
		if (recordDescriptor[i].type == TypeInt){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(int);
			}
		}
		else if (recordDescriptor[i].type == TypeReal){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(float);
			}
		}
		else if (recordDescriptor[i].type == TypeVarChar){
			for(unsigned int u = 0; u < nullBits.size(); u++){ if((unsigned)nullBits[u] == (unsigned)i){ isNull = true; break; } }
			if(isNull == false){
				temp += sizeof(int) + *(int*) ((char*)record + temp);
			}
		}
		i++;
	}
	if (i == recordDescriptor.size())
		return FAIL;

	for(unsigned int u = 0; u < nullBits.size(); u++){
		if((unsigned)nullBits[u] == (unsigned)i){
			return FAIL;
		}
	}
	int intValue = 0;
	float floatValue = 0.0;
	if (type == TypeInt){
		int vv = (int)(*(float*)value);
		floatValue = (float)vv;
		memcpy((char*)record + temp, &floatValue, sizeof(int));
	}
	else if (type == TypeReal){
		floatValue = *(float*)value;
		memcpy((char*)record + temp, &floatValue, sizeof(float));
	}
	else if (type == TypeVarChar){
		return FAIL;
	}
	return PASS;
}

RC Aggregate::getNextTuple(void *data){
	int intTemp1 = 0;
	float floatTemp1 = 0.0;
	float floatTemp = 0.0;
	int intTemp = 0;
	float average = 0.0;
	AggregateOp op = this->operation;
	vector <Attribute> attrs;
	this->aggregateIterator->getAttributes(attrs);
	while(1){
		if(this->initial == false){
			goto LAST_LABEL;
		}
		if (this->aggregateIterator->getNextTuple(data) != QE_EOF || this->aggregateIterator->getNextTuple(data) == PASS) {
			this->recordCount++;
			if(op == MIN){
				if(isVarCharType(this->aggregateAttribute.type)){
					return FAIL;
				}
				if(readAttributeValueFromPage(data, attrs, this->aggregateAttribute.name, tempData) == FAIL){
					return FAIL;
				}

				if(this->aggregateAttribute.type ==TypeInt){
					if(this->groupByAttribute.length != 0){
						if(readAttributeValueFromPage(data, attrs, this->groupByAttribute.name, tempData1) == FAIL){
							return FAIL;
						}
						if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
							intTemp = *(int*)tempData;
							intTemp1 = *(int*)tempData1;
							floatIt = this->floatFloatMap.find((float)intTemp);
							if(floatIt != this->floatFloatMap.end()){
								if(intTemp <= floatIt->second){
									floatIt->second = (float)intTemp;
								}
							}else{
								this->floatFloatMap.insert(pair<float, float > ((float)intTemp1, (float)intTemp ));
							}
						}else{
							//need varchar
						}
					}else{
						intTemp = *(int*)tempData;
						if(intTemp <= this->minimumFloat){
							this->minimumFloat = intTemp;
							memcpy(buffer, &(this->minimumFloat), sizeof(int));
							//							this->writeAttribute(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
						}
					}
				}else if(this->aggregateAttribute.type ==TypeReal){
					if(this->groupByAttribute.length != 0){
						if(readAttributeValueFromPage(data, attrs, this->groupByAttribute.name, tempData1) == FAIL){
							return FAIL;
						}
						if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
							floatTemp = *(float*)tempData;
							floatTemp1 = *(float*)tempData1;
							floatIt = this->floatFloatMap.find((float)floatTemp);
							if(floatIt != this->floatFloatMap.end()){
								if(floatTemp <= floatIt->second){
									floatIt->second = (float)floatTemp;
								}
							}else{
								this->floatFloatMap.insert(pair<float, float > ((float)floatTemp1, (float)floatTemp ));
							}
						}else{
							//need varchar
						}
					}else{
						floatTemp = *(float*)tempData;
						if(floatTemp <= this->minimumFloat){
							this->minimumFloat = floatTemp;
							memcpy(buffer, &(this->minimumFloat), sizeof(float));
//							this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
						}
					}
				}
			}
			else if(op == MAX){
				if(isVarCharType(this->aggregateAttribute.type)){
					return FAIL;
				}
				if(readAttributeValueFromPage(data, attrs, this->aggregateAttribute.name, tempData) == FAIL){
					return FAIL;
				}

				if(this->aggregateAttribute.type ==TypeInt){
					if(this->groupByAttribute.length != 0){
						if(readAttributeValueFromPage(data, attrs, this->groupByAttribute.name, tempData1) == FAIL){
							return FAIL;
						}
						if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
							intTemp = *(int*)tempData;
							intTemp1 = *(int*)tempData1;
							floatIt = this->floatFloatMap.find((float)intTemp);
							if(floatIt != this->floatFloatMap.end()){
								if(intTemp >= floatIt->second){
									floatIt->second = (float)intTemp;
								}
							}else{
								this->floatFloatMap.insert(pair<float, float > ((float)intTemp1, (float)intTemp ));
							}
						}else{
							//need varchar
						}
					}else{
						intTemp = *(int*)tempData;
						if(intTemp >= this->maximumFloat){
							this->maximumFloat = intTemp;
							memcpy(buffer, &(this->maximumFloat), sizeof(int));
							//							this->writeAttribute(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
						}
					}
				}else if(this->aggregateAttribute.type ==TypeReal){
					if(this->groupByAttribute.length != 0){
						if(readAttributeValueFromPage(data, attrs, this->groupByAttribute.name, tempData1) == FAIL){
							return FAIL;
						}
						if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
							floatTemp = *(float*)tempData;
							floatTemp1 = *(float*)tempData1;
							floatIt = this->floatFloatMap.find((float)floatTemp);
							if(floatIt != this->floatFloatMap.end()){
								if(floatTemp >= floatIt->second){
									floatIt->second = (float)floatTemp;
								}
							}else{
								this->floatFloatMap.insert(pair<float, float > ((float)floatTemp1, (float)floatTemp ));
							}
						}else{
							//need varchar
						}
					}else{
						floatTemp = *(float*)tempData;
						if(floatTemp >= this->maximumFloat){
							this->maximumFloat = floatTemp;
							memcpy(buffer, &(this->maximumFloat), sizeof(float));
//							this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
						}
					}
				}
			}
			else if(op == COUNT){
				if(isVarCharType(this->aggregateAttribute.type)){
					return FAIL;
				}
				if(readAttributeValueFromPage(data, attrs, this->aggregateAttribute.name, tempData) == FAIL){
					return FAIL;
				}

				if(this->aggregateAttribute.type ==TypeInt){
					if(this->groupByAttribute.length != 0){
						if(readAttributeValueFromPage(data, attrs, this->groupByAttribute.name, tempData1) == FAIL){
							return FAIL;
						}
						if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
							intTemp = (int)this->recordCount;
							intTemp1 = *(int*)tempData1;
							floatIt = this->floatFloatMap.find((float)intTemp);
							if(floatIt != this->floatFloatMap.end()){
								floatIt->second = floatIt->second + 1;
							}else{
								this->floatFloatMap.insert(pair<float, float > ((float)intTemp1, (float)(1) ));
							}
						}else{
							//need varchar
						}
					}else{
						memcpy(buffer, &(this->recordCount), sizeof(float));
						this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
					}
				}else if(this->aggregateAttribute.type ==TypeReal){
					if(this->groupByAttribute.length != 0){
						if(readAttributeValueFromPage(data, attrs, this->groupByAttribute.name, tempData1) == FAIL){
							return FAIL;
						}
						if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
							floatTemp = (float)this->recordCount;
							floatTemp1 = *(float*)tempData1;
							floatIt = this->floatFloatMap.find((float)floatTemp);
							if(floatIt != this->floatFloatMap.end()){
								floatIt->second = floatIt->second + 1;
							}else{
								this->floatFloatMap.insert(pair<float, float > ((float)floatTemp1, (float)(1.0) ));
							}
						}else{
							//need varchar
						}
					}else{
						memcpy(buffer, &(this->recordCount), sizeof(float));
						this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
					}
				}
			}
			else if(op == SUM || op == AVG){
				if(isVarCharType(this->aggregateAttribute.type)){
					return FAIL;
				}
				if(readAttributeValueFromPage(data, attrs, this->aggregateAttribute.name, tempData) == FAIL){
					return FAIL;
				}

				if(this->aggregateAttribute.type ==TypeInt){
					if(this->groupByAttribute.length != 0){
						if(readAttributeValueFromPage(data, attrs, this->groupByAttribute.name, tempData1) == FAIL){
							return FAIL;
						}
						if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
							intTemp = *(int*)tempData;
							intTemp1 = *(int*)tempData1;
							floatIt = this->floatFloatMap.find((float)intTemp);
							if(floatIt != this->floatFloatMap.end()){
								floatIt->second = floatIt->second + (float)intTemp;
							}else{
								this->floatFloatMap.insert(pair<float, float > ((float)intTemp1, (float)intTemp ));
							}
						}else{
							//need varchar
						}
					}else{
						intTemp = *(int*)tempData;
						this->attributeSumInt = this->attributeSumInt + intTemp;
						memcpy(buffer, &(this->attributeSumInt), sizeof(int));
						this->writeAttribute(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
					}
				}else if(this->aggregateAttribute.type ==TypeReal){
					if(this->groupByAttribute.length != 0){
						if(readAttributeValueFromPage(data, attrs, this->groupByAttribute.name, tempData1) == FAIL){
							return FAIL;
						}
						if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
							floatTemp = *(float*)tempData;
							floatTemp1 = *(float*)tempData1;
							floatIt = this->floatFloatMap.find((float)floatTemp);
							if(floatIt != this->floatFloatMap.end()){
								floatIt->second = floatIt->second + (float)floatTemp;
							}else{
								this->floatFloatMap.insert(pair<float, float > ((float)floatTemp1, (float)floatTemp ));
							}
						}else{
							//need varchar
						}
					}else{
						floatTemp = *(float*)tempData;
						this->attributeSumFloat = this->attributeSumFloat + floatTemp;
						memcpy(buffer, &(this->attributeSumFloat), sizeof(float));
						this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
					}
				}
			}
		}else{
			LAST_LABEL:
			if(this->groupByAttribute.length != 0){
				if(op != AVG){
					if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
						if(this->initial == true){
							this->floatIt = this->floatFloatMap.begin();
							this->initial = false;
						}
						if((this->floatIt) == this->floatFloatMap.end()){
							return QE_EOF;
						}
						if(this->groupByAttribute.type == TypeInt){
							intTemp = (int)(this->floatIt->first);
							floatTemp1 = this->floatIt->second;
							memcpy(buffer, &(intTemp), sizeof(float));
							this->writeGroupBy(data, this->groupByAttribute.name, this->groupByAttribute.type, buffer);
							memcpy(buffer, &(floatTemp1), sizeof(float));
							this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
							this->floatIt++;
							return PASS;
						}else if(this->groupByAttribute.type == TypeReal){
							floatTemp = this->floatIt->first;
							floatTemp1 = this->floatIt->second;
							memcpy(buffer, &(floatTemp), sizeof(float));
							this->writeAttribute1(data, this->groupByAttribute.name, this->groupByAttribute.type, buffer);
							memcpy(buffer, &(floatTemp1), sizeof(float));
							this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
							this->floatIt++;
							return PASS;
						}

					}else{
						//handle varchar
					}
				}else if(op == AVG){
					if(this->groupByAttribute.type == TypeInt || this->groupByAttribute.type == TypeReal){
						if((this->floatIt) == this->floatFloatMap.end()){
							return QE_EOF;
						}
						if(this->initial == true){
							this->floatIt = this->floatFloatMap.begin();
							this->initial = false;
						}
						if(this->groupByAttribute.type == TypeInt){
							intTemp = (int)(this->floatIt->first);
							floatTemp1 = this->floatIt->second;
							memcpy(buffer, &(intTemp), sizeof(float));
							this->writeGroupBy(data, this->groupByAttribute.name, this->groupByAttribute.type, buffer);
							floatTemp1 = (float)(floatTemp1/this->recordCount);
							cout<<"\n avg : " << floatTemp1;
							memcpy(buffer, &(floatTemp1), sizeof(float));
							this->writeAttribute(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
							this->floatIt++;
							return PASS;
						}else if(this->groupByAttribute.type == TypeReal){
							floatTemp = this->floatIt->first;
							floatTemp1 = this->floatIt->second;
							memcpy(buffer, &(floatTemp), sizeof(float));
							this->writeAttribute1(data, this->groupByAttribute.name, this->groupByAttribute.type, buffer);
							floatTemp1 = (float)(floatTemp1/this->recordCount);
							cout<<"\n avg : " << floatTemp1;
							memcpy(buffer, &(floatTemp1), sizeof(float));
							this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
							this->floatIt++;
							return PASS;
						}

					}else{
						//handle varchar
					}
				}
			}else{
				if(op == AVG){
					if(isVarCharType(this->aggregateAttribute.type)){
						return FAIL;
					}
					if(readAttributeValueFromPage(data, attrs, this->aggregateAttribute.name, tempData) == FAIL){
						return FAIL;
					}
					if(this->aggregateAttribute.type ==TypeInt){
						intTemp = this->attributeSumInt;
						Attribute dummy;
						dummy.type = TypeReal;
						dummy.length = this->aggregateAttribute.length;
						dummy.name = this->aggregateAttribute.name;
						floatTemp = intTemp;
						float divisor = this->recordCount;
						average = (float)(floatTemp/divisor);
						memcpy(buffer, &(average), sizeof(float));
						this->writeAttribute(data, this->aggregateAttribute.name, dummy.type, buffer);
					}else if(this->aggregateAttribute.type ==TypeReal){
						floatTemp =this->attributeSumFloat;
						float divisor = this->recordCount;
						average = floatTemp/divisor;
						memcpy(buffer, &(average), sizeof(float));
						this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
					}
				}else if(op == MIN){
					floatTemp =this->minimumFloat;
					memcpy(buffer, &(floatTemp), sizeof(float));
					this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
				}else if(op == MAX){
					floatTemp =this->maximumFloat;
					memcpy(buffer, &(floatTemp), sizeof(float));
					this->writeAttribute1(data, this->aggregateAttribute.name, this->aggregateAttribute.type, buffer);
				}
				if(this->initial == true){
					this->initial = false;
					return true;
				}else{
					return QE_EOF;
				}
			}

		}
	}
	return QE_EOF;
}

Project::Project(Iterator *input, const vector<string> &attrNames) {
	this->iter = input;
	this->attrNames = attrNames;
}

Project::~Project() {}



RC Project::getNextTuple(void *data)
{
	while(iter->getNextTuple(data)==0){
		projectSpecificAttrs(data);

		return 0;

	}



	return QE_EOF;

}



void Project::getAttributes(vector<Attribute> &attrs) const
{
	attrs.clear();
	vector<Attribute> originalAttrs;
	iter->getAttributes(originalAttrs);
	int j = 0;
	for( int i = 0; i < originalAttrs.size(); i++){
		if(strcmp(originalAttrs[i].name.c_str(), attrNames[j].c_str()) == 0){
			attrs.push_back(originalAttrs[i]);
			j++;
		}
		if(j == attrNames.size())
			break;
	}
}



void Project::projectSpecificAttrs(void* data)
{
	vector<Attribute> projectAttributes;
	vector<Attribute> originalAttributes;
	getAttributes(projectAttributes);
	iter->getAttributes(originalAttributes);
	int numOfFields = projectAttributes.size();
	int numOfNullBytes = (numOfFields + 8 - 1) / 8;
	char * temp = (char *)malloc(PAGE_SIZE);
	int offset=0;
	char *value;
	value = (char *)malloc(PAGE_SIZE);
	memset(temp,0,numOfNullBytes);
	offset = numOfNullBytes;
	int size;
	for(int i=0;i<projectAttributes.size();i++)
	{
		int s = readAttributeValueFromPage(data,originalAttributes,attrNames[i],value);
		if(s == -1)
		{
			temp[i/8] |= (1<<(7-i%8));
		}
		else{
			if(projectAttributes[i].type == 0 || projectAttributes[i].type == 1 )
			{
				memcpy(temp + offset,value,sizeof(int));
				offset += sizeof(int);
			}
			else{
				memcpy(&size, value, sizeof(int));
				memcpy(temp + offset,&size,sizeof(int));
				offset += sizeof(int);
				memcpy(temp + offset , value + sizeof(int),size);
				offset += size;
			}
		}
	}
	free(value);
	memcpy(data,temp,offset);
	free(temp);
}
