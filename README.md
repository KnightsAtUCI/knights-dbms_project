Implementation of a basic database management system consisting of the following integrated four core components
i) Record based file manager
ii) Relation manager
iii) Index manager
iv) Query engine

The code is written in C++ programming language.


@ author:
Prashanth Reddy Billa (prashanthreddybilla@gmail.com)

@ author
Rajesh Yarlagadda (rajesh.9625@gmail.com)